package m2.idm.tp3.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import m2.idm.tp3.services.UIMMGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUIMMParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Image'", "'Checkbox'", "'Radio'", "'Select'", "'Input'", "'Poll'", "'{'", "'}'", "'Question'", "'options'", "'type:'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUIMMParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUIMMParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUIMMParser.tokenNames; }
    public String getGrammarFileName() { return "../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g"; }


     
     	private UIMMGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(UIMMGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRulePollSystem"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:60:1: entryRulePollSystem : rulePollSystem EOF ;
    public final void entryRulePollSystem() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:61:1: ( rulePollSystem EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:62:1: rulePollSystem EOF
            {
             before(grammarAccess.getPollSystemRule()); 
            pushFollow(FOLLOW_rulePollSystem_in_entryRulePollSystem61);
            rulePollSystem();

            state._fsp--;

             after(grammarAccess.getPollSystemRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePollSystem68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePollSystem"


    // $ANTLR start "rulePollSystem"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:69:1: rulePollSystem : ( ( rule__PollSystem__PollsAssignment )* ) ;
    public final void rulePollSystem() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:73:2: ( ( ( rule__PollSystem__PollsAssignment )* ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:74:1: ( ( rule__PollSystem__PollsAssignment )* )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:74:1: ( ( rule__PollSystem__PollsAssignment )* )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:75:1: ( rule__PollSystem__PollsAssignment )*
            {
             before(grammarAccess.getPollSystemAccess().getPollsAssignment()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:76:1: ( rule__PollSystem__PollsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==16) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:76:2: rule__PollSystem__PollsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__PollSystem__PollsAssignment_in_rulePollSystem94);
            	    rule__PollSystem__PollsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getPollSystemAccess().getPollsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePollSystem"


    // $ANTLR start "entryRulePoll"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:88:1: entryRulePoll : rulePoll EOF ;
    public final void entryRulePoll() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:89:1: ( rulePoll EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:90:1: rulePoll EOF
            {
             before(grammarAccess.getPollRule()); 
            pushFollow(FOLLOW_rulePoll_in_entryRulePoll122);
            rulePoll();

            state._fsp--;

             after(grammarAccess.getPollRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePoll129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePoll"


    // $ANTLR start "rulePoll"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:97:1: rulePoll : ( ( rule__Poll__Group__0 ) ) ;
    public final void rulePoll() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:101:2: ( ( ( rule__Poll__Group__0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:102:1: ( ( rule__Poll__Group__0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:102:1: ( ( rule__Poll__Group__0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:103:1: ( rule__Poll__Group__0 )
            {
             before(grammarAccess.getPollAccess().getGroup()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:104:1: ( rule__Poll__Group__0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:104:2: rule__Poll__Group__0
            {
            pushFollow(FOLLOW_rule__Poll__Group__0_in_rulePoll155);
            rule__Poll__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPollAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePoll"


    // $ANTLR start "entryRuleQuestion"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:116:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:117:1: ( ruleQuestion EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:118:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion182);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:125:1: ruleQuestion : ( ( rule__Question__Group__0 ) ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:129:2: ( ( ( rule__Question__Group__0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:130:1: ( ( rule__Question__Group__0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:130:1: ( ( rule__Question__Group__0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:131:1: ( rule__Question__Group__0 )
            {
             before(grammarAccess.getQuestionAccess().getGroup()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:132:1: ( rule__Question__Group__0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:132:2: rule__Question__Group__0
            {
            pushFollow(FOLLOW_rule__Question__Group__0_in_ruleQuestion215);
            rule__Question__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:144:1: entryRuleOption : ruleOption EOF ;
    public final void entryRuleOption() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:145:1: ( ruleOption EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:146:1: ruleOption EOF
            {
             before(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption242);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getOptionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:153:1: ruleOption : ( ( rule__Option__Group__0 ) ) ;
    public final void ruleOption() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:157:2: ( ( ( rule__Option__Group__0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:158:1: ( ( rule__Option__Group__0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:158:1: ( ( rule__Option__Group__0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:159:1: ( rule__Option__Group__0 )
            {
             before(grammarAccess.getOptionAccess().getGroup()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:160:1: ( rule__Option__Group__0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:160:2: rule__Option__Group__0
            {
            pushFollow(FOLLOW_rule__Option__Group__0_in_ruleOption275);
            rule__Option__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOption"


    // $ANTLR start "entryRuleWidgetQuestType"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:172:1: entryRuleWidgetQuestType : ruleWidgetQuestType EOF ;
    public final void entryRuleWidgetQuestType() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:173:1: ( ruleWidgetQuestType EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:174:1: ruleWidgetQuestType EOF
            {
             before(grammarAccess.getWidgetQuestTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType302);
            ruleWidgetQuestType();

            state._fsp--;

             after(grammarAccess.getWidgetQuestTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuestType309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetQuestType"


    // $ANTLR start "ruleWidgetQuestType"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:181:1: ruleWidgetQuestType : ( ( rule__WidgetQuestType__Alternatives ) ) ;
    public final void ruleWidgetQuestType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:185:2: ( ( ( rule__WidgetQuestType__Alternatives ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:186:1: ( ( rule__WidgetQuestType__Alternatives ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:186:1: ( ( rule__WidgetQuestType__Alternatives ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:187:1: ( rule__WidgetQuestType__Alternatives )
            {
             before(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:188:1: ( rule__WidgetQuestType__Alternatives )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:188:2: rule__WidgetQuestType__Alternatives
            {
            pushFollow(FOLLOW_rule__WidgetQuestType__Alternatives_in_ruleWidgetQuestType335);
            rule__WidgetQuestType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetQuestType"


    // $ANTLR start "entryRuleWidgetOptType"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:200:1: entryRuleWidgetOptType : ruleWidgetOptType EOF ;
    public final void entryRuleWidgetOptType() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:201:1: ( ruleWidgetOptType EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:202:1: ruleWidgetOptType EOF
            {
             before(grammarAccess.getWidgetOptTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType362);
            ruleWidgetOptType();

            state._fsp--;

             after(grammarAccess.getWidgetOptTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptType369); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetOptType"


    // $ANTLR start "ruleWidgetOptType"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:209:1: ruleWidgetOptType : ( ( rule__WidgetOptType__Group__0 ) ) ;
    public final void ruleWidgetOptType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:213:2: ( ( ( rule__WidgetOptType__Group__0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:214:1: ( ( rule__WidgetOptType__Group__0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:214:1: ( ( rule__WidgetOptType__Group__0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:215:1: ( rule__WidgetOptType__Group__0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getGroup()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:216:1: ( rule__WidgetOptType__Group__0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:216:2: rule__WidgetOptType__Group__0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__0_in_ruleWidgetOptType395);
            rule__WidgetOptType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetOptType"


    // $ANTLR start "entryRuleImage"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:228:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:229:1: ( ruleImage EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:230:1: ruleImage EOF
            {
             before(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage422);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getImageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage429); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:237:1: ruleImage : ( 'Image' ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:241:2: ( ( 'Image' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:242:1: ( 'Image' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:242:1: ( 'Image' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:243:1: 'Image'
            {
             before(grammarAccess.getImageAccess().getImageKeyword()); 
            match(input,11,FOLLOW_11_in_ruleImage456); 
             after(grammarAccess.getImageAccess().getImageKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckbox"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:258:1: entryRuleCheckbox : ruleCheckbox EOF ;
    public final void entryRuleCheckbox() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:259:1: ( ruleCheckbox EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:260:1: ruleCheckbox EOF
            {
             before(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox484);
            ruleCheckbox();

            state._fsp--;

             after(grammarAccess.getCheckboxRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox491); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:267:1: ruleCheckbox : ( 'Checkbox' ) ;
    public final void ruleCheckbox() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:271:2: ( ( 'Checkbox' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:272:1: ( 'Checkbox' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:272:1: ( 'Checkbox' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:273:1: 'Checkbox'
            {
             before(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 
            match(input,12,FOLLOW_12_in_ruleCheckbox518); 
             after(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadio"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:288:1: entryRuleRadio : ruleRadio EOF ;
    public final void entryRuleRadio() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:289:1: ( ruleRadio EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:290:1: ruleRadio EOF
            {
             before(grammarAccess.getRadioRule()); 
            pushFollow(FOLLOW_ruleRadio_in_entryRuleRadio546);
            ruleRadio();

            state._fsp--;

             after(grammarAccess.getRadioRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadio553); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRadio"


    // $ANTLR start "ruleRadio"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:297:1: ruleRadio : ( 'Radio' ) ;
    public final void ruleRadio() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:301:2: ( ( 'Radio' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:302:1: ( 'Radio' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:302:1: ( 'Radio' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:303:1: 'Radio'
            {
             before(grammarAccess.getRadioAccess().getRadioKeyword()); 
            match(input,13,FOLLOW_13_in_ruleRadio580); 
             after(grammarAccess.getRadioAccess().getRadioKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRadio"


    // $ANTLR start "entryRuleSelect"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:318:1: entryRuleSelect : ruleSelect EOF ;
    public final void entryRuleSelect() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:319:1: ( ruleSelect EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:320:1: ruleSelect EOF
            {
             before(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_ruleSelect_in_entryRuleSelect608);
            ruleSelect();

            state._fsp--;

             after(grammarAccess.getSelectRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelect615); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:327:1: ruleSelect : ( 'Select' ) ;
    public final void ruleSelect() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:331:2: ( ( 'Select' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:332:1: ( 'Select' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:332:1: ( 'Select' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:333:1: 'Select'
            {
             before(grammarAccess.getSelectAccess().getSelectKeyword()); 
            match(input,14,FOLLOW_14_in_ruleSelect642); 
             after(grammarAccess.getSelectAccess().getSelectKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleInput"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:348:1: entryRuleInput : ruleInput EOF ;
    public final void entryRuleInput() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:349:1: ( ruleInput EOF )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:350:1: ruleInput EOF
            {
             before(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput670);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getInputRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput677); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:357:1: ruleInput : ( 'Input' ) ;
    public final void ruleInput() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:361:2: ( ( 'Input' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:362:1: ( 'Input' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:362:1: ( 'Input' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:363:1: 'Input'
            {
             before(grammarAccess.getInputAccess().getInputKeyword()); 
            match(input,15,FOLLOW_15_in_ruleInput704); 
             after(grammarAccess.getInputAccess().getInputKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "rule__WidgetQuestType__Alternatives"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:378:1: rule__WidgetQuestType__Alternatives : ( ( ruleCheckbox ) | ( ruleRadio ) | ( ruleSelect ) );
    public final void rule__WidgetQuestType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:382:1: ( ( ruleCheckbox ) | ( ruleRadio ) | ( ruleSelect ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt2=1;
                }
                break;
            case 13:
                {
                alt2=2;
                }
                break;
            case 14:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:383:1: ( ruleCheckbox )
                    {
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:383:1: ( ruleCheckbox )
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:384:1: ruleCheckbox
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleCheckbox_in_rule__WidgetQuestType__Alternatives741);
                    ruleCheckbox();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:389:6: ( ruleRadio )
                    {
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:389:6: ( ruleRadio )
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:390:1: ruleRadio
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleRadio_in_rule__WidgetQuestType__Alternatives758);
                    ruleRadio();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:395:6: ( ruleSelect )
                    {
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:395:6: ( ruleSelect )
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:396:1: ruleSelect
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleSelect_in_rule__WidgetQuestType__Alternatives775);
                    ruleSelect();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuestType__Alternatives"


    // $ANTLR start "rule__WidgetOptType__TypeAlternatives_0_0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:406:1: rule__WidgetOptType__TypeAlternatives_0_0 : ( ( ruleImage ) | ( ruleInput ) );
    public final void rule__WidgetOptType__TypeAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:410:1: ( ( ruleImage ) | ( ruleInput ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            else if ( (LA3_0==15) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:411:1: ( ruleImage )
                    {
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:411:1: ( ruleImage )
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:412:1: ruleImage
                    {
                     before(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 
                    pushFollow(FOLLOW_ruleImage_in_rule__WidgetOptType__TypeAlternatives_0_0807);
                    ruleImage();

                    state._fsp--;

                     after(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:417:6: ( ruleInput )
                    {
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:417:6: ( ruleInput )
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:418:1: ruleInput
                    {
                     before(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 
                    pushFollow(FOLLOW_ruleInput_in_rule__WidgetOptType__TypeAlternatives_0_0824);
                    ruleInput();

                    state._fsp--;

                     after(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__TypeAlternatives_0_0"


    // $ANTLR start "rule__Poll__Group__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:430:1: rule__Poll__Group__0 : rule__Poll__Group__0__Impl rule__Poll__Group__1 ;
    public final void rule__Poll__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:434:1: ( rule__Poll__Group__0__Impl rule__Poll__Group__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:435:2: rule__Poll__Group__0__Impl rule__Poll__Group__1
            {
            pushFollow(FOLLOW_rule__Poll__Group__0__Impl_in_rule__Poll__Group__0854);
            rule__Poll__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__1_in_rule__Poll__Group__0857);
            rule__Poll__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__0"


    // $ANTLR start "rule__Poll__Group__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:442:1: rule__Poll__Group__0__Impl : ( 'Poll' ) ;
    public final void rule__Poll__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:446:1: ( ( 'Poll' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:447:1: ( 'Poll' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:447:1: ( 'Poll' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:448:1: 'Poll'
            {
             before(grammarAccess.getPollAccess().getPollKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__Poll__Group__0__Impl885); 
             after(grammarAccess.getPollAccess().getPollKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__0__Impl"


    // $ANTLR start "rule__Poll__Group__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:461:1: rule__Poll__Group__1 : rule__Poll__Group__1__Impl rule__Poll__Group__2 ;
    public final void rule__Poll__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:465:1: ( rule__Poll__Group__1__Impl rule__Poll__Group__2 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:466:2: rule__Poll__Group__1__Impl rule__Poll__Group__2
            {
            pushFollow(FOLLOW_rule__Poll__Group__1__Impl_in_rule__Poll__Group__1916);
            rule__Poll__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__2_in_rule__Poll__Group__1919);
            rule__Poll__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__1"


    // $ANTLR start "rule__Poll__Group__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:473:1: rule__Poll__Group__1__Impl : ( ( rule__Poll__NameAssignment_1 )? ) ;
    public final void rule__Poll__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:477:1: ( ( ( rule__Poll__NameAssignment_1 )? ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:478:1: ( ( rule__Poll__NameAssignment_1 )? )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:478:1: ( ( rule__Poll__NameAssignment_1 )? )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:479:1: ( rule__Poll__NameAssignment_1 )?
            {
             before(grammarAccess.getPollAccess().getNameAssignment_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:480:1: ( rule__Poll__NameAssignment_1 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:480:2: rule__Poll__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Poll__NameAssignment_1_in_rule__Poll__Group__1__Impl946);
                    rule__Poll__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPollAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__1__Impl"


    // $ANTLR start "rule__Poll__Group__2"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:490:1: rule__Poll__Group__2 : rule__Poll__Group__2__Impl rule__Poll__Group__3 ;
    public final void rule__Poll__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:494:1: ( rule__Poll__Group__2__Impl rule__Poll__Group__3 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:495:2: rule__Poll__Group__2__Impl rule__Poll__Group__3
            {
            pushFollow(FOLLOW_rule__Poll__Group__2__Impl_in_rule__Poll__Group__2977);
            rule__Poll__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__3_in_rule__Poll__Group__2980);
            rule__Poll__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__2"


    // $ANTLR start "rule__Poll__Group__2__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:502:1: rule__Poll__Group__2__Impl : ( '{' ) ;
    public final void rule__Poll__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:506:1: ( ( '{' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:507:1: ( '{' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:507:1: ( '{' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:508:1: '{'
            {
             before(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__Poll__Group__2__Impl1008); 
             after(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__2__Impl"


    // $ANTLR start "rule__Poll__Group__3"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:521:1: rule__Poll__Group__3 : rule__Poll__Group__3__Impl rule__Poll__Group__4 ;
    public final void rule__Poll__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:525:1: ( rule__Poll__Group__3__Impl rule__Poll__Group__4 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:526:2: rule__Poll__Group__3__Impl rule__Poll__Group__4
            {
            pushFollow(FOLLOW_rule__Poll__Group__3__Impl_in_rule__Poll__Group__31039);
            rule__Poll__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__4_in_rule__Poll__Group__31042);
            rule__Poll__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__3"


    // $ANTLR start "rule__Poll__Group__3__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:533:1: rule__Poll__Group__3__Impl : ( ( ( rule__Poll__QuestionsAssignment_3 ) ) ( ( rule__Poll__QuestionsAssignment_3 )* ) ) ;
    public final void rule__Poll__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:537:1: ( ( ( ( rule__Poll__QuestionsAssignment_3 ) ) ( ( rule__Poll__QuestionsAssignment_3 )* ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:538:1: ( ( ( rule__Poll__QuestionsAssignment_3 ) ) ( ( rule__Poll__QuestionsAssignment_3 )* ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:538:1: ( ( ( rule__Poll__QuestionsAssignment_3 ) ) ( ( rule__Poll__QuestionsAssignment_3 )* ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:539:1: ( ( rule__Poll__QuestionsAssignment_3 ) ) ( ( rule__Poll__QuestionsAssignment_3 )* )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:539:1: ( ( rule__Poll__QuestionsAssignment_3 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:540:1: ( rule__Poll__QuestionsAssignment_3 )
            {
             before(grammarAccess.getPollAccess().getQuestionsAssignment_3()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:541:1: ( rule__Poll__QuestionsAssignment_3 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:541:2: rule__Poll__QuestionsAssignment_3
            {
            pushFollow(FOLLOW_rule__Poll__QuestionsAssignment_3_in_rule__Poll__Group__3__Impl1071);
            rule__Poll__QuestionsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPollAccess().getQuestionsAssignment_3()); 

            }

            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:544:1: ( ( rule__Poll__QuestionsAssignment_3 )* )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:545:1: ( rule__Poll__QuestionsAssignment_3 )*
            {
             before(grammarAccess.getPollAccess().getQuestionsAssignment_3()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:546:1: ( rule__Poll__QuestionsAssignment_3 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==19) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:546:2: rule__Poll__QuestionsAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Poll__QuestionsAssignment_3_in_rule__Poll__Group__3__Impl1083);
            	    rule__Poll__QuestionsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getPollAccess().getQuestionsAssignment_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__3__Impl"


    // $ANTLR start "rule__Poll__Group__4"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:557:1: rule__Poll__Group__4 : rule__Poll__Group__4__Impl ;
    public final void rule__Poll__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:561:1: ( rule__Poll__Group__4__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:562:2: rule__Poll__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Poll__Group__4__Impl_in_rule__Poll__Group__41116);
            rule__Poll__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__4"


    // $ANTLR start "rule__Poll__Group__4__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:568:1: rule__Poll__Group__4__Impl : ( '}' ) ;
    public final void rule__Poll__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:572:1: ( ( '}' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:573:1: ( '}' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:573:1: ( '}' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:574:1: '}'
            {
             before(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); 
            match(input,18,FOLLOW_18_in_rule__Poll__Group__4__Impl1144); 
             after(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__4__Impl"


    // $ANTLR start "rule__Question__Group__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:597:1: rule__Question__Group__0 : rule__Question__Group__0__Impl rule__Question__Group__1 ;
    public final void rule__Question__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:601:1: ( rule__Question__Group__0__Impl rule__Question__Group__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:602:2: rule__Question__Group__0__Impl rule__Question__Group__1
            {
            pushFollow(FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__01185);
            rule__Question__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__1_in_rule__Question__Group__01188);
            rule__Question__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0"


    // $ANTLR start "rule__Question__Group__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:609:1: rule__Question__Group__0__Impl : ( 'Question' ) ;
    public final void rule__Question__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:613:1: ( ( 'Question' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:614:1: ( 'Question' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:614:1: ( 'Question' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:615:1: 'Question'
            {
             before(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__Question__Group__0__Impl1216); 
             after(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0__Impl"


    // $ANTLR start "rule__Question__Group__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:628:1: rule__Question__Group__1 : rule__Question__Group__1__Impl rule__Question__Group__2 ;
    public final void rule__Question__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:632:1: ( rule__Question__Group__1__Impl rule__Question__Group__2 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:633:2: rule__Question__Group__1__Impl rule__Question__Group__2
            {
            pushFollow(FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__11247);
            rule__Question__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__2_in_rule__Question__Group__11250);
            rule__Question__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1"


    // $ANTLR start "rule__Question__Group__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:640:1: rule__Question__Group__1__Impl : ( ( rule__Question__NameAssignment_1 )? ) ;
    public final void rule__Question__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:644:1: ( ( ( rule__Question__NameAssignment_1 )? ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:645:1: ( ( rule__Question__NameAssignment_1 )? )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:645:1: ( ( rule__Question__NameAssignment_1 )? )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:646:1: ( rule__Question__NameAssignment_1 )?
            {
             before(grammarAccess.getQuestionAccess().getNameAssignment_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:647:1: ( rule__Question__NameAssignment_1 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:647:2: rule__Question__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Question__NameAssignment_1_in_rule__Question__Group__1__Impl1277);
                    rule__Question__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuestionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1__Impl"


    // $ANTLR start "rule__Question__Group__2"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:657:1: rule__Question__Group__2 : rule__Question__Group__2__Impl rule__Question__Group__3 ;
    public final void rule__Question__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:661:1: ( rule__Question__Group__2__Impl rule__Question__Group__3 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:662:2: rule__Question__Group__2__Impl rule__Question__Group__3
            {
            pushFollow(FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__21308);
            rule__Question__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21311);
            rule__Question__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2"


    // $ANTLR start "rule__Question__Group__2__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:669:1: rule__Question__Group__2__Impl : ( '{' ) ;
    public final void rule__Question__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:673:1: ( ( '{' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:674:1: ( '{' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:674:1: ( '{' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:675:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__Question__Group__2__Impl1339); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2__Impl"


    // $ANTLR start "rule__Question__Group__3"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:688:1: rule__Question__Group__3 : rule__Question__Group__3__Impl rule__Question__Group__4 ;
    public final void rule__Question__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:692:1: ( rule__Question__Group__3__Impl rule__Question__Group__4 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:693:2: rule__Question__Group__3__Impl rule__Question__Group__4
            {
            pushFollow(FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31370);
            rule__Question__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31373);
            rule__Question__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3"


    // $ANTLR start "rule__Question__Group__3__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:700:1: rule__Question__Group__3__Impl : ( ( rule__Question__TextAssignment_3 ) ) ;
    public final void rule__Question__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:704:1: ( ( ( rule__Question__TextAssignment_3 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:705:1: ( ( rule__Question__TextAssignment_3 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:705:1: ( ( rule__Question__TextAssignment_3 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:706:1: ( rule__Question__TextAssignment_3 )
            {
             before(grammarAccess.getQuestionAccess().getTextAssignment_3()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:707:1: ( rule__Question__TextAssignment_3 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:707:2: rule__Question__TextAssignment_3
            {
            pushFollow(FOLLOW_rule__Question__TextAssignment_3_in_rule__Question__Group__3__Impl1400);
            rule__Question__TextAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getTextAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3__Impl"


    // $ANTLR start "rule__Question__Group__4"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:717:1: rule__Question__Group__4 : rule__Question__Group__4__Impl rule__Question__Group__5 ;
    public final void rule__Question__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:721:1: ( rule__Question__Group__4__Impl rule__Question__Group__5 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:722:2: rule__Question__Group__4__Impl rule__Question__Group__5
            {
            pushFollow(FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41430);
            rule__Question__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41433);
            rule__Question__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4"


    // $ANTLR start "rule__Question__Group__4__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:729:1: rule__Question__Group__4__Impl : ( ( rule__Question__Group_4__0 )? ) ;
    public final void rule__Question__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:733:1: ( ( ( rule__Question__Group_4__0 )? ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:734:1: ( ( rule__Question__Group_4__0 )? )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:734:1: ( ( rule__Question__Group_4__0 )? )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:735:1: ( rule__Question__Group_4__0 )?
            {
             before(grammarAccess.getQuestionAccess().getGroup_4()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:736:1: ( rule__Question__Group_4__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==21) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:736:2: rule__Question__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__Question__Group_4__0_in_rule__Question__Group__4__Impl1460);
                    rule__Question__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuestionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4__Impl"


    // $ANTLR start "rule__Question__Group__5"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:746:1: rule__Question__Group__5 : rule__Question__Group__5__Impl rule__Question__Group__6 ;
    public final void rule__Question__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:750:1: ( rule__Question__Group__5__Impl rule__Question__Group__6 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:751:2: rule__Question__Group__5__Impl rule__Question__Group__6
            {
            pushFollow(FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51491);
            rule__Question__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51494);
            rule__Question__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5"


    // $ANTLR start "rule__Question__Group__5__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:758:1: rule__Question__Group__5__Impl : ( 'options' ) ;
    public final void rule__Question__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:762:1: ( ( 'options' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:763:1: ( 'options' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:763:1: ( 'options' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:764:1: 'options'
            {
             before(grammarAccess.getQuestionAccess().getOptionsKeyword_5()); 
            match(input,20,FOLLOW_20_in_rule__Question__Group__5__Impl1522); 
             after(grammarAccess.getQuestionAccess().getOptionsKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5__Impl"


    // $ANTLR start "rule__Question__Group__6"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:777:1: rule__Question__Group__6 : rule__Question__Group__6__Impl rule__Question__Group__7 ;
    public final void rule__Question__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:781:1: ( rule__Question__Group__6__Impl rule__Question__Group__7 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:782:2: rule__Question__Group__6__Impl rule__Question__Group__7
            {
            pushFollow(FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61553);
            rule__Question__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61556);
            rule__Question__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6"


    // $ANTLR start "rule__Question__Group__6__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:789:1: rule__Question__Group__6__Impl : ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) ) ;
    public final void rule__Question__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:793:1: ( ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:794:1: ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:794:1: ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:795:1: ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:795:1: ( ( rule__Question__OptionsAssignment_6 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:796:1: ( rule__Question__OptionsAssignment_6 )
            {
             before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:797:1: ( rule__Question__OptionsAssignment_6 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:797:2: rule__Question__OptionsAssignment_6
            {
            pushFollow(FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1585);
            rule__Question__OptionsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 

            }

            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:800:1: ( ( rule__Question__OptionsAssignment_6 )* )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:801:1: ( rule__Question__OptionsAssignment_6 )*
            {
             before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:802:1: ( rule__Question__OptionsAssignment_6 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_ID && LA8_0<=RULE_STRING)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:802:2: rule__Question__OptionsAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1597);
            	    rule__Question__OptionsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6__Impl"


    // $ANTLR start "rule__Question__Group__7"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:813:1: rule__Question__Group__7 : rule__Question__Group__7__Impl ;
    public final void rule__Question__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:817:1: ( rule__Question__Group__7__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:818:2: rule__Question__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71630);
            rule__Question__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7"


    // $ANTLR start "rule__Question__Group__7__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:824:1: rule__Question__Group__7__Impl : ( '}' ) ;
    public final void rule__Question__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:828:1: ( ( '}' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:829:1: ( '}' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:829:1: ( '}' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:830:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); 
            match(input,18,FOLLOW_18_in_rule__Question__Group__7__Impl1658); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7__Impl"


    // $ANTLR start "rule__Question__Group_4__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:859:1: rule__Question__Group_4__0 : rule__Question__Group_4__0__Impl rule__Question__Group_4__1 ;
    public final void rule__Question__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:863:1: ( rule__Question__Group_4__0__Impl rule__Question__Group_4__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:864:2: rule__Question__Group_4__0__Impl rule__Question__Group_4__1
            {
            pushFollow(FOLLOW_rule__Question__Group_4__0__Impl_in_rule__Question__Group_4__01705);
            rule__Question__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group_4__1_in_rule__Question__Group_4__01708);
            rule__Question__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group_4__0"


    // $ANTLR start "rule__Question__Group_4__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:871:1: rule__Question__Group_4__0__Impl : ( 'type:' ) ;
    public final void rule__Question__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:875:1: ( ( 'type:' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:876:1: ( 'type:' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:876:1: ( 'type:' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:877:1: 'type:'
            {
             before(grammarAccess.getQuestionAccess().getTypeKeyword_4_0()); 
            match(input,21,FOLLOW_21_in_rule__Question__Group_4__0__Impl1736); 
             after(grammarAccess.getQuestionAccess().getTypeKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group_4__0__Impl"


    // $ANTLR start "rule__Question__Group_4__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:890:1: rule__Question__Group_4__1 : rule__Question__Group_4__1__Impl ;
    public final void rule__Question__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:894:1: ( rule__Question__Group_4__1__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:895:2: rule__Question__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__Question__Group_4__1__Impl_in_rule__Question__Group_4__11767);
            rule__Question__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group_4__1"


    // $ANTLR start "rule__Question__Group_4__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:901:1: rule__Question__Group_4__1__Impl : ( ( rule__Question__WidgetAssignment_4_1 ) ) ;
    public final void rule__Question__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:905:1: ( ( ( rule__Question__WidgetAssignment_4_1 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:906:1: ( ( rule__Question__WidgetAssignment_4_1 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:906:1: ( ( rule__Question__WidgetAssignment_4_1 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:907:1: ( rule__Question__WidgetAssignment_4_1 )
            {
             before(grammarAccess.getQuestionAccess().getWidgetAssignment_4_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:908:1: ( rule__Question__WidgetAssignment_4_1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:908:2: rule__Question__WidgetAssignment_4_1
            {
            pushFollow(FOLLOW_rule__Question__WidgetAssignment_4_1_in_rule__Question__Group_4__1__Impl1794);
            rule__Question__WidgetAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getWidgetAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group_4__1__Impl"


    // $ANTLR start "rule__Option__Group__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:922:1: rule__Option__Group__0 : rule__Option__Group__0__Impl rule__Option__Group__1 ;
    public final void rule__Option__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:926:1: ( rule__Option__Group__0__Impl rule__Option__Group__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:927:2: rule__Option__Group__0__Impl rule__Option__Group__1
            {
            pushFollow(FOLLOW_rule__Option__Group__0__Impl_in_rule__Option__Group__01828);
            rule__Option__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group__1_in_rule__Option__Group__01831);
            rule__Option__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__0"


    // $ANTLR start "rule__Option__Group__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:934:1: rule__Option__Group__0__Impl : ( ( rule__Option__Group_0__0 )? ) ;
    public final void rule__Option__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:938:1: ( ( ( rule__Option__Group_0__0 )? ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:939:1: ( ( rule__Option__Group_0__0 )? )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:939:1: ( ( rule__Option__Group_0__0 )? )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:940:1: ( rule__Option__Group_0__0 )?
            {
             before(grammarAccess.getOptionAccess().getGroup_0()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:941:1: ( rule__Option__Group_0__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:941:2: rule__Option__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Option__Group_0__0_in_rule__Option__Group__0__Impl1858);
                    rule__Option__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOptionAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__0__Impl"


    // $ANTLR start "rule__Option__Group__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:951:1: rule__Option__Group__1 : rule__Option__Group__1__Impl rule__Option__Group__2 ;
    public final void rule__Option__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:955:1: ( rule__Option__Group__1__Impl rule__Option__Group__2 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:956:2: rule__Option__Group__1__Impl rule__Option__Group__2
            {
            pushFollow(FOLLOW_rule__Option__Group__1__Impl_in_rule__Option__Group__11889);
            rule__Option__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group__2_in_rule__Option__Group__11892);
            rule__Option__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__1"


    // $ANTLR start "rule__Option__Group__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:963:1: rule__Option__Group__1__Impl : ( ( rule__Option__TextAssignment_1 ) ) ;
    public final void rule__Option__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:967:1: ( ( ( rule__Option__TextAssignment_1 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:968:1: ( ( rule__Option__TextAssignment_1 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:968:1: ( ( rule__Option__TextAssignment_1 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:969:1: ( rule__Option__TextAssignment_1 )
            {
             before(grammarAccess.getOptionAccess().getTextAssignment_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:970:1: ( rule__Option__TextAssignment_1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:970:2: rule__Option__TextAssignment_1
            {
            pushFollow(FOLLOW_rule__Option__TextAssignment_1_in_rule__Option__Group__1__Impl1919);
            rule__Option__TextAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getTextAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__1__Impl"


    // $ANTLR start "rule__Option__Group__2"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:980:1: rule__Option__Group__2 : rule__Option__Group__2__Impl ;
    public final void rule__Option__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:984:1: ( rule__Option__Group__2__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:985:2: rule__Option__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Option__Group__2__Impl_in_rule__Option__Group__21949);
            rule__Option__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__2"


    // $ANTLR start "rule__Option__Group__2__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:991:1: rule__Option__Group__2__Impl : ( ( rule__Option__Group_2__0 )? ) ;
    public final void rule__Option__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:995:1: ( ( ( rule__Option__Group_2__0 )? ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:996:1: ( ( rule__Option__Group_2__0 )? )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:996:1: ( ( rule__Option__Group_2__0 )? )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:997:1: ( rule__Option__Group_2__0 )?
            {
             before(grammarAccess.getOptionAccess().getGroup_2()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:998:1: ( rule__Option__Group_2__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:998:2: rule__Option__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Option__Group_2__0_in_rule__Option__Group__2__Impl1976);
                    rule__Option__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOptionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__2__Impl"


    // $ANTLR start "rule__Option__Group_0__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1014:1: rule__Option__Group_0__0 : rule__Option__Group_0__0__Impl rule__Option__Group_0__1 ;
    public final void rule__Option__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1018:1: ( rule__Option__Group_0__0__Impl rule__Option__Group_0__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1019:2: rule__Option__Group_0__0__Impl rule__Option__Group_0__1
            {
            pushFollow(FOLLOW_rule__Option__Group_0__0__Impl_in_rule__Option__Group_0__02013);
            rule__Option__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group_0__1_in_rule__Option__Group_0__02016);
            rule__Option__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__0"


    // $ANTLR start "rule__Option__Group_0__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1026:1: rule__Option__Group_0__0__Impl : ( ( rule__Option__NameAssignment_0_0 ) ) ;
    public final void rule__Option__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1030:1: ( ( ( rule__Option__NameAssignment_0_0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1031:1: ( ( rule__Option__NameAssignment_0_0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1031:1: ( ( rule__Option__NameAssignment_0_0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1032:1: ( rule__Option__NameAssignment_0_0 )
            {
             before(grammarAccess.getOptionAccess().getNameAssignment_0_0()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1033:1: ( rule__Option__NameAssignment_0_0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1033:2: rule__Option__NameAssignment_0_0
            {
            pushFollow(FOLLOW_rule__Option__NameAssignment_0_0_in_rule__Option__Group_0__0__Impl2043);
            rule__Option__NameAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getNameAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__0__Impl"


    // $ANTLR start "rule__Option__Group_0__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1043:1: rule__Option__Group_0__1 : rule__Option__Group_0__1__Impl ;
    public final void rule__Option__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1047:1: ( rule__Option__Group_0__1__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1048:2: rule__Option__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Option__Group_0__1__Impl_in_rule__Option__Group_0__12073);
            rule__Option__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__1"


    // $ANTLR start "rule__Option__Group_0__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1054:1: rule__Option__Group_0__1__Impl : ( '=>' ) ;
    public final void rule__Option__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1058:1: ( ( '=>' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1059:1: ( '=>' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1059:1: ( '=>' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1060:1: '=>'
            {
             before(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); 
            match(input,22,FOLLOW_22_in_rule__Option__Group_0__1__Impl2101); 
             after(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__1__Impl"


    // $ANTLR start "rule__Option__Group_2__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1077:1: rule__Option__Group_2__0 : rule__Option__Group_2__0__Impl rule__Option__Group_2__1 ;
    public final void rule__Option__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1081:1: ( rule__Option__Group_2__0__Impl rule__Option__Group_2__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1082:2: rule__Option__Group_2__0__Impl rule__Option__Group_2__1
            {
            pushFollow(FOLLOW_rule__Option__Group_2__0__Impl_in_rule__Option__Group_2__02136);
            rule__Option__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group_2__1_in_rule__Option__Group_2__02139);
            rule__Option__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_2__0"


    // $ANTLR start "rule__Option__Group_2__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1089:1: rule__Option__Group_2__0__Impl : ( 'type:' ) ;
    public final void rule__Option__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1093:1: ( ( 'type:' ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1094:1: ( 'type:' )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1094:1: ( 'type:' )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1095:1: 'type:'
            {
             before(grammarAccess.getOptionAccess().getTypeKeyword_2_0()); 
            match(input,21,FOLLOW_21_in_rule__Option__Group_2__0__Impl2167); 
             after(grammarAccess.getOptionAccess().getTypeKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_2__0__Impl"


    // $ANTLR start "rule__Option__Group_2__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1108:1: rule__Option__Group_2__1 : rule__Option__Group_2__1__Impl ;
    public final void rule__Option__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1112:1: ( rule__Option__Group_2__1__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1113:2: rule__Option__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Option__Group_2__1__Impl_in_rule__Option__Group_2__12198);
            rule__Option__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_2__1"


    // $ANTLR start "rule__Option__Group_2__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1119:1: rule__Option__Group_2__1__Impl : ( ( rule__Option__WidgetAssignment_2_1 ) ) ;
    public final void rule__Option__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1123:1: ( ( ( rule__Option__WidgetAssignment_2_1 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1124:1: ( ( rule__Option__WidgetAssignment_2_1 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1124:1: ( ( rule__Option__WidgetAssignment_2_1 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1125:1: ( rule__Option__WidgetAssignment_2_1 )
            {
             before(grammarAccess.getOptionAccess().getWidgetAssignment_2_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1126:1: ( rule__Option__WidgetAssignment_2_1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1126:2: rule__Option__WidgetAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Option__WidgetAssignment_2_1_in_rule__Option__Group_2__1__Impl2225);
            rule__Option__WidgetAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getWidgetAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_2__1__Impl"


    // $ANTLR start "rule__WidgetOptType__Group__0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1140:1: rule__WidgetOptType__Group__0 : rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1 ;
    public final void rule__WidgetOptType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1144:1: ( rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1145:2: rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__0__Impl_in_rule__WidgetOptType__Group__02259);
            rule__WidgetOptType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptType__Group__1_in_rule__WidgetOptType__Group__02262);
            rule__WidgetOptType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__0"


    // $ANTLR start "rule__WidgetOptType__Group__0__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1152:1: rule__WidgetOptType__Group__0__Impl : ( ( rule__WidgetOptType__TypeAssignment_0 ) ) ;
    public final void rule__WidgetOptType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1156:1: ( ( ( rule__WidgetOptType__TypeAssignment_0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1157:1: ( ( rule__WidgetOptType__TypeAssignment_0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1157:1: ( ( rule__WidgetOptType__TypeAssignment_0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1158:1: ( rule__WidgetOptType__TypeAssignment_0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1159:1: ( rule__WidgetOptType__TypeAssignment_0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1159:2: rule__WidgetOptType__TypeAssignment_0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__TypeAssignment_0_in_rule__WidgetOptType__Group__0__Impl2289);
            rule__WidgetOptType__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__0__Impl"


    // $ANTLR start "rule__WidgetOptType__Group__1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1169:1: rule__WidgetOptType__Group__1 : rule__WidgetOptType__Group__1__Impl ;
    public final void rule__WidgetOptType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1173:1: ( rule__WidgetOptType__Group__1__Impl )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1174:2: rule__WidgetOptType__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__1__Impl_in_rule__WidgetOptType__Group__12319);
            rule__WidgetOptType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__1"


    // $ANTLR start "rule__WidgetOptType__Group__1__Impl"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1180:1: rule__WidgetOptType__Group__1__Impl : ( ( rule__WidgetOptType__ParamAssignment_1 ) ) ;
    public final void rule__WidgetOptType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1184:1: ( ( ( rule__WidgetOptType__ParamAssignment_1 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1185:1: ( ( rule__WidgetOptType__ParamAssignment_1 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1185:1: ( ( rule__WidgetOptType__ParamAssignment_1 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1186:1: ( rule__WidgetOptType__ParamAssignment_1 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1187:1: ( rule__WidgetOptType__ParamAssignment_1 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1187:2: rule__WidgetOptType__ParamAssignment_1
            {
            pushFollow(FOLLOW_rule__WidgetOptType__ParamAssignment_1_in_rule__WidgetOptType__Group__1__Impl2346);
            rule__WidgetOptType__ParamAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__1__Impl"


    // $ANTLR start "rule__PollSystem__PollsAssignment"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1202:1: rule__PollSystem__PollsAssignment : ( rulePoll ) ;
    public final void rule__PollSystem__PollsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1206:1: ( ( rulePoll ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1207:1: ( rulePoll )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1207:1: ( rulePoll )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1208:1: rulePoll
            {
             before(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_0()); 
            pushFollow(FOLLOW_rulePoll_in_rule__PollSystem__PollsAssignment2385);
            rulePoll();

            state._fsp--;

             after(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__PollsAssignment"


    // $ANTLR start "rule__Poll__NameAssignment_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1217:1: rule__Poll__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Poll__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1221:1: ( ( RULE_ID ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1222:1: ( RULE_ID )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1222:1: ( RULE_ID )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1223:1: RULE_ID
            {
             before(grammarAccess.getPollAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Poll__NameAssignment_12416); 
             after(grammarAccess.getPollAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__NameAssignment_1"


    // $ANTLR start "rule__Poll__QuestionsAssignment_3"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1232:1: rule__Poll__QuestionsAssignment_3 : ( ruleQuestion ) ;
    public final void rule__Poll__QuestionsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1236:1: ( ( ruleQuestion ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1237:1: ( ruleQuestion )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1237:1: ( ruleQuestion )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1238:1: ruleQuestion
            {
             before(grammarAccess.getPollAccess().getQuestionsQuestionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleQuestion_in_rule__Poll__QuestionsAssignment_32447);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getPollAccess().getQuestionsQuestionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__QuestionsAssignment_3"


    // $ANTLR start "rule__Question__NameAssignment_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1247:1: rule__Question__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Question__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1251:1: ( ( RULE_ID ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1252:1: ( RULE_ID )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1252:1: ( RULE_ID )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1253:1: RULE_ID
            {
             before(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Question__NameAssignment_12478); 
             after(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__NameAssignment_1"


    // $ANTLR start "rule__Question__TextAssignment_3"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1262:1: rule__Question__TextAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Question__TextAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1266:1: ( ( RULE_STRING ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1267:1: ( RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1267:1: ( RULE_STRING )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1268:1: RULE_STRING
            {
             before(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_32509); 
             after(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__TextAssignment_3"


    // $ANTLR start "rule__Question__WidgetAssignment_4_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1277:1: rule__Question__WidgetAssignment_4_1 : ( ruleWidgetQuestType ) ;
    public final void rule__Question__WidgetAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1281:1: ( ( ruleWidgetQuestType ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1282:1: ( ruleWidgetQuestType )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1282:1: ( ruleWidgetQuestType )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1283:1: ruleWidgetQuestType
            {
             before(grammarAccess.getQuestionAccess().getWidgetWidgetQuestTypeParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_rule__Question__WidgetAssignment_4_12540);
            ruleWidgetQuestType();

            state._fsp--;

             after(grammarAccess.getQuestionAccess().getWidgetWidgetQuestTypeParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__WidgetAssignment_4_1"


    // $ANTLR start "rule__Question__OptionsAssignment_6"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1292:1: rule__Question__OptionsAssignment_6 : ( ruleOption ) ;
    public final void rule__Question__OptionsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1296:1: ( ( ruleOption ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1297:1: ( ruleOption )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1297:1: ( ruleOption )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1298:1: ruleOption
            {
             before(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleOption_in_rule__Question__OptionsAssignment_62571);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__OptionsAssignment_6"


    // $ANTLR start "rule__Option__NameAssignment_0_0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1307:1: rule__Option__NameAssignment_0_0 : ( RULE_ID ) ;
    public final void rule__Option__NameAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1311:1: ( ( RULE_ID ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1312:1: ( RULE_ID )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1312:1: ( RULE_ID )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1313:1: RULE_ID
            {
             before(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Option__NameAssignment_0_02602); 
             after(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__NameAssignment_0_0"


    // $ANTLR start "rule__Option__TextAssignment_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1322:1: rule__Option__TextAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Option__TextAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1326:1: ( ( RULE_STRING ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1327:1: ( RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1327:1: ( RULE_STRING )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1328:1: RULE_STRING
            {
             before(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Option__TextAssignment_12633); 
             after(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__TextAssignment_1"


    // $ANTLR start "rule__Option__WidgetAssignment_2_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1337:1: rule__Option__WidgetAssignment_2_1 : ( ruleWidgetOptType ) ;
    public final void rule__Option__WidgetAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1341:1: ( ( ruleWidgetOptType ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1342:1: ( ruleWidgetOptType )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1342:1: ( ruleWidgetOptType )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1343:1: ruleWidgetOptType
            {
             before(grammarAccess.getOptionAccess().getWidgetWidgetOptTypeParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_rule__Option__WidgetAssignment_2_12664);
            ruleWidgetOptType();

            state._fsp--;

             after(grammarAccess.getOptionAccess().getWidgetWidgetOptTypeParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__WidgetAssignment_2_1"


    // $ANTLR start "rule__WidgetOptType__TypeAssignment_0"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1352:1: rule__WidgetOptType__TypeAssignment_0 : ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) ) ;
    public final void rule__WidgetOptType__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1356:1: ( ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1357:1: ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1357:1: ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1358:1: ( rule__WidgetOptType__TypeAlternatives_0_0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); 
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1359:1: ( rule__WidgetOptType__TypeAlternatives_0_0 )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1359:2: rule__WidgetOptType__TypeAlternatives_0_0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__TypeAlternatives_0_0_in_rule__WidgetOptType__TypeAssignment_02695);
            rule__WidgetOptType__TypeAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__TypeAssignment_0"


    // $ANTLR start "rule__WidgetOptType__ParamAssignment_1"
    // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1368:1: rule__WidgetOptType__ParamAssignment_1 : ( RULE_STRING ) ;
    public final void rule__WidgetOptType__ParamAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1372:1: ( ( RULE_STRING ) )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1373:1: ( RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1373:1: ( RULE_STRING )
            // ../m2.idm.tp3.UIMM.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIMM.g:1374:1: RULE_STRING
            {
             before(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__WidgetOptType__ParamAssignment_12728); 
             after(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__ParamAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePollSystem_in_entryRulePollSystem61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePollSystem68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__PollsAssignment_in_rulePollSystem94 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rulePoll_in_entryRulePoll122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePoll129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__0_in_rulePoll155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__0_in_ruleQuestion215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__0_in_ruleOption275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuestType309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuestType__Alternatives_in_ruleWidgetQuestType335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptType369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__0_in_ruleWidgetOptType395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleImage456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox484 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleCheckbox518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_entryRuleRadio546 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadio553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleRadio580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_entryRuleSelect608 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelect615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleSelect642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput670 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleInput704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_rule__WidgetQuestType__Alternatives741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_rule__WidgetQuestType__Alternatives758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_rule__WidgetQuestType__Alternatives775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_rule__WidgetOptType__TypeAlternatives_0_0807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_rule__WidgetOptType__TypeAlternatives_0_0824 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__0__Impl_in_rule__Poll__Group__0854 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__Poll__Group__1_in_rule__Poll__Group__0857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Poll__Group__0__Impl885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__1__Impl_in_rule__Poll__Group__1916 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__Poll__Group__2_in_rule__Poll__Group__1919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__NameAssignment_1_in_rule__Poll__Group__1__Impl946 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__2__Impl_in_rule__Poll__Group__2977 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__Poll__Group__3_in_rule__Poll__Group__2980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Poll__Group__2__Impl1008 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__3__Impl_in_rule__Poll__Group__31039 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Poll__Group__4_in_rule__Poll__Group__31042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__QuestionsAssignment_3_in_rule__Poll__Group__3__Impl1071 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__Poll__QuestionsAssignment_3_in_rule__Poll__Group__3__Impl1083 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__Poll__Group__4__Impl_in_rule__Poll__Group__41116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Poll__Group__4__Impl1144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__01185 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__Question__Group__1_in_rule__Question__Group__01188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Question__Group__0__Impl1216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__11247 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__Question__Group__2_in_rule__Question__Group__11250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__NameAssignment_1_in_rule__Question__Group__1__Impl1277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__21308 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Question__Group__2__Impl1339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31370 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__TextAssignment_3_in_rule__Question__Group__3__Impl1400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41430 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group_4__0_in_rule__Question__Group__4__Impl1460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51491 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Question__Group__5__Impl1522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61553 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1585 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1597 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Question__Group__7__Impl1658 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group_4__0__Impl_in_rule__Question__Group_4__01705 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_rule__Question__Group_4__1_in_rule__Question__Group_4__01708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Question__Group_4__0__Impl1736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group_4__1__Impl_in_rule__Question__Group_4__11767 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__WidgetAssignment_4_1_in_rule__Question__Group_4__1__Impl1794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__0__Impl_in_rule__Option__Group__01828 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Option__Group__1_in_rule__Option__Group__01831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__0_in_rule__Option__Group__0__Impl1858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__1__Impl_in_rule__Option__Group__11889 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Option__Group__2_in_rule__Option__Group__11892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__TextAssignment_1_in_rule__Option__Group__1__Impl1919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__2__Impl_in_rule__Option__Group__21949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_2__0_in_rule__Option__Group__2__Impl1976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__0__Impl_in_rule__Option__Group_0__02013 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Option__Group_0__1_in_rule__Option__Group_0__02016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__NameAssignment_0_0_in_rule__Option__Group_0__0__Impl2043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__1__Impl_in_rule__Option__Group_0__12073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Option__Group_0__1__Impl2101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_2__0__Impl_in_rule__Option__Group_2__02136 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_rule__Option__Group_2__1_in_rule__Option__Group_2__02139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Option__Group_2__0__Impl2167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_2__1__Impl_in_rule__Option__Group_2__12198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__WidgetAssignment_2_1_in_rule__Option__Group_2__1__Impl2225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__0__Impl_in_rule__WidgetOptType__Group__02259 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__1_in_rule__WidgetOptType__Group__02262 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__TypeAssignment_0_in_rule__WidgetOptType__Group__0__Impl2289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__1__Impl_in_rule__WidgetOptType__Group__12319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__ParamAssignment_1_in_rule__WidgetOptType__Group__1__Impl2346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePoll_in_rule__PollSystem__PollsAssignment2385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Poll__NameAssignment_12416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_rule__Poll__QuestionsAssignment_32447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Question__NameAssignment_12478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_32509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_rule__Question__WidgetAssignment_4_12540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_rule__Question__OptionsAssignment_62571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Option__NameAssignment_0_02602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Option__TextAssignment_12633 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_rule__Option__WidgetAssignment_2_12664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__TypeAlternatives_0_0_in_rule__WidgetOptType__TypeAssignment_02695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__WidgetOptType__ParamAssignment_12728 = new BitSet(new long[]{0x0000000000000002L});

}