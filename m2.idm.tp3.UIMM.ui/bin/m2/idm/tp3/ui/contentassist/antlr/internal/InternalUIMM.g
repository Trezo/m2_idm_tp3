/*
* generated by Xtext
*/
grammar InternalUIMM;

options {
	superClass=AbstractInternalContentAssistParser;
	
}

@lexer::header {
package m2.idm.tp3.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package m2.idm.tp3.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import m2.idm.tp3.services.UIMMGrammarAccess;

}

@parser::members {
 
 	private UIMMGrammarAccess grammarAccess;
 	
    public void setGrammarAccess(UIMMGrammarAccess grammarAccess) {
    	this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected Grammar getGrammar() {
    	return grammarAccess.getGrammar();
    }
    
    @Override
    protected String getValueForTokenName(String tokenName) {
    	return tokenName;
    }

}




// Entry rule entryRulePollSystem
entryRulePollSystem 
:
{ before(grammarAccess.getPollSystemRule()); }
	 rulePollSystem
{ after(grammarAccess.getPollSystemRule()); } 
	 EOF 
;

// Rule PollSystem
rulePollSystem
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getPollSystemAccess().getPollsAssignment()); }
(rule__PollSystem__PollsAssignment)*
{ after(grammarAccess.getPollSystemAccess().getPollsAssignment()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRulePoll
entryRulePoll 
:
{ before(grammarAccess.getPollRule()); }
	 rulePoll
{ after(grammarAccess.getPollRule()); } 
	 EOF 
;

// Rule Poll
rulePoll
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getPollAccess().getGroup()); }
(rule__Poll__Group__0)
{ after(grammarAccess.getPollAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleQuestion
entryRuleQuestion 
:
{ before(grammarAccess.getQuestionRule()); }
	 ruleQuestion
{ after(grammarAccess.getQuestionRule()); } 
	 EOF 
;

// Rule Question
ruleQuestion
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getQuestionAccess().getGroup()); }
(rule__Question__Group__0)
{ after(grammarAccess.getQuestionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleOption
entryRuleOption 
:
{ before(grammarAccess.getOptionRule()); }
	 ruleOption
{ after(grammarAccess.getOptionRule()); } 
	 EOF 
;

// Rule Option
ruleOption
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getOptionAccess().getGroup()); }
(rule__Option__Group__0)
{ after(grammarAccess.getOptionAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleWidgetQuestType
entryRuleWidgetQuestType 
:
{ before(grammarAccess.getWidgetQuestTypeRule()); }
	 ruleWidgetQuestType
{ after(grammarAccess.getWidgetQuestTypeRule()); } 
	 EOF 
;

// Rule WidgetQuestType
ruleWidgetQuestType
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); }
(rule__WidgetQuestType__Alternatives)
{ after(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleWidgetOptType
entryRuleWidgetOptType 
:
{ before(grammarAccess.getWidgetOptTypeRule()); }
	 ruleWidgetOptType
{ after(grammarAccess.getWidgetOptTypeRule()); } 
	 EOF 
;

// Rule WidgetOptType
ruleWidgetOptType
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getGroup()); }
(rule__WidgetOptType__Group__0)
{ after(grammarAccess.getWidgetOptTypeAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleImage
entryRuleImage 
:
{ before(grammarAccess.getImageRule()); }
	 ruleImage
{ after(grammarAccess.getImageRule()); } 
	 EOF 
;

// Rule Image
ruleImage
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getImageAccess().getImageKeyword()); }

	'Image' 

{ after(grammarAccess.getImageAccess().getImageKeyword()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCheckbox
entryRuleCheckbox 
:
{ before(grammarAccess.getCheckboxRule()); }
	 ruleCheckbox
{ after(grammarAccess.getCheckboxRule()); } 
	 EOF 
;

// Rule Checkbox
ruleCheckbox
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); }

	'Checkbox' 

{ after(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRadio
entryRuleRadio 
:
{ before(grammarAccess.getRadioRule()); }
	 ruleRadio
{ after(grammarAccess.getRadioRule()); } 
	 EOF 
;

// Rule Radio
ruleRadio
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRadioAccess().getRadioKeyword()); }

	'Radio' 

{ after(grammarAccess.getRadioAccess().getRadioKeyword()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleSelect
entryRuleSelect 
:
{ before(grammarAccess.getSelectRule()); }
	 ruleSelect
{ after(grammarAccess.getSelectRule()); } 
	 EOF 
;

// Rule Select
ruleSelect
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getSelectAccess().getSelectKeyword()); }

	'Select' 

{ after(grammarAccess.getSelectAccess().getSelectKeyword()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleInput
entryRuleInput 
:
{ before(grammarAccess.getInputRule()); }
	 ruleInput
{ after(grammarAccess.getInputRule()); } 
	 EOF 
;

// Rule Input
ruleInput
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getInputAccess().getInputKeyword()); }

	'Input' 

{ after(grammarAccess.getInputAccess().getInputKeyword()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__WidgetQuestType__Alternatives
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); }
	ruleCheckbox
{ after(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); }
)

    |(
{ before(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); }
	ruleRadio
{ after(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); }
)

    |(
{ before(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); }
	ruleSelect
{ after(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WidgetOptType__TypeAlternatives_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); }
	ruleImage
{ after(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); }
)

    |(
{ before(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); }
	ruleInput
{ after(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__Poll__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Poll__Group__0__Impl
	rule__Poll__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getPollKeyword_0()); }

	'Poll' 

{ after(grammarAccess.getPollAccess().getPollKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Poll__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Poll__Group__1__Impl
	rule__Poll__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getNameAssignment_1()); }
(rule__Poll__NameAssignment_1)?
{ after(grammarAccess.getPollAccess().getNameAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Poll__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Poll__Group__2__Impl
	rule__Poll__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); }

	'{' 

{ after(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Poll__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Poll__Group__3__Impl
	rule__Poll__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
(
{ before(grammarAccess.getPollAccess().getQuestionsAssignment_3()); }
(rule__Poll__QuestionsAssignment_3)
{ after(grammarAccess.getPollAccess().getQuestionsAssignment_3()); }
)
(
{ before(grammarAccess.getPollAccess().getQuestionsAssignment_3()); }
(rule__Poll__QuestionsAssignment_3)*
{ after(grammarAccess.getPollAccess().getQuestionsAssignment_3()); }
)
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Poll__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Poll__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); }

	'}' 

{ after(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__Question__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__0__Impl
	rule__Question__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); }

	'Question' 

{ after(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__1__Impl
	rule__Question__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getNameAssignment_1()); }
(rule__Question__NameAssignment_1)?
{ after(grammarAccess.getQuestionAccess().getNameAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__2__Impl
	rule__Question__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); }

	'{' 

{ after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__3__Impl
	rule__Question__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getTextAssignment_3()); }
(rule__Question__TextAssignment_3)
{ after(grammarAccess.getQuestionAccess().getTextAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__4__Impl
	rule__Question__Group__5
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getGroup_4()); }
(rule__Question__Group_4__0)?
{ after(grammarAccess.getQuestionAccess().getGroup_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__5
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__5__Impl
	rule__Question__Group__6
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__5__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getOptionsKeyword_5()); }

	'options' 

{ after(grammarAccess.getQuestionAccess().getOptionsKeyword_5()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__6
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__6__Impl
	rule__Question__Group__7
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__6__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
(
{ before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); }
(rule__Question__OptionsAssignment_6)
{ after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); }
)
(
{ before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); }
(rule__Question__OptionsAssignment_6)*
{ after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); }
)
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group__7
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group__7__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group__7__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); }

	'}' 

{ after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); }
)

;
finally {
	restoreStackSize(stackSize);
}


















rule__Question__Group_4__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group_4__0__Impl
	rule__Question__Group_4__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group_4__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getTypeKeyword_4_0()); }

	'type:' 

{ after(grammarAccess.getQuestionAccess().getTypeKeyword_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Question__Group_4__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Question__Group_4__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Question__Group_4__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getWidgetAssignment_4_1()); }
(rule__Question__WidgetAssignment_4_1)
{ after(grammarAccess.getQuestionAccess().getWidgetAssignment_4_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Option__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group__0__Impl
	rule__Option__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getGroup_0()); }
(rule__Option__Group_0__0)?
{ after(grammarAccess.getOptionAccess().getGroup_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Option__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group__1__Impl
	rule__Option__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getTextAssignment_1()); }
(rule__Option__TextAssignment_1)
{ after(grammarAccess.getOptionAccess().getTextAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Option__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getGroup_2()); }
(rule__Option__Group_2__0)?
{ after(grammarAccess.getOptionAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Option__Group_0__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group_0__0__Impl
	rule__Option__Group_0__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group_0__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getNameAssignment_0_0()); }
(rule__Option__NameAssignment_0_0)
{ after(grammarAccess.getOptionAccess().getNameAssignment_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Option__Group_0__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group_0__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group_0__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); }

	'=>' 

{ after(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Option__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group_2__0__Impl
	rule__Option__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getTypeKeyword_2_0()); }

	'type:' 

{ after(grammarAccess.getOptionAccess().getTypeKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Option__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Option__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Option__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getWidgetAssignment_2_1()); }
(rule__Option__WidgetAssignment_2_1)
{ after(grammarAccess.getOptionAccess().getWidgetAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__WidgetOptType__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WidgetOptType__Group__0__Impl
	rule__WidgetOptType__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__WidgetOptType__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); }
(rule__WidgetOptType__TypeAssignment_0)
{ after(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__WidgetOptType__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__WidgetOptType__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__WidgetOptType__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); }
(rule__WidgetOptType__ParamAssignment_1)
{ after(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__PollSystem__PollsAssignment
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_0()); }
	rulePoll{ after(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__NameAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getNameIDTerminalRuleCall_1_0()); }
	RULE_ID{ after(grammarAccess.getPollAccess().getNameIDTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Poll__QuestionsAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getPollAccess().getQuestionsQuestionParserRuleCall_3_0()); }
	ruleQuestion{ after(grammarAccess.getPollAccess().getQuestionsQuestionParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Question__NameAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_1_0()); }
	RULE_ID{ after(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Question__TextAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); }
	RULE_STRING{ after(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Question__WidgetAssignment_4_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getWidgetWidgetQuestTypeParserRuleCall_4_1_0()); }
	ruleWidgetQuestType{ after(grammarAccess.getQuestionAccess().getWidgetWidgetQuestTypeParserRuleCall_4_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Question__OptionsAssignment_6
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); }
	ruleOption{ after(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Option__NameAssignment_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0_0_0()); }
	RULE_ID{ after(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Option__TextAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Option__WidgetAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getOptionAccess().getWidgetWidgetOptTypeParserRuleCall_2_1_0()); }
	ruleWidgetOptType{ after(grammarAccess.getOptionAccess().getWidgetWidgetOptTypeParserRuleCall_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WidgetOptType__TypeAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); }
(rule__WidgetOptType__TypeAlternatives_0_0)
{ after(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__WidgetOptType__ParamAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


