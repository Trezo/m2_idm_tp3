/*
* generated by Xtext
*/
package m2.idm.tp3;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class UIQuestionnaireStandaloneSetup extends UIQuestionnaireStandaloneSetupGenerated{

	public static void doSetup() {
		new UIQuestionnaireStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

