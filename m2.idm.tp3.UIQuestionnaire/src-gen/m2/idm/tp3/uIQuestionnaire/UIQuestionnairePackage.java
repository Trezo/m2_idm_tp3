/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaireFactory
 * @model kind="package"
 * @generated
 */
public interface UIQuestionnairePackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "uIQuestionnaire";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.idm.m2/tp3/UIQuestionnaire";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "uIQuestionnaire";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  UIQuestionnairePackage eINSTANCE = m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl.init();

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl <em>UI Questionnaire</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getUIQuestionnaire()
   * @generated
   */
  int UI_QUESTIONNAIRE = 0;

  /**
   * The feature id for the '<em><b>Quest Widgets</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UI_QUESTIONNAIRE__QUEST_WIDGETS = 0;

  /**
   * The feature id for the '<em><b>Opt Quest Widget</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UI_QUESTIONNAIRE__OPT_QUEST_WIDGET = 1;

  /**
   * The feature id for the '<em><b>Opt Widget</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UI_QUESTIONNAIRE__OPT_WIDGET = 2;

  /**
   * The number of structural features of the '<em>UI Questionnaire</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UI_QUESTIONNAIRE_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetQuestImpl <em>Widget Quest</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetQuestImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetQuest()
   * @generated
   */
  int WIDGET_QUEST = 1;

  /**
   * The feature id for the '<em><b>Question</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_QUEST__QUESTION = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_QUEST__TYPE = 1;

  /**
   * The number of structural features of the '<em>Widget Quest</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_QUEST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl <em>Widget Opt Quest</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetOptQuest()
   * @generated
   */
  int WIDGET_OPT_QUEST = 2;

  /**
   * The feature id for the '<em><b>Question</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_QUEST__QUESTION = 0;

  /**
   * The feature id for the '<em><b>Option</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_QUEST__OPTION = 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_QUEST__TYPE = 2;

  /**
   * The number of structural features of the '<em>Widget Opt Quest</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_QUEST_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.OptQuestImpl <em>Opt Quest</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.OptQuestImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getOptQuest()
   * @generated
   */
  int OPT_QUEST = 3;

  /**
   * The feature id for the '<em><b>Option</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_QUEST__OPTION = 0;

  /**
   * The feature id for the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_QUEST__TYPE = 1;

  /**
   * The number of structural features of the '<em>Opt Quest</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPT_QUEST_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.QuestionImpl <em>Question</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.QuestionImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getQuestion()
   * @generated
   */
  int QUESTION = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION__NAME = 0;

  /**
   * The number of structural features of the '<em>Question</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.OptionImpl <em>Option</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.OptionImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getOption()
   * @generated
   */
  int OPTION = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTION__NAME = 0;

  /**
   * The number of structural features of the '<em>Option</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPTION_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptTypeImpl <em>Widget Opt Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetOptTypeImpl
   * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetOptType()
   * @generated
   */
  int WIDGET_OPT_TYPE = 6;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_TYPE__TYPE = 0;

  /**
   * The feature id for the '<em><b>Param</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_TYPE__PARAM = 1;

  /**
   * The number of structural features of the '<em>Widget Opt Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_OPT_TYPE_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire <em>UI Questionnaire</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>UI Questionnaire</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaire
   * @generated
   */
  EClass getUIQuestionnaire();

  /**
   * Returns the meta object for the containment reference list '{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getQuestWidgets <em>Quest Widgets</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Quest Widgets</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getQuestWidgets()
   * @see #getUIQuestionnaire()
   * @generated
   */
  EReference getUIQuestionnaire_QuestWidgets();

  /**
   * Returns the meta object for the containment reference list '{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptQuestWidget <em>Opt Quest Widget</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Opt Quest Widget</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptQuestWidget()
   * @see #getUIQuestionnaire()
   * @generated
   */
  EReference getUIQuestionnaire_OptQuestWidget();

  /**
   * Returns the meta object for the containment reference list '{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptWidget <em>Opt Widget</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Opt Widget</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptWidget()
   * @see #getUIQuestionnaire()
   * @generated
   */
  EReference getUIQuestionnaire_OptWidget();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest <em>Widget Quest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Widget Quest</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetQuest
   * @generated
   */
  EClass getWidgetQuest();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getQuestion <em>Question</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Question</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetQuest#getQuestion()
   * @see #getWidgetQuest()
   * @generated
   */
  EReference getWidgetQuest_Question();

  /**
   * Returns the meta object for the attribute '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetQuest#getType()
   * @see #getWidgetQuest()
   * @generated
   */
  EAttribute getWidgetQuest_Type();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest <em>Widget Opt Quest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Widget Opt Quest</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptQuest
   * @generated
   */
  EClass getWidgetOptQuest();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getQuestion <em>Question</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Question</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getQuestion()
   * @see #getWidgetOptQuest()
   * @generated
   */
  EReference getWidgetOptQuest_Question();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getOption <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Option</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getOption()
   * @see #getWidgetOptQuest()
   * @generated
   */
  EReference getWidgetOptQuest_Option();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptQuest#getType()
   * @see #getWidgetOptQuest()
   * @generated
   */
  EReference getWidgetOptQuest_Type();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.OptQuest <em>Opt Quest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Opt Quest</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.OptQuest
   * @generated
   */
  EClass getOptQuest();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getOption <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Option</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.OptQuest#getOption()
   * @see #getOptQuest()
   * @generated
   */
  EReference getOptQuest_Option();

  /**
   * Returns the meta object for the containment reference '{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Type</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.OptQuest#getType()
   * @see #getOptQuest()
   * @generated
   */
  EReference getOptQuest_Type();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.Question <em>Question</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Question</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.Question
   * @generated
   */
  EClass getQuestion();

  /**
   * Returns the meta object for the attribute '{@link m2.idm.tp3.uIQuestionnaire.Question#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.Question#getName()
   * @see #getQuestion()
   * @generated
   */
  EAttribute getQuestion_Name();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.Option <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Option</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.Option
   * @generated
   */
  EClass getOption();

  /**
   * Returns the meta object for the attribute '{@link m2.idm.tp3.uIQuestionnaire.Option#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.Option#getName()
   * @see #getOption()
   * @generated
   */
  EAttribute getOption_Name();

  /**
   * Returns the meta object for class '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptType <em>Widget Opt Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Widget Opt Type</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptType
   * @generated
   */
  EClass getWidgetOptType();

  /**
   * Returns the meta object for the attribute '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptType#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptType#getType()
   * @see #getWidgetOptType()
   * @generated
   */
  EAttribute getWidgetOptType_Type();

  /**
   * Returns the meta object for the attribute '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptType#getParam <em>Param</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Param</em>'.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptType#getParam()
   * @see #getWidgetOptType()
   * @generated
   */
  EAttribute getWidgetOptType_Param();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  UIQuestionnaireFactory getUIQuestionnaireFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl <em>UI Questionnaire</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getUIQuestionnaire()
     * @generated
     */
    EClass UI_QUESTIONNAIRE = eINSTANCE.getUIQuestionnaire();

    /**
     * The meta object literal for the '<em><b>Quest Widgets</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UI_QUESTIONNAIRE__QUEST_WIDGETS = eINSTANCE.getUIQuestionnaire_QuestWidgets();

    /**
     * The meta object literal for the '<em><b>Opt Quest Widget</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UI_QUESTIONNAIRE__OPT_QUEST_WIDGET = eINSTANCE.getUIQuestionnaire_OptQuestWidget();

    /**
     * The meta object literal for the '<em><b>Opt Widget</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UI_QUESTIONNAIRE__OPT_WIDGET = eINSTANCE.getUIQuestionnaire_OptWidget();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetQuestImpl <em>Widget Quest</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetQuestImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetQuest()
     * @generated
     */
    EClass WIDGET_QUEST = eINSTANCE.getWidgetQuest();

    /**
     * The meta object literal for the '<em><b>Question</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WIDGET_QUEST__QUESTION = eINSTANCE.getWidgetQuest_Question();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WIDGET_QUEST__TYPE = eINSTANCE.getWidgetQuest_Type();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl <em>Widget Opt Quest</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetOptQuest()
     * @generated
     */
    EClass WIDGET_OPT_QUEST = eINSTANCE.getWidgetOptQuest();

    /**
     * The meta object literal for the '<em><b>Question</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WIDGET_OPT_QUEST__QUESTION = eINSTANCE.getWidgetOptQuest_Question();

    /**
     * The meta object literal for the '<em><b>Option</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WIDGET_OPT_QUEST__OPTION = eINSTANCE.getWidgetOptQuest_Option();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WIDGET_OPT_QUEST__TYPE = eINSTANCE.getWidgetOptQuest_Type();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.OptQuestImpl <em>Opt Quest</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.OptQuestImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getOptQuest()
     * @generated
     */
    EClass OPT_QUEST = eINSTANCE.getOptQuest();

    /**
     * The meta object literal for the '<em><b>Option</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OPT_QUEST__OPTION = eINSTANCE.getOptQuest_Option();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OPT_QUEST__TYPE = eINSTANCE.getOptQuest_Type();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.QuestionImpl <em>Question</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.QuestionImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getQuestion()
     * @generated
     */
    EClass QUESTION = eINSTANCE.getQuestion();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUESTION__NAME = eINSTANCE.getQuestion_Name();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.OptionImpl <em>Option</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.OptionImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getOption()
     * @generated
     */
    EClass OPTION = eINSTANCE.getOption();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPTION__NAME = eINSTANCE.getOption_Name();

    /**
     * The meta object literal for the '{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptTypeImpl <em>Widget Opt Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see m2.idm.tp3.uIQuestionnaire.impl.WidgetOptTypeImpl
     * @see m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnairePackageImpl#getWidgetOptType()
     * @generated
     */
    EClass WIDGET_OPT_TYPE = eINSTANCE.getWidgetOptType();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WIDGET_OPT_TYPE__TYPE = eINSTANCE.getWidgetOptType_Type();

    /**
     * The meta object literal for the '<em><b>Param</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WIDGET_OPT_TYPE__PARAM = eINSTANCE.getWidgetOptType_Param();

  }

} //UIQuestionnairePackage
