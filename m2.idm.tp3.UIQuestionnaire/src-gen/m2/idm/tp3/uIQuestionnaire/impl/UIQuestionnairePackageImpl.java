/**
 */
package m2.idm.tp3.uIQuestionnaire.impl;

import m2.idm.tp3.uIQuestionnaire.OptQuest;
import m2.idm.tp3.uIQuestionnaire.Option;
import m2.idm.tp3.uIQuestionnaire.Question;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnaire;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnaireFactory;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage;
import m2.idm.tp3.uIQuestionnaire.WidgetOptQuest;
import m2.idm.tp3.uIQuestionnaire.WidgetOptType;
import m2.idm.tp3.uIQuestionnaire.WidgetQuest;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UIQuestionnairePackageImpl extends EPackageImpl implements UIQuestionnairePackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass uiQuestionnaireEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass widgetQuestEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass widgetOptQuestEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass optQuestEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass questionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass optionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass widgetOptTypeEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#eNS_URI
   * @see #init()
   * @generated
   */
  private UIQuestionnairePackageImpl()
  {
    super(eNS_URI, UIQuestionnaireFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link UIQuestionnairePackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static UIQuestionnairePackage init()
  {
    if (isInited) return (UIQuestionnairePackage)EPackage.Registry.INSTANCE.getEPackage(UIQuestionnairePackage.eNS_URI);

    // Obtain or create and register package
    UIQuestionnairePackageImpl theUIQuestionnairePackage = (UIQuestionnairePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UIQuestionnairePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UIQuestionnairePackageImpl());

    isInited = true;

    // Create package meta-data objects
    theUIQuestionnairePackage.createPackageContents();

    // Initialize created meta-data
    theUIQuestionnairePackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theUIQuestionnairePackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(UIQuestionnairePackage.eNS_URI, theUIQuestionnairePackage);
    return theUIQuestionnairePackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getUIQuestionnaire()
  {
    return uiQuestionnaireEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUIQuestionnaire_QuestWidgets()
  {
    return (EReference)uiQuestionnaireEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUIQuestionnaire_OptQuestWidget()
  {
    return (EReference)uiQuestionnaireEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getUIQuestionnaire_OptWidget()
  {
    return (EReference)uiQuestionnaireEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWidgetQuest()
  {
    return widgetQuestEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWidgetQuest_Question()
  {
    return (EReference)widgetQuestEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getWidgetQuest_Type()
  {
    return (EAttribute)widgetQuestEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWidgetOptQuest()
  {
    return widgetOptQuestEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWidgetOptQuest_Question()
  {
    return (EReference)widgetOptQuestEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWidgetOptQuest_Option()
  {
    return (EReference)widgetOptQuestEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWidgetOptQuest_Type()
  {
    return (EReference)widgetOptQuestEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOptQuest()
  {
    return optQuestEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOptQuest_Option()
  {
    return (EReference)optQuestEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOptQuest_Type()
  {
    return (EReference)optQuestEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getQuestion()
  {
    return questionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getQuestion_Name()
  {
    return (EAttribute)questionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOption()
  {
    return optionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOption_Name()
  {
    return (EAttribute)optionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWidgetOptType()
  {
    return widgetOptTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getWidgetOptType_Type()
  {
    return (EAttribute)widgetOptTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getWidgetOptType_Param()
  {
    return (EAttribute)widgetOptTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UIQuestionnaireFactory getUIQuestionnaireFactory()
  {
    return (UIQuestionnaireFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    uiQuestionnaireEClass = createEClass(UI_QUESTIONNAIRE);
    createEReference(uiQuestionnaireEClass, UI_QUESTIONNAIRE__QUEST_WIDGETS);
    createEReference(uiQuestionnaireEClass, UI_QUESTIONNAIRE__OPT_QUEST_WIDGET);
    createEReference(uiQuestionnaireEClass, UI_QUESTIONNAIRE__OPT_WIDGET);

    widgetQuestEClass = createEClass(WIDGET_QUEST);
    createEReference(widgetQuestEClass, WIDGET_QUEST__QUESTION);
    createEAttribute(widgetQuestEClass, WIDGET_QUEST__TYPE);

    widgetOptQuestEClass = createEClass(WIDGET_OPT_QUEST);
    createEReference(widgetOptQuestEClass, WIDGET_OPT_QUEST__QUESTION);
    createEReference(widgetOptQuestEClass, WIDGET_OPT_QUEST__OPTION);
    createEReference(widgetOptQuestEClass, WIDGET_OPT_QUEST__TYPE);

    optQuestEClass = createEClass(OPT_QUEST);
    createEReference(optQuestEClass, OPT_QUEST__OPTION);
    createEReference(optQuestEClass, OPT_QUEST__TYPE);

    questionEClass = createEClass(QUESTION);
    createEAttribute(questionEClass, QUESTION__NAME);

    optionEClass = createEClass(OPTION);
    createEAttribute(optionEClass, OPTION__NAME);

    widgetOptTypeEClass = createEClass(WIDGET_OPT_TYPE);
    createEAttribute(widgetOptTypeEClass, WIDGET_OPT_TYPE__TYPE);
    createEAttribute(widgetOptTypeEClass, WIDGET_OPT_TYPE__PARAM);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(uiQuestionnaireEClass, UIQuestionnaire.class, "UIQuestionnaire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getUIQuestionnaire_QuestWidgets(), this.getWidgetQuest(), null, "questWidgets", null, 0, -1, UIQuestionnaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getUIQuestionnaire_OptQuestWidget(), this.getWidgetOptQuest(), null, "optQuestWidget", null, 0, -1, UIQuestionnaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getUIQuestionnaire_OptWidget(), this.getOptQuest(), null, "optWidget", null, 0, -1, UIQuestionnaire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(widgetQuestEClass, WidgetQuest.class, "WidgetQuest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWidgetQuest_Question(), this.getQuestion(), null, "question", null, 0, 1, WidgetQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getWidgetQuest_Type(), ecorePackage.getEString(), "type", null, 0, 1, WidgetQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(widgetOptQuestEClass, WidgetOptQuest.class, "WidgetOptQuest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWidgetOptQuest_Question(), this.getQuestion(), null, "question", null, 0, 1, WidgetOptQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getWidgetOptQuest_Option(), this.getOption(), null, "option", null, 0, 1, WidgetOptQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getWidgetOptQuest_Type(), this.getWidgetOptType(), null, "type", null, 0, 1, WidgetOptQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(optQuestEClass, OptQuest.class, "OptQuest", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOptQuest_Option(), this.getOption(), null, "option", null, 0, 1, OptQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOptQuest_Type(), this.getWidgetOptType(), null, "type", null, 0, 1, OptQuest.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(questionEClass, Question.class, "Question", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getQuestion_Name(), ecorePackage.getEString(), "name", null, 0, 1, Question.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(optionEClass, Option.class, "Option", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOption_Name(), ecorePackage.getEString(), "name", null, 0, 1, Option.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(widgetOptTypeEClass, WidgetOptType.class, "WidgetOptType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getWidgetOptType_Type(), ecorePackage.getEString(), "type", null, 0, 1, WidgetOptType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getWidgetOptType_Param(), ecorePackage.getEString(), "param", null, 0, 1, WidgetOptType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //UIQuestionnairePackageImpl
