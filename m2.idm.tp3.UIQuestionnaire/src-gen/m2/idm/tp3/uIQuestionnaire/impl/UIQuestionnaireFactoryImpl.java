/**
 */
package m2.idm.tp3.uIQuestionnaire.impl;

import m2.idm.tp3.uIQuestionnaire.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UIQuestionnaireFactoryImpl extends EFactoryImpl implements UIQuestionnaireFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static UIQuestionnaireFactory init()
  {
    try
    {
      UIQuestionnaireFactory theUIQuestionnaireFactory = (UIQuestionnaireFactory)EPackage.Registry.INSTANCE.getEFactory(UIQuestionnairePackage.eNS_URI);
      if (theUIQuestionnaireFactory != null)
      {
        return theUIQuestionnaireFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new UIQuestionnaireFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UIQuestionnaireFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE: return createUIQuestionnaire();
      case UIQuestionnairePackage.WIDGET_QUEST: return createWidgetQuest();
      case UIQuestionnairePackage.WIDGET_OPT_QUEST: return createWidgetOptQuest();
      case UIQuestionnairePackage.OPT_QUEST: return createOptQuest();
      case UIQuestionnairePackage.QUESTION: return createQuestion();
      case UIQuestionnairePackage.OPTION: return createOption();
      case UIQuestionnairePackage.WIDGET_OPT_TYPE: return createWidgetOptType();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UIQuestionnaire createUIQuestionnaire()
  {
    UIQuestionnaireImpl uiQuestionnaire = new UIQuestionnaireImpl();
    return uiQuestionnaire;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WidgetQuest createWidgetQuest()
  {
    WidgetQuestImpl widgetQuest = new WidgetQuestImpl();
    return widgetQuest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WidgetOptQuest createWidgetOptQuest()
  {
    WidgetOptQuestImpl widgetOptQuest = new WidgetOptQuestImpl();
    return widgetOptQuest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OptQuest createOptQuest()
  {
    OptQuestImpl optQuest = new OptQuestImpl();
    return optQuest;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Question createQuestion()
  {
    QuestionImpl question = new QuestionImpl();
    return question;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Option createOption()
  {
    OptionImpl option = new OptionImpl();
    return option;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WidgetOptType createWidgetOptType()
  {
    WidgetOptTypeImpl widgetOptType = new WidgetOptTypeImpl();
    return widgetOptType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UIQuestionnairePackage getUIQuestionnairePackage()
  {
    return (UIQuestionnairePackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static UIQuestionnairePackage getPackage()
  {
    return UIQuestionnairePackage.eINSTANCE;
  }

} //UIQuestionnaireFactoryImpl
