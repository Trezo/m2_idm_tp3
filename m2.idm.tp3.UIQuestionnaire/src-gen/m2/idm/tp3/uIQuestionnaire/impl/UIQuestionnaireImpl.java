/**
 */
package m2.idm.tp3.uIQuestionnaire.impl;

import java.util.Collection;

import m2.idm.tp3.uIQuestionnaire.OptQuest;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnaire;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage;
import m2.idm.tp3.uIQuestionnaire.WidgetOptQuest;
import m2.idm.tp3.uIQuestionnaire.WidgetQuest;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UI Questionnaire</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl#getQuestWidgets <em>Quest Widgets</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl#getOptQuestWidget <em>Opt Quest Widget</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireImpl#getOptWidget <em>Opt Widget</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UIQuestionnaireImpl extends MinimalEObjectImpl.Container implements UIQuestionnaire
{
  /**
   * The cached value of the '{@link #getQuestWidgets() <em>Quest Widgets</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQuestWidgets()
   * @generated
   * @ordered
   */
  protected EList<WidgetQuest> questWidgets;

  /**
   * The cached value of the '{@link #getOptQuestWidget() <em>Opt Quest Widget</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOptQuestWidget()
   * @generated
   * @ordered
   */
  protected EList<WidgetOptQuest> optQuestWidget;

  /**
   * The cached value of the '{@link #getOptWidget() <em>Opt Widget</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOptWidget()
   * @generated
   * @ordered
   */
  protected EList<OptQuest> optWidget;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UIQuestionnaireImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return UIQuestionnairePackage.Literals.UI_QUESTIONNAIRE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<WidgetQuest> getQuestWidgets()
  {
    if (questWidgets == null)
    {
      questWidgets = new EObjectContainmentEList<WidgetQuest>(WidgetQuest.class, this, UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS);
    }
    return questWidgets;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<WidgetOptQuest> getOptQuestWidget()
  {
    if (optQuestWidget == null)
    {
      optQuestWidget = new EObjectContainmentEList<WidgetOptQuest>(WidgetOptQuest.class, this, UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET);
    }
    return optQuestWidget;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<OptQuest> getOptWidget()
  {
    if (optWidget == null)
    {
      optWidget = new EObjectContainmentEList<OptQuest>(OptQuest.class, this, UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET);
    }
    return optWidget;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS:
        return ((InternalEList<?>)getQuestWidgets()).basicRemove(otherEnd, msgs);
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET:
        return ((InternalEList<?>)getOptQuestWidget()).basicRemove(otherEnd, msgs);
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET:
        return ((InternalEList<?>)getOptWidget()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS:
        return getQuestWidgets();
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET:
        return getOptQuestWidget();
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET:
        return getOptWidget();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS:
        getQuestWidgets().clear();
        getQuestWidgets().addAll((Collection<? extends WidgetQuest>)newValue);
        return;
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET:
        getOptQuestWidget().clear();
        getOptQuestWidget().addAll((Collection<? extends WidgetOptQuest>)newValue);
        return;
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET:
        getOptWidget().clear();
        getOptWidget().addAll((Collection<? extends OptQuest>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS:
        getQuestWidgets().clear();
        return;
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET:
        getOptQuestWidget().clear();
        return;
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET:
        getOptWidget().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__QUEST_WIDGETS:
        return questWidgets != null && !questWidgets.isEmpty();
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_QUEST_WIDGET:
        return optQuestWidget != null && !optQuestWidget.isEmpty();
      case UIQuestionnairePackage.UI_QUESTIONNAIRE__OPT_WIDGET:
        return optWidget != null && !optWidget.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //UIQuestionnaireImpl
