/**
 */
package m2.idm.tp3.uIQuestionnaire.impl;

import m2.idm.tp3.uIQuestionnaire.Option;
import m2.idm.tp3.uIQuestionnaire.Question;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage;
import m2.idm.tp3.uIQuestionnaire.WidgetOptQuest;
import m2.idm.tp3.uIQuestionnaire.WidgetOptType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Widget Opt Quest</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl#getQuestion <em>Question</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl#getOption <em>Option</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.impl.WidgetOptQuestImpl#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class WidgetOptQuestImpl extends MinimalEObjectImpl.Container implements WidgetOptQuest
{
  /**
   * The cached value of the '{@link #getQuestion() <em>Question</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getQuestion()
   * @generated
   * @ordered
   */
  protected Question question;

  /**
   * The cached value of the '{@link #getOption() <em>Option</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOption()
   * @generated
   * @ordered
   */
  protected Option option;

  /**
   * The cached value of the '{@link #getType() <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getType()
   * @generated
   * @ordered
   */
  protected WidgetOptType type;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected WidgetOptQuestImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Question getQuestion()
  {
    return question;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetQuestion(Question newQuestion, NotificationChain msgs)
  {
    Question oldQuestion = question;
    question = newQuestion;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION, oldQuestion, newQuestion);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setQuestion(Question newQuestion)
  {
    if (newQuestion != question)
    {
      NotificationChain msgs = null;
      if (question != null)
        msgs = ((InternalEObject)question).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION, null, msgs);
      if (newQuestion != null)
        msgs = ((InternalEObject)newQuestion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION, null, msgs);
      msgs = basicSetQuestion(newQuestion, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION, newQuestion, newQuestion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Option getOption()
  {
    return option;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOption(Option newOption, NotificationChain msgs)
  {
    Option oldOption = option;
    option = newOption;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION, oldOption, newOption);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOption(Option newOption)
  {
    if (newOption != option)
    {
      NotificationChain msgs = null;
      if (option != null)
        msgs = ((InternalEObject)option).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION, null, msgs);
      if (newOption != null)
        msgs = ((InternalEObject)newOption).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION, null, msgs);
      msgs = basicSetOption(newOption, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION, newOption, newOption));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WidgetOptType getType()
  {
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetType(WidgetOptType newType, NotificationChain msgs)
  {
    WidgetOptType oldType = type;
    type = newType;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE, oldType, newType);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setType(WidgetOptType newType)
  {
    if (newType != type)
    {
      NotificationChain msgs = null;
      if (type != null)
        msgs = ((InternalEObject)type).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE, null, msgs);
      if (newType != null)
        msgs = ((InternalEObject)newType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE, null, msgs);
      msgs = basicSetType(newType, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE, newType, newType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION:
        return basicSetQuestion(null, msgs);
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION:
        return basicSetOption(null, msgs);
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE:
        return basicSetType(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION:
        return getQuestion();
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION:
        return getOption();
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE:
        return getType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION:
        setQuestion((Question)newValue);
        return;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION:
        setOption((Option)newValue);
        return;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE:
        setType((WidgetOptType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION:
        setQuestion((Question)null);
        return;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION:
        setOption((Option)null);
        return;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE:
        setType((WidgetOptType)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__QUESTION:
        return question != null;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__OPTION:
        return option != null;
      case UIQuestionnairePackage.WIDGET_OPT_QUEST__TYPE:
        return type != null;
    }
    return super.eIsSet(featureID);
  }

} //WidgetOptQuestImpl
