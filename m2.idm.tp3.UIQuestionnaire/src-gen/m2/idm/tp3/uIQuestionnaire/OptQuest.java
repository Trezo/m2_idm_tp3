/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Opt Quest</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getOption <em>Option</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getOptQuest()
 * @model
 * @generated
 */
public interface OptQuest extends EObject
{
  /**
   * Returns the value of the '<em><b>Option</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Option</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Option</em>' containment reference.
   * @see #setOption(Option)
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getOptQuest_Option()
   * @model containment="true"
   * @generated
   */
  Option getOption();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getOption <em>Option</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Option</em>' containment reference.
   * @see #getOption()
   * @generated
   */
  void setOption(Option value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(WidgetOptType)
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getOptQuest_Type()
   * @model containment="true"
   * @generated
   */
  WidgetOptType getType();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIQuestionnaire.OptQuest#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(WidgetOptType value);

} // OptQuest
