/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage
 * @generated
 */
public interface UIQuestionnaireFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  UIQuestionnaireFactory eINSTANCE = m2.idm.tp3.uIQuestionnaire.impl.UIQuestionnaireFactoryImpl.init();

  /**
   * Returns a new object of class '<em>UI Questionnaire</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>UI Questionnaire</em>'.
   * @generated
   */
  UIQuestionnaire createUIQuestionnaire();

  /**
   * Returns a new object of class '<em>Widget Quest</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Widget Quest</em>'.
   * @generated
   */
  WidgetQuest createWidgetQuest();

  /**
   * Returns a new object of class '<em>Widget Opt Quest</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Widget Opt Quest</em>'.
   * @generated
   */
  WidgetOptQuest createWidgetOptQuest();

  /**
   * Returns a new object of class '<em>Opt Quest</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Opt Quest</em>'.
   * @generated
   */
  OptQuest createOptQuest();

  /**
   * Returns a new object of class '<em>Question</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Question</em>'.
   * @generated
   */
  Question createQuestion();

  /**
   * Returns a new object of class '<em>Option</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Option</em>'.
   * @generated
   */
  Option createOption();

  /**
   * Returns a new object of class '<em>Widget Opt Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Widget Opt Type</em>'.
   * @generated
   */
  WidgetOptType createWidgetOptType();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  UIQuestionnairePackage getUIQuestionnairePackage();

} //UIQuestionnaireFactory
