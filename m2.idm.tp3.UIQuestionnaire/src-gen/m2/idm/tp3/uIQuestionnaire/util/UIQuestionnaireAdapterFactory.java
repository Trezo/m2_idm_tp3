/**
 */
package m2.idm.tp3.uIQuestionnaire.util;

import m2.idm.tp3.uIQuestionnaire.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage
 * @generated
 */
public class UIQuestionnaireAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static UIQuestionnairePackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UIQuestionnaireAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = UIQuestionnairePackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UIQuestionnaireSwitch<Adapter> modelSwitch =
    new UIQuestionnaireSwitch<Adapter>()
    {
      @Override
      public Adapter caseUIQuestionnaire(UIQuestionnaire object)
      {
        return createUIQuestionnaireAdapter();
      }
      @Override
      public Adapter caseWidgetQuest(WidgetQuest object)
      {
        return createWidgetQuestAdapter();
      }
      @Override
      public Adapter caseWidgetOptQuest(WidgetOptQuest object)
      {
        return createWidgetOptQuestAdapter();
      }
      @Override
      public Adapter caseOptQuest(OptQuest object)
      {
        return createOptQuestAdapter();
      }
      @Override
      public Adapter caseQuestion(Question object)
      {
        return createQuestionAdapter();
      }
      @Override
      public Adapter caseOption(Option object)
      {
        return createOptionAdapter();
      }
      @Override
      public Adapter caseWidgetOptType(WidgetOptType object)
      {
        return createWidgetOptTypeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire <em>UI Questionnaire</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnaire
   * @generated
   */
  public Adapter createUIQuestionnaireAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest <em>Widget Quest</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetQuest
   * @generated
   */
  public Adapter createWidgetQuestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest <em>Widget Opt Quest</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptQuest
   * @generated
   */
  public Adapter createWidgetOptQuestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.OptQuest <em>Opt Quest</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.OptQuest
   * @generated
   */
  public Adapter createOptQuestAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.Question <em>Question</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.Question
   * @generated
   */
  public Adapter createQuestionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.Option <em>Option</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.Option
   * @generated
   */
  public Adapter createOptionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link m2.idm.tp3.uIQuestionnaire.WidgetOptType <em>Widget Opt Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see m2.idm.tp3.uIQuestionnaire.WidgetOptType
   * @generated
   */
  public Adapter createWidgetOptTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //UIQuestionnaireAdapterFactory
