/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UI Questionnaire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getQuestWidgets <em>Quest Widgets</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptQuestWidget <em>Opt Quest Widget</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.UIQuestionnaire#getOptWidget <em>Opt Widget</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getUIQuestionnaire()
 * @model
 * @generated
 */
public interface UIQuestionnaire extends EObject
{
  /**
   * Returns the value of the '<em><b>Quest Widgets</b></em>' containment reference list.
   * The list contents are of type {@link m2.idm.tp3.uIQuestionnaire.WidgetQuest}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Quest Widgets</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Quest Widgets</em>' containment reference list.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getUIQuestionnaire_QuestWidgets()
   * @model containment="true"
   * @generated
   */
  EList<WidgetQuest> getQuestWidgets();

  /**
   * Returns the value of the '<em><b>Opt Quest Widget</b></em>' containment reference list.
   * The list contents are of type {@link m2.idm.tp3.uIQuestionnaire.WidgetOptQuest}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Opt Quest Widget</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Opt Quest Widget</em>' containment reference list.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getUIQuestionnaire_OptQuestWidget()
   * @model containment="true"
   * @generated
   */
  EList<WidgetOptQuest> getOptQuestWidget();

  /**
   * Returns the value of the '<em><b>Opt Widget</b></em>' containment reference list.
   * The list contents are of type {@link m2.idm.tp3.uIQuestionnaire.OptQuest}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Opt Widget</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Opt Widget</em>' containment reference list.
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getUIQuestionnaire_OptWidget()
   * @model containment="true"
   * @generated
   */
  EList<OptQuest> getOptWidget();

} // UIQuestionnaire
