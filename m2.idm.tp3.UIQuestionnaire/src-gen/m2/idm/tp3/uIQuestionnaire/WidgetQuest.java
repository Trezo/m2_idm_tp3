/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Widget Quest</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getQuestion <em>Question</em>}</li>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getWidgetQuest()
 * @model
 * @generated
 */
public interface WidgetQuest extends EObject
{
  /**
   * Returns the value of the '<em><b>Question</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Question</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Question</em>' containment reference.
   * @see #setQuestion(Question)
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getWidgetQuest_Question()
   * @model containment="true"
   * @generated
   */
  Question getQuestion();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getQuestion <em>Question</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Question</em>' containment reference.
   * @see #getQuestion()
   * @generated
   */
  void setQuestion(Question value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getWidgetQuest_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIQuestionnaire.WidgetQuest#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

} // WidgetQuest
