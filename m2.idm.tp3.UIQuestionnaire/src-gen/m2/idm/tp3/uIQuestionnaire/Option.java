/**
 */
package m2.idm.tp3.uIQuestionnaire;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIQuestionnaire.Option#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getOption()
 * @model
 * @generated
 */
public interface Option extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage#getOption_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIQuestionnaire.Option#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

} // Option
