package m2.idm.tp3.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import m2.idm.tp3.services.UIQuestionnaireGrammarAccess;
import m2.idm.tp3.uIQuestionnaire.OptQuest;
import m2.idm.tp3.uIQuestionnaire.Option;
import m2.idm.tp3.uIQuestionnaire.Question;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnaire;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnairePackage;
import m2.idm.tp3.uIQuestionnaire.WidgetOptQuest;
import m2.idm.tp3.uIQuestionnaire.WidgetOptType;
import m2.idm.tp3.uIQuestionnaire.WidgetQuest;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class UIQuestionnaireSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private UIQuestionnaireGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == UIQuestionnairePackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case UIQuestionnairePackage.OPT_QUEST:
				if(context == grammarAccess.getOptQuestRule()) {
					sequence_OptQuest(context, (OptQuest) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.OPTION:
				if(context == grammarAccess.getOptionRule()) {
					sequence_Option(context, (Option) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.QUESTION:
				if(context == grammarAccess.getQuestionRule()) {
					sequence_Question(context, (Question) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.UI_QUESTIONNAIRE:
				if(context == grammarAccess.getUIQuestionnaireRule()) {
					sequence_UIQuestionnaire(context, (UIQuestionnaire) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.WIDGET_OPT_QUEST:
				if(context == grammarAccess.getWidgetOptQuestRule()) {
					sequence_WidgetOptQuest(context, (WidgetOptQuest) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.WIDGET_OPT_TYPE:
				if(context == grammarAccess.getWidgetOptTypeRule()) {
					sequence_WidgetOptType(context, (WidgetOptType) semanticObject); 
					return; 
				}
				else break;
			case UIQuestionnairePackage.WIDGET_QUEST:
				if(context == grammarAccess.getWidgetQuestRule()) {
					sequence_WidgetQuest(context, (WidgetQuest) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (option=Option type=WidgetOptType)
	 */
	protected void sequence_OptQuest(EObject context, OptQuest semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.OPT_QUEST__OPTION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.OPT_QUEST__OPTION));
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.OPT_QUEST__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.OPT_QUEST__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOptQuestAccess().getOptionOptionParserRuleCall_0_0(), semanticObject.getOption());
		feeder.accept(grammarAccess.getOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_2_0(), semanticObject.getType());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Option(EObject context, Option semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.OPTION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.OPTION__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Question(EObject context, Question semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.QUESTION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.QUESTION__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (questWidgets+=WidgetQuest* optQuestWidget+=WidgetOptQuest* optWidget+=OptQuest*)
	 */
	protected void sequence_UIQuestionnaire(EObject context, UIQuestionnaire semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (question=Question option=Option type=WidgetOptType)
	 */
	protected void sequence_WidgetOptQuest(EObject context, WidgetOptQuest semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__QUESTION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__QUESTION));
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__OPTION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__OPTION));
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.WIDGET_OPT_QUEST__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWidgetOptQuestAccess().getQuestionQuestionParserRuleCall_0_0(), semanticObject.getQuestion());
		feeder.accept(grammarAccess.getWidgetOptQuestAccess().getOptionOptionParserRuleCall_2_0(), semanticObject.getOption());
		feeder.accept(grammarAccess.getWidgetOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_4_0(), semanticObject.getType());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     ((type=Image | type=Input) param=STRING)
	 */
	protected void sequence_WidgetOptType(EObject context, WidgetOptType semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (question=Question type=WidgetQuestType)
	 */
	protected void sequence_WidgetQuest(EObject context, WidgetQuest semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.WIDGET_QUEST__QUESTION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.WIDGET_QUEST__QUESTION));
			if(transientValues.isValueTransient(semanticObject, UIQuestionnairePackage.Literals.WIDGET_QUEST__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, UIQuestionnairePackage.Literals.WIDGET_QUEST__TYPE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getWidgetQuestAccess().getQuestionQuestionParserRuleCall_0_0(), semanticObject.getQuestion());
		feeder.accept(grammarAccess.getWidgetQuestAccess().getTypeWidgetQuestTypeParserRuleCall_2_0(), semanticObject.getType());
		feeder.finish();
	}
}
