package m2.idm.tp3.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import m2.idm.tp3.services.UIQuestionnaireGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUIQuestionnaireParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Questions{'", "'}'", "'Question:Options{'", "'Options{'", "':'", "'=>'", "'Image'", "'Checkbox'", "'Radio'", "'Select'", "'Input'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUIQuestionnaireParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUIQuestionnaireParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUIQuestionnaireParser.tokenNames; }
    public String getGrammarFileName() { return "../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g"; }



     	private UIQuestionnaireGrammarAccess grammarAccess;
     	
        public InternalUIQuestionnaireParser(TokenStream input, UIQuestionnaireGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "UIQuestionnaire";	
       	}
       	
       	@Override
       	protected UIQuestionnaireGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleUIQuestionnaire"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:67:1: entryRuleUIQuestionnaire returns [EObject current=null] : iv_ruleUIQuestionnaire= ruleUIQuestionnaire EOF ;
    public final EObject entryRuleUIQuestionnaire() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUIQuestionnaire = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:68:2: (iv_ruleUIQuestionnaire= ruleUIQuestionnaire EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:69:2: iv_ruleUIQuestionnaire= ruleUIQuestionnaire EOF
            {
             newCompositeNode(grammarAccess.getUIQuestionnaireRule()); 
            pushFollow(FOLLOW_ruleUIQuestionnaire_in_entryRuleUIQuestionnaire75);
            iv_ruleUIQuestionnaire=ruleUIQuestionnaire();

            state._fsp--;

             current =iv_ruleUIQuestionnaire; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUIQuestionnaire85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUIQuestionnaire"


    // $ANTLR start "ruleUIQuestionnaire"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:76:1: ruleUIQuestionnaire returns [EObject current=null] : (otherlv_0= 'Questions{' ( (lv_questWidgets_1_0= ruleWidgetQuest ) )* otherlv_2= '}' otherlv_3= 'Question:Options{' ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )* otherlv_5= '}' otherlv_6= 'Options{' ( (lv_optWidget_7_0= ruleOptQuest ) )* otherlv_8= '}' ) ;
    public final EObject ruleUIQuestionnaire() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_questWidgets_1_0 = null;

        EObject lv_optQuestWidget_4_0 = null;

        EObject lv_optWidget_7_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:79:28: ( (otherlv_0= 'Questions{' ( (lv_questWidgets_1_0= ruleWidgetQuest ) )* otherlv_2= '}' otherlv_3= 'Question:Options{' ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )* otherlv_5= '}' otherlv_6= 'Options{' ( (lv_optWidget_7_0= ruleOptQuest ) )* otherlv_8= '}' ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:80:1: (otherlv_0= 'Questions{' ( (lv_questWidgets_1_0= ruleWidgetQuest ) )* otherlv_2= '}' otherlv_3= 'Question:Options{' ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )* otherlv_5= '}' otherlv_6= 'Options{' ( (lv_optWidget_7_0= ruleOptQuest ) )* otherlv_8= '}' )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:80:1: (otherlv_0= 'Questions{' ( (lv_questWidgets_1_0= ruleWidgetQuest ) )* otherlv_2= '}' otherlv_3= 'Question:Options{' ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )* otherlv_5= '}' otherlv_6= 'Options{' ( (lv_optWidget_7_0= ruleOptQuest ) )* otherlv_8= '}' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:80:3: otherlv_0= 'Questions{' ( (lv_questWidgets_1_0= ruleWidgetQuest ) )* otherlv_2= '}' otherlv_3= 'Question:Options{' ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )* otherlv_5= '}' otherlv_6= 'Options{' ( (lv_optWidget_7_0= ruleOptQuest ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleUIQuestionnaire122); 

                	newLeafNode(otherlv_0, grammarAccess.getUIQuestionnaireAccess().getQuestionsKeyword_0());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:84:1: ( (lv_questWidgets_1_0= ruleWidgetQuest ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:85:1: (lv_questWidgets_1_0= ruleWidgetQuest )
            	    {
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:85:1: (lv_questWidgets_1_0= ruleWidgetQuest )
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:86:3: lv_questWidgets_1_0= ruleWidgetQuest
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getUIQuestionnaireAccess().getQuestWidgetsWidgetQuestParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleWidgetQuest_in_ruleUIQuestionnaire143);
            	    lv_questWidgets_1_0=ruleWidgetQuest();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getUIQuestionnaireRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"questWidgets",
            	            		lv_questWidgets_1_0, 
            	            		"WidgetQuest");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleUIQuestionnaire156); 

                	newLeafNode(otherlv_2, grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_2());
                
            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleUIQuestionnaire168); 

                	newLeafNode(otherlv_3, grammarAccess.getUIQuestionnaireAccess().getQuestionOptionsKeyword_3());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:110:1: ( (lv_optQuestWidget_4_0= ruleWidgetOptQuest ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:111:1: (lv_optQuestWidget_4_0= ruleWidgetOptQuest )
            	    {
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:111:1: (lv_optQuestWidget_4_0= ruleWidgetOptQuest )
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:112:3: lv_optQuestWidget_4_0= ruleWidgetOptQuest
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getUIQuestionnaireAccess().getOptQuestWidgetWidgetOptQuestParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleWidgetOptQuest_in_ruleUIQuestionnaire189);
            	    lv_optQuestWidget_4_0=ruleWidgetOptQuest();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getUIQuestionnaireRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"optQuestWidget",
            	            		lv_optQuestWidget_4_0, 
            	            		"WidgetOptQuest");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            otherlv_5=(Token)match(input,12,FOLLOW_12_in_ruleUIQuestionnaire202); 

                	newLeafNode(otherlv_5, grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_5());
                
            otherlv_6=(Token)match(input,14,FOLLOW_14_in_ruleUIQuestionnaire214); 

                	newLeafNode(otherlv_6, grammarAccess.getUIQuestionnaireAccess().getOptionsKeyword_6());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:136:1: ( (lv_optWidget_7_0= ruleOptQuest ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:137:1: (lv_optWidget_7_0= ruleOptQuest )
            	    {
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:137:1: (lv_optWidget_7_0= ruleOptQuest )
            	    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:138:3: lv_optWidget_7_0= ruleOptQuest
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getUIQuestionnaireAccess().getOptWidgetOptQuestParserRuleCall_7_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOptQuest_in_ruleUIQuestionnaire235);
            	    lv_optWidget_7_0=ruleOptQuest();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getUIQuestionnaireRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"optWidget",
            	            		lv_optWidget_7_0, 
            	            		"OptQuest");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_8=(Token)match(input,12,FOLLOW_12_in_ruleUIQuestionnaire248); 

                	newLeafNode(otherlv_8, grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUIQuestionnaire"


    // $ANTLR start "entryRuleWidgetQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:166:1: entryRuleWidgetQuest returns [EObject current=null] : iv_ruleWidgetQuest= ruleWidgetQuest EOF ;
    public final EObject entryRuleWidgetQuest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidgetQuest = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:167:2: (iv_ruleWidgetQuest= ruleWidgetQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:168:2: iv_ruleWidgetQuest= ruleWidgetQuest EOF
            {
             newCompositeNode(grammarAccess.getWidgetQuestRule()); 
            pushFollow(FOLLOW_ruleWidgetQuest_in_entryRuleWidgetQuest284);
            iv_ruleWidgetQuest=ruleWidgetQuest();

            state._fsp--;

             current =iv_ruleWidgetQuest; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuest294); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetQuest"


    // $ANTLR start "ruleWidgetQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:175:1: ruleWidgetQuest returns [EObject current=null] : ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetQuestType ) ) ) ;
    public final EObject ruleWidgetQuest() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_question_0_0 = null;

        AntlrDatatypeRuleToken lv_type_2_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:178:28: ( ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetQuestType ) ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:179:1: ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetQuestType ) ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:179:1: ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetQuestType ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:179:2: ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetQuestType ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:179:2: ( (lv_question_0_0= ruleQuestion ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:180:1: (lv_question_0_0= ruleQuestion )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:180:1: (lv_question_0_0= ruleQuestion )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:181:3: lv_question_0_0= ruleQuestion
            {
             
            	        newCompositeNode(grammarAccess.getWidgetQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleQuestion_in_ruleWidgetQuest340);
            lv_question_0_0=ruleQuestion();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWidgetQuestRule());
            	        }
                   		set(
                   			current, 
                   			"question",
                    		lv_question_0_0, 
                    		"Question");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleWidgetQuest352); 

                	newLeafNode(otherlv_1, grammarAccess.getWidgetQuestAccess().getColonKeyword_1());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:201:1: ( (lv_type_2_0= ruleWidgetQuestType ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:202:1: (lv_type_2_0= ruleWidgetQuestType )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:202:1: (lv_type_2_0= ruleWidgetQuestType )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:203:3: lv_type_2_0= ruleWidgetQuestType
            {
             
            	        newCompositeNode(grammarAccess.getWidgetQuestAccess().getTypeWidgetQuestTypeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleWidgetQuestType_in_ruleWidgetQuest373);
            lv_type_2_0=ruleWidgetQuestType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWidgetQuestRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_2_0, 
                    		"WidgetQuestType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetQuest"


    // $ANTLR start "entryRuleWidgetOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:227:1: entryRuleWidgetOptQuest returns [EObject current=null] : iv_ruleWidgetOptQuest= ruleWidgetOptQuest EOF ;
    public final EObject entryRuleWidgetOptQuest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidgetOptQuest = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:228:2: (iv_ruleWidgetOptQuest= ruleWidgetOptQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:229:2: iv_ruleWidgetOptQuest= ruleWidgetOptQuest EOF
            {
             newCompositeNode(grammarAccess.getWidgetOptQuestRule()); 
            pushFollow(FOLLOW_ruleWidgetOptQuest_in_entryRuleWidgetOptQuest409);
            iv_ruleWidgetOptQuest=ruleWidgetOptQuest();

            state._fsp--;

             current =iv_ruleWidgetOptQuest; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptQuest419); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetOptQuest"


    // $ANTLR start "ruleWidgetOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:236:1: ruleWidgetOptQuest returns [EObject current=null] : ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= '=>' ( (lv_option_2_0= ruleOption ) ) otherlv_3= ':' ( (lv_type_4_0= ruleWidgetOptType ) ) ) ;
    public final EObject ruleWidgetOptQuest() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_question_0_0 = null;

        EObject lv_option_2_0 = null;

        EObject lv_type_4_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:239:28: ( ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= '=>' ( (lv_option_2_0= ruleOption ) ) otherlv_3= ':' ( (lv_type_4_0= ruleWidgetOptType ) ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:240:1: ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= '=>' ( (lv_option_2_0= ruleOption ) ) otherlv_3= ':' ( (lv_type_4_0= ruleWidgetOptType ) ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:240:1: ( ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= '=>' ( (lv_option_2_0= ruleOption ) ) otherlv_3= ':' ( (lv_type_4_0= ruleWidgetOptType ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:240:2: ( (lv_question_0_0= ruleQuestion ) ) otherlv_1= '=>' ( (lv_option_2_0= ruleOption ) ) otherlv_3= ':' ( (lv_type_4_0= ruleWidgetOptType ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:240:2: ( (lv_question_0_0= ruleQuestion ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:241:1: (lv_question_0_0= ruleQuestion )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:241:1: (lv_question_0_0= ruleQuestion )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:242:3: lv_question_0_0= ruleQuestion
            {
             
            	        newCompositeNode(grammarAccess.getWidgetOptQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleQuestion_in_ruleWidgetOptQuest465);
            lv_question_0_0=ruleQuestion();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWidgetOptQuestRule());
            	        }
                   		set(
                   			current, 
                   			"question",
                    		lv_question_0_0, 
                    		"Question");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,16,FOLLOW_16_in_ruleWidgetOptQuest477); 

                	newLeafNode(otherlv_1, grammarAccess.getWidgetOptQuestAccess().getEqualsSignGreaterThanSignKeyword_1());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:262:1: ( (lv_option_2_0= ruleOption ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:263:1: (lv_option_2_0= ruleOption )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:263:1: (lv_option_2_0= ruleOption )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:264:3: lv_option_2_0= ruleOption
            {
             
            	        newCompositeNode(grammarAccess.getWidgetOptQuestAccess().getOptionOptionParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleOption_in_ruleWidgetOptQuest498);
            lv_option_2_0=ruleOption();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWidgetOptQuestRule());
            	        }
                   		set(
                   			current, 
                   			"option",
                    		lv_option_2_0, 
                    		"Option");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_15_in_ruleWidgetOptQuest510); 

                	newLeafNode(otherlv_3, grammarAccess.getWidgetOptQuestAccess().getColonKeyword_3());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:284:1: ( (lv_type_4_0= ruleWidgetOptType ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:285:1: (lv_type_4_0= ruleWidgetOptType )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:285:1: (lv_type_4_0= ruleWidgetOptType )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:286:3: lv_type_4_0= ruleWidgetOptType
            {
             
            	        newCompositeNode(grammarAccess.getWidgetOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_4_0()); 
            	    
            pushFollow(FOLLOW_ruleWidgetOptType_in_ruleWidgetOptQuest531);
            lv_type_4_0=ruleWidgetOptType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getWidgetOptQuestRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_4_0, 
                    		"WidgetOptType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetOptQuest"


    // $ANTLR start "entryRuleOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:310:1: entryRuleOptQuest returns [EObject current=null] : iv_ruleOptQuest= ruleOptQuest EOF ;
    public final EObject entryRuleOptQuest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptQuest = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:311:2: (iv_ruleOptQuest= ruleOptQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:312:2: iv_ruleOptQuest= ruleOptQuest EOF
            {
             newCompositeNode(grammarAccess.getOptQuestRule()); 
            pushFollow(FOLLOW_ruleOptQuest_in_entryRuleOptQuest567);
            iv_ruleOptQuest=ruleOptQuest();

            state._fsp--;

             current =iv_ruleOptQuest; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptQuest577); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptQuest"


    // $ANTLR start "ruleOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:319:1: ruleOptQuest returns [EObject current=null] : ( ( (lv_option_0_0= ruleOption ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetOptType ) ) ) ;
    public final EObject ruleOptQuest() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_option_0_0 = null;

        EObject lv_type_2_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:322:28: ( ( ( (lv_option_0_0= ruleOption ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetOptType ) ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:323:1: ( ( (lv_option_0_0= ruleOption ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetOptType ) ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:323:1: ( ( (lv_option_0_0= ruleOption ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetOptType ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:323:2: ( (lv_option_0_0= ruleOption ) ) otherlv_1= ':' ( (lv_type_2_0= ruleWidgetOptType ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:323:2: ( (lv_option_0_0= ruleOption ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:324:1: (lv_option_0_0= ruleOption )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:324:1: (lv_option_0_0= ruleOption )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:325:3: lv_option_0_0= ruleOption
            {
             
            	        newCompositeNode(grammarAccess.getOptQuestAccess().getOptionOptionParserRuleCall_0_0()); 
            	    
            pushFollow(FOLLOW_ruleOption_in_ruleOptQuest623);
            lv_option_0_0=ruleOption();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptQuestRule());
            	        }
                   		set(
                   			current, 
                   			"option",
                    		lv_option_0_0, 
                    		"Option");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleOptQuest635); 

                	newLeafNode(otherlv_1, grammarAccess.getOptQuestAccess().getColonKeyword_1());
                
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:345:1: ( (lv_type_2_0= ruleWidgetOptType ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:346:1: (lv_type_2_0= ruleWidgetOptType )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:346:1: (lv_type_2_0= ruleWidgetOptType )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:347:3: lv_type_2_0= ruleWidgetOptType
            {
             
            	        newCompositeNode(grammarAccess.getOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_2_0()); 
            	    
            pushFollow(FOLLOW_ruleWidgetOptType_in_ruleOptQuest656);
            lv_type_2_0=ruleWidgetOptType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getOptQuestRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_2_0, 
                    		"WidgetOptType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptQuest"


    // $ANTLR start "entryRuleQuestion"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:371:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:372:2: (iv_ruleQuestion= ruleQuestion EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:373:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion692);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion702); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:380:1: ruleQuestion returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:383:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:384:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:384:1: ( (lv_name_0_0= RULE_ID ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:385:1: (lv_name_0_0= RULE_ID )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:385:1: (lv_name_0_0= RULE_ID )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:386:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQuestion743); 

            			newLeafNode(lv_name_0_0, grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuestionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:410:1: entryRuleOption returns [EObject current=null] : iv_ruleOption= ruleOption EOF ;
    public final EObject entryRuleOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOption = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:411:2: (iv_ruleOption= ruleOption EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:412:2: iv_ruleOption= ruleOption EOF
            {
             newCompositeNode(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption783);
            iv_ruleOption=ruleOption();

            state._fsp--;

             current =iv_ruleOption; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption793); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:419:1: ruleOption returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleOption() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:422:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:423:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:423:1: ( (lv_name_0_0= RULE_ID ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:424:1: (lv_name_0_0= RULE_ID )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:424:1: (lv_name_0_0= RULE_ID )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:425:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOption834); 

            			newLeafNode(lv_name_0_0, grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOptionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"ID");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOption"


    // $ANTLR start "entryRuleWidgetQuestType"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:449:1: entryRuleWidgetQuestType returns [String current=null] : iv_ruleWidgetQuestType= ruleWidgetQuestType EOF ;
    public final String entryRuleWidgetQuestType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleWidgetQuestType = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:450:2: (iv_ruleWidgetQuestType= ruleWidgetQuestType EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:451:2: iv_ruleWidgetQuestType= ruleWidgetQuestType EOF
            {
             newCompositeNode(grammarAccess.getWidgetQuestTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType875);
            iv_ruleWidgetQuestType=ruleWidgetQuestType();

            state._fsp--;

             current =iv_ruleWidgetQuestType.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuestType886); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetQuestType"


    // $ANTLR start "ruleWidgetQuestType"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:458:1: ruleWidgetQuestType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect ) ;
    public final AntlrDatatypeRuleToken ruleWidgetQuestType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_Checkbox_0 = null;

        AntlrDatatypeRuleToken this_Radio_1 = null;

        AntlrDatatypeRuleToken this_Select_2 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:461:28: ( (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:462:1: (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:462:1: (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 18:
                {
                alt4=1;
                }
                break;
            case 19:
                {
                alt4=2;
                }
                break;
            case 20:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:463:5: this_Checkbox_0= ruleCheckbox
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleCheckbox_in_ruleWidgetQuestType933);
                    this_Checkbox_0=ruleCheckbox();

                    state._fsp--;


                    		current.merge(this_Checkbox_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:475:5: this_Radio_1= ruleRadio
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleRadio_in_ruleWidgetQuestType966);
                    this_Radio_1=ruleRadio();

                    state._fsp--;


                    		current.merge(this_Radio_1);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:487:5: this_Select_2= ruleSelect
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleSelect_in_ruleWidgetQuestType999);
                    this_Select_2=ruleSelect();

                    state._fsp--;


                    		current.merge(this_Select_2);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetQuestType"


    // $ANTLR start "entryRuleWidgetOptType"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:505:1: entryRuleWidgetOptType returns [EObject current=null] : iv_ruleWidgetOptType= ruleWidgetOptType EOF ;
    public final EObject entryRuleWidgetOptType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidgetOptType = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:506:2: (iv_ruleWidgetOptType= ruleWidgetOptType EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:507:2: iv_ruleWidgetOptType= ruleWidgetOptType EOF
            {
             newCompositeNode(grammarAccess.getWidgetOptTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType1044);
            iv_ruleWidgetOptType=ruleWidgetOptType();

            state._fsp--;

             current =iv_ruleWidgetOptType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptType1054); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetOptType"


    // $ANTLR start "ruleWidgetOptType"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:514:1: ruleWidgetOptType returns [EObject current=null] : ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleWidgetOptType() throws RecognitionException {
        EObject current = null;

        Token lv_param_1_0=null;
        AntlrDatatypeRuleToken lv_type_0_1 = null;

        AntlrDatatypeRuleToken lv_type_0_2 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:517:28: ( ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:518:1: ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:518:1: ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:518:2: ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:518:2: ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:519:1: ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:519:1: ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:520:1: (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:520:1: (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            else if ( (LA5_0==21) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:521:3: lv_type_0_1= ruleImage
                    {
                     
                    	        newCompositeNode(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleImage_in_ruleWidgetOptType1102);
                    lv_type_0_1=ruleImage();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWidgetOptTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_0_1, 
                            		"Image");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:536:8: lv_type_0_2= ruleInput
                    {
                     
                    	        newCompositeNode(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleInput_in_ruleWidgetOptType1121);
                    lv_type_0_2=ruleInput();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWidgetOptTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_0_2, 
                            		"Input");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }

            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:554:2: ( (lv_param_1_0= RULE_STRING ) )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:555:1: (lv_param_1_0= RULE_STRING )
            {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:555:1: (lv_param_1_0= RULE_STRING )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:556:3: lv_param_1_0= RULE_STRING
            {
            lv_param_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleWidgetOptType1141); 

            			newLeafNode(lv_param_1_0, grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWidgetOptTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"param",
                    		lv_param_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetOptType"


    // $ANTLR start "entryRuleImage"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:580:1: entryRuleImage returns [String current=null] : iv_ruleImage= ruleImage EOF ;
    public final String entryRuleImage() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleImage = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:581:2: (iv_ruleImage= ruleImage EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:582:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage1183);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage1194); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:589:1: ruleImage returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Image' ;
    public final AntlrDatatypeRuleToken ruleImage() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:592:28: (kw= 'Image' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:594:2: kw= 'Image'
            {
            kw=(Token)match(input,17,FOLLOW_17_in_ruleImage1231); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getImageAccess().getImageKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckbox"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:607:1: entryRuleCheckbox returns [String current=null] : iv_ruleCheckbox= ruleCheckbox EOF ;
    public final String entryRuleCheckbox() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCheckbox = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:608:2: (iv_ruleCheckbox= ruleCheckbox EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:609:2: iv_ruleCheckbox= ruleCheckbox EOF
            {
             newCompositeNode(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox1271);
            iv_ruleCheckbox=ruleCheckbox();

            state._fsp--;

             current =iv_ruleCheckbox.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox1282); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:616:1: ruleCheckbox returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Checkbox' ;
    public final AntlrDatatypeRuleToken ruleCheckbox() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:619:28: (kw= 'Checkbox' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:621:2: kw= 'Checkbox'
            {
            kw=(Token)match(input,18,FOLLOW_18_in_ruleCheckbox1319); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadio"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:634:1: entryRuleRadio returns [String current=null] : iv_ruleRadio= ruleRadio EOF ;
    public final String entryRuleRadio() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRadio = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:635:2: (iv_ruleRadio= ruleRadio EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:636:2: iv_ruleRadio= ruleRadio EOF
            {
             newCompositeNode(grammarAccess.getRadioRule()); 
            pushFollow(FOLLOW_ruleRadio_in_entryRuleRadio1359);
            iv_ruleRadio=ruleRadio();

            state._fsp--;

             current =iv_ruleRadio.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadio1370); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRadio"


    // $ANTLR start "ruleRadio"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:643:1: ruleRadio returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Radio' ;
    public final AntlrDatatypeRuleToken ruleRadio() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:646:28: (kw= 'Radio' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:648:2: kw= 'Radio'
            {
            kw=(Token)match(input,19,FOLLOW_19_in_ruleRadio1407); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getRadioAccess().getRadioKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRadio"


    // $ANTLR start "entryRuleSelect"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:661:1: entryRuleSelect returns [String current=null] : iv_ruleSelect= ruleSelect EOF ;
    public final String entryRuleSelect() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSelect = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:662:2: (iv_ruleSelect= ruleSelect EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:663:2: iv_ruleSelect= ruleSelect EOF
            {
             newCompositeNode(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_ruleSelect_in_entryRuleSelect1447);
            iv_ruleSelect=ruleSelect();

            state._fsp--;

             current =iv_ruleSelect.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelect1458); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:670:1: ruleSelect returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Select' ;
    public final AntlrDatatypeRuleToken ruleSelect() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:673:28: (kw= 'Select' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:675:2: kw= 'Select'
            {
            kw=(Token)match(input,20,FOLLOW_20_in_ruleSelect1495); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getSelectAccess().getSelectKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleInput"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:688:1: entryRuleInput returns [String current=null] : iv_ruleInput= ruleInput EOF ;
    public final String entryRuleInput() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInput = null;


        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:689:2: (iv_ruleInput= ruleInput EOF )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:690:2: iv_ruleInput= ruleInput EOF
            {
             newCompositeNode(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput1535);
            iv_ruleInput=ruleInput();

            state._fsp--;

             current =iv_ruleInput.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput1546); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:697:1: ruleInput returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Input' ;
    public final AntlrDatatypeRuleToken ruleInput() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:700:28: (kw= 'Input' )
            // ../m2.idm.tp3.UIQuestionnaire/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIQuestionnaire.g:702:2: kw= 'Input'
            {
            kw=(Token)match(input,21,FOLLOW_21_in_ruleInput1583); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInputAccess().getInputKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleUIQuestionnaire_in_entryRuleUIQuestionnaire75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUIQuestionnaire85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleUIQuestionnaire122 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_ruleWidgetQuest_in_ruleUIQuestionnaire143 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_12_in_ruleUIQuestionnaire156 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleUIQuestionnaire168 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_ruleWidgetOptQuest_in_ruleUIQuestionnaire189 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_12_in_ruleUIQuestionnaire202 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleUIQuestionnaire214 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_ruleOptQuest_in_ruleUIQuestionnaire235 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_12_in_ruleUIQuestionnaire248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuest_in_entryRuleWidgetQuest284 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuest294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_ruleWidgetQuest340 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleWidgetQuest352 = new BitSet(new long[]{0x00000000001C0000L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_ruleWidgetQuest373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptQuest_in_entryRuleWidgetOptQuest409 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptQuest419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_ruleWidgetOptQuest465 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleWidgetOptQuest477 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleOption_in_ruleWidgetOptQuest498 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleWidgetOptQuest510 = new BitSet(new long[]{0x0000000000220000L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_ruleWidgetOptQuest531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptQuest_in_entryRuleOptQuest567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptQuest577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_ruleOptQuest623 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleOptQuest635 = new BitSet(new long[]{0x0000000000220000L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_ruleOptQuest656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion692 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion702 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQuestion743 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption783 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption793 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOption834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType875 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuestType886 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_ruleWidgetQuestType933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_ruleWidgetQuestType966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_ruleWidgetQuestType999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType1044 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptType1054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_ruleWidgetOptType1102 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleInput_in_ruleWidgetOptType1121 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleWidgetOptType1141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage1183 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage1194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleImage1231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox1271 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox1282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleCheckbox1319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_entryRuleRadio1359 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadio1370 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleRadio1407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_entryRuleSelect1447 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelect1458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleSelect1495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput1535 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput1546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleInput1583 = new BitSet(new long[]{0x0000000000000002L});

}