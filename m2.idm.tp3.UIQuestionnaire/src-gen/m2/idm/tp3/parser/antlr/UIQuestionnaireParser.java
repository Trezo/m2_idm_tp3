/*
* generated by Xtext
*/
package m2.idm.tp3.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import m2.idm.tp3.services.UIQuestionnaireGrammarAccess;

public class UIQuestionnaireParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private UIQuestionnaireGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected m2.idm.tp3.parser.antlr.internal.InternalUIQuestionnaireParser createParser(XtextTokenStream stream) {
		return new m2.idm.tp3.parser.antlr.internal.InternalUIQuestionnaireParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "UIQuestionnaire";
	}
	
	public UIQuestionnaireGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(UIQuestionnaireGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
