package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Widget'", "'{'", "'}'", "'Image'", "'('", "','", "')'", "'CheckboxGroup'", "'Checkbox'", "'RadioButtonGroup'", "'Checked'", "'Unchecked'", "'RadioButton'", "'Input'"
    };
    public static final int RULE_ID=5;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g"; }


     
     	private MyDslGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleWidget"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:60:1: entryRuleWidget : ruleWidget EOF ;
    public final void entryRuleWidget() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:61:1: ( ruleWidget EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:62:1: ruleWidget EOF
            {
             before(grammarAccess.getWidgetRule()); 
            pushFollow(FOLLOW_ruleWidget_in_entryRuleWidget61);
            ruleWidget();

            state._fsp--;

             after(grammarAccess.getWidgetRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidget68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidget"


    // $ANTLR start "ruleWidget"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:69:1: ruleWidget : ( ( rule__Widget__Group__0 ) ) ;
    public final void ruleWidget() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:73:2: ( ( ( rule__Widget__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:74:1: ( ( rule__Widget__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:74:1: ( ( rule__Widget__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:75:1: ( rule__Widget__Group__0 )
            {
             before(grammarAccess.getWidgetAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:76:1: ( rule__Widget__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:76:2: rule__Widget__Group__0
            {
            pushFollow(FOLLOW_rule__Widget__Group__0_in_ruleWidget94);
            rule__Widget__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidget"


    // $ANTLR start "entryRuleItem"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:88:1: entryRuleItem : ruleItem EOF ;
    public final void entryRuleItem() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:89:1: ( ruleItem EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:90:1: ruleItem EOF
            {
             before(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_ruleItem_in_entryRuleItem121);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getItemRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleItem128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:97:1: ruleItem : ( ( rule__Item__Alternatives ) ) ;
    public final void ruleItem() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:101:2: ( ( ( rule__Item__Alternatives ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:102:1: ( ( rule__Item__Alternatives ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:102:1: ( ( rule__Item__Alternatives ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:103:1: ( rule__Item__Alternatives )
            {
             before(grammarAccess.getItemAccess().getAlternatives()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:104:1: ( rule__Item__Alternatives )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:104:2: rule__Item__Alternatives
            {
            pushFollow(FOLLOW_rule__Item__Alternatives_in_ruleItem154);
            rule__Item__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getItemAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleImage"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:116:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:117:1: ( ruleImage EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:118:1: ruleImage EOF
            {
             before(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage181);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getImageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:125:1: ruleImage : ( ( rule__Image__Group__0 ) ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:129:2: ( ( ( rule__Image__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:130:1: ( ( rule__Image__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:130:1: ( ( rule__Image__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:131:1: ( rule__Image__Group__0 )
            {
             before(grammarAccess.getImageAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:132:1: ( rule__Image__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:132:2: rule__Image__Group__0
            {
            pushFollow(FOLLOW_rule__Image__Group__0_in_ruleImage214);
            rule__Image__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckboxGroup"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:144:1: entryRuleCheckboxGroup : ruleCheckboxGroup EOF ;
    public final void entryRuleCheckboxGroup() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:145:1: ( ruleCheckboxGroup EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:146:1: ruleCheckboxGroup EOF
            {
             before(grammarAccess.getCheckboxGroupRule()); 
            pushFollow(FOLLOW_ruleCheckboxGroup_in_entryRuleCheckboxGroup241);
            ruleCheckboxGroup();

            state._fsp--;

             after(grammarAccess.getCheckboxGroupRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckboxGroup248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckboxGroup"


    // $ANTLR start "ruleCheckboxGroup"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:153:1: ruleCheckboxGroup : ( ( rule__CheckboxGroup__Group__0 ) ) ;
    public final void ruleCheckboxGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:157:2: ( ( ( rule__CheckboxGroup__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:158:1: ( ( rule__CheckboxGroup__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:158:1: ( ( rule__CheckboxGroup__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:159:1: ( rule__CheckboxGroup__Group__0 )
            {
             before(grammarAccess.getCheckboxGroupAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:160:1: ( rule__CheckboxGroup__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:160:2: rule__CheckboxGroup__Group__0
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__0_in_ruleCheckboxGroup274);
            rule__CheckboxGroup__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckboxGroup"


    // $ANTLR start "entryRuleCheckbox"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:172:1: entryRuleCheckbox : ruleCheckbox EOF ;
    public final void entryRuleCheckbox() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:173:1: ( ruleCheckbox EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:174:1: ruleCheckbox EOF
            {
             before(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox301);
            ruleCheckbox();

            state._fsp--;

             after(grammarAccess.getCheckboxRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:181:1: ruleCheckbox : ( ( rule__Checkbox__Group__0 ) ) ;
    public final void ruleCheckbox() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:185:2: ( ( ( rule__Checkbox__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:186:1: ( ( rule__Checkbox__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:186:1: ( ( rule__Checkbox__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:187:1: ( rule__Checkbox__Group__0 )
            {
             before(grammarAccess.getCheckboxAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:188:1: ( rule__Checkbox__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:188:2: rule__Checkbox__Group__0
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__0_in_ruleCheckbox334);
            rule__Checkbox__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadioButtonGroup"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:200:1: entryRuleRadioButtonGroup : ruleRadioButtonGroup EOF ;
    public final void entryRuleRadioButtonGroup() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:201:1: ( ruleRadioButtonGroup EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:202:1: ruleRadioButtonGroup EOF
            {
             before(grammarAccess.getRadioButtonGroupRule()); 
            pushFollow(FOLLOW_ruleRadioButtonGroup_in_entryRuleRadioButtonGroup361);
            ruleRadioButtonGroup();

            state._fsp--;

             after(grammarAccess.getRadioButtonGroupRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButtonGroup368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRadioButtonGroup"


    // $ANTLR start "ruleRadioButtonGroup"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:209:1: ruleRadioButtonGroup : ( ( rule__RadioButtonGroup__Group__0 ) ) ;
    public final void ruleRadioButtonGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:213:2: ( ( ( rule__RadioButtonGroup__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:214:1: ( ( rule__RadioButtonGroup__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:214:1: ( ( rule__RadioButtonGroup__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:215:1: ( rule__RadioButtonGroup__Group__0 )
            {
             before(grammarAccess.getRadioButtonGroupAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:216:1: ( rule__RadioButtonGroup__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:216:2: rule__RadioButtonGroup__Group__0
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__0_in_ruleRadioButtonGroup394);
            rule__RadioButtonGroup__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRadioButtonGroup"


    // $ANTLR start "entryRuleRadioButton"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:228:1: entryRuleRadioButton : ruleRadioButton EOF ;
    public final void entryRuleRadioButton() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:229:1: ( ruleRadioButton EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:230:1: ruleRadioButton EOF
            {
             before(grammarAccess.getRadioButtonRule()); 
            pushFollow(FOLLOW_ruleRadioButton_in_entryRuleRadioButton421);
            ruleRadioButton();

            state._fsp--;

             after(grammarAccess.getRadioButtonRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButton428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRadioButton"


    // $ANTLR start "ruleRadioButton"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:237:1: ruleRadioButton : ( ( rule__RadioButton__Group__0 ) ) ;
    public final void ruleRadioButton() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:241:2: ( ( ( rule__RadioButton__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:242:1: ( ( rule__RadioButton__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:242:1: ( ( rule__RadioButton__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:243:1: ( rule__RadioButton__Group__0 )
            {
             before(grammarAccess.getRadioButtonAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:244:1: ( rule__RadioButton__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:244:2: rule__RadioButton__Group__0
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__0_in_ruleRadioButton454);
            rule__RadioButton__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRadioButton"


    // $ANTLR start "entryRuleInput"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:256:1: entryRuleInput : ruleInput EOF ;
    public final void entryRuleInput() throws RecognitionException {
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:257:1: ( ruleInput EOF )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:258:1: ruleInput EOF
            {
             before(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput481);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getInputRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:265:1: ruleInput : ( ( rule__Input__Group__0 ) ) ;
    public final void ruleInput() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:269:2: ( ( ( rule__Input__Group__0 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:270:1: ( ( rule__Input__Group__0 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:270:1: ( ( rule__Input__Group__0 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:271:1: ( rule__Input__Group__0 )
            {
             before(grammarAccess.getInputAccess().getGroup()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:272:1: ( rule__Input__Group__0 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:272:2: rule__Input__Group__0
            {
            pushFollow(FOLLOW_rule__Input__Group__0_in_ruleInput514);
            rule__Input__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "rule__Item__Alternatives"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:284:1: rule__Item__Alternatives : ( ( ruleImage ) | ( ruleCheckboxGroup ) | ( ruleRadioButtonGroup ) | ( ruleInput ) );
    public final void rule__Item__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:288:1: ( ( ruleImage ) | ( ruleCheckboxGroup ) | ( ruleRadioButtonGroup ) | ( ruleInput ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt1=1;
                }
                break;
            case 18:
                {
                alt1=2;
                }
                break;
            case 20:
                {
                alt1=3;
                }
                break;
            case 24:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:289:1: ( ruleImage )
                    {
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:289:1: ( ruleImage )
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:290:1: ruleImage
                    {
                     before(grammarAccess.getItemAccess().getImageParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleImage_in_rule__Item__Alternatives550);
                    ruleImage();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getImageParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:295:6: ( ruleCheckboxGroup )
                    {
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:295:6: ( ruleCheckboxGroup )
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:296:1: ruleCheckboxGroup
                    {
                     before(grammarAccess.getItemAccess().getCheckboxGroupParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleCheckboxGroup_in_rule__Item__Alternatives567);
                    ruleCheckboxGroup();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getCheckboxGroupParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:301:6: ( ruleRadioButtonGroup )
                    {
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:301:6: ( ruleRadioButtonGroup )
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:302:1: ruleRadioButtonGroup
                    {
                     before(grammarAccess.getItemAccess().getRadioButtonGroupParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleRadioButtonGroup_in_rule__Item__Alternatives584);
                    ruleRadioButtonGroup();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getRadioButtonGroupParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:307:6: ( ruleInput )
                    {
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:307:6: ( ruleInput )
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:308:1: ruleInput
                    {
                     before(grammarAccess.getItemAccess().getInputParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleInput_in_rule__Item__Alternatives601);
                    ruleInput();

                    state._fsp--;

                     after(grammarAccess.getItemAccess().getInputParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__Alternatives"


    // $ANTLR start "rule__Widget__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:320:1: rule__Widget__Group__0 : rule__Widget__Group__0__Impl rule__Widget__Group__1 ;
    public final void rule__Widget__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:324:1: ( rule__Widget__Group__0__Impl rule__Widget__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:325:2: rule__Widget__Group__0__Impl rule__Widget__Group__1
            {
            pushFollow(FOLLOW_rule__Widget__Group__0__Impl_in_rule__Widget__Group__0631);
            rule__Widget__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Widget__Group__1_in_rule__Widget__Group__0634);
            rule__Widget__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__0"


    // $ANTLR start "rule__Widget__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:332:1: rule__Widget__Group__0__Impl : ( 'Widget' ) ;
    public final void rule__Widget__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:336:1: ( ( 'Widget' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:337:1: ( 'Widget' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:337:1: ( 'Widget' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:338:1: 'Widget'
            {
             before(grammarAccess.getWidgetAccess().getWidgetKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__Widget__Group__0__Impl662); 
             after(grammarAccess.getWidgetAccess().getWidgetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__0__Impl"


    // $ANTLR start "rule__Widget__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:351:1: rule__Widget__Group__1 : rule__Widget__Group__1__Impl rule__Widget__Group__2 ;
    public final void rule__Widget__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:355:1: ( rule__Widget__Group__1__Impl rule__Widget__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:356:2: rule__Widget__Group__1__Impl rule__Widget__Group__2
            {
            pushFollow(FOLLOW_rule__Widget__Group__1__Impl_in_rule__Widget__Group__1693);
            rule__Widget__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Widget__Group__2_in_rule__Widget__Group__1696);
            rule__Widget__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__1"


    // $ANTLR start "rule__Widget__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:363:1: rule__Widget__Group__1__Impl : ( '{' ) ;
    public final void rule__Widget__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:367:1: ( ( '{' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:368:1: ( '{' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:368:1: ( '{' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:369:1: '{'
            {
             before(grammarAccess.getWidgetAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,12,FOLLOW_12_in_rule__Widget__Group__1__Impl724); 
             after(grammarAccess.getWidgetAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__1__Impl"


    // $ANTLR start "rule__Widget__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:382:1: rule__Widget__Group__2 : rule__Widget__Group__2__Impl rule__Widget__Group__3 ;
    public final void rule__Widget__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:386:1: ( rule__Widget__Group__2__Impl rule__Widget__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:387:2: rule__Widget__Group__2__Impl rule__Widget__Group__3
            {
            pushFollow(FOLLOW_rule__Widget__Group__2__Impl_in_rule__Widget__Group__2755);
            rule__Widget__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Widget__Group__3_in_rule__Widget__Group__2758);
            rule__Widget__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__2"


    // $ANTLR start "rule__Widget__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:394:1: rule__Widget__Group__2__Impl : ( ( ( rule__Widget__ItemsAssignment_2 ) ) ( ( rule__Widget__ItemsAssignment_2 )* ) ) ;
    public final void rule__Widget__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:398:1: ( ( ( ( rule__Widget__ItemsAssignment_2 ) ) ( ( rule__Widget__ItemsAssignment_2 )* ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:399:1: ( ( ( rule__Widget__ItemsAssignment_2 ) ) ( ( rule__Widget__ItemsAssignment_2 )* ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:399:1: ( ( ( rule__Widget__ItemsAssignment_2 ) ) ( ( rule__Widget__ItemsAssignment_2 )* ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:400:1: ( ( rule__Widget__ItemsAssignment_2 ) ) ( ( rule__Widget__ItemsAssignment_2 )* )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:400:1: ( ( rule__Widget__ItemsAssignment_2 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:401:1: ( rule__Widget__ItemsAssignment_2 )
            {
             before(grammarAccess.getWidgetAccess().getItemsAssignment_2()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:402:1: ( rule__Widget__ItemsAssignment_2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:402:2: rule__Widget__ItemsAssignment_2
            {
            pushFollow(FOLLOW_rule__Widget__ItemsAssignment_2_in_rule__Widget__Group__2__Impl787);
            rule__Widget__ItemsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWidgetAccess().getItemsAssignment_2()); 

            }

            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:405:1: ( ( rule__Widget__ItemsAssignment_2 )* )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:406:1: ( rule__Widget__ItemsAssignment_2 )*
            {
             before(grammarAccess.getWidgetAccess().getItemsAssignment_2()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:407:1: ( rule__Widget__ItemsAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==14||LA2_0==18||LA2_0==20||LA2_0==24) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:407:2: rule__Widget__ItemsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Widget__ItemsAssignment_2_in_rule__Widget__Group__2__Impl799);
            	    rule__Widget__ItemsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getWidgetAccess().getItemsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__2__Impl"


    // $ANTLR start "rule__Widget__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:418:1: rule__Widget__Group__3 : rule__Widget__Group__3__Impl ;
    public final void rule__Widget__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:422:1: ( rule__Widget__Group__3__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:423:2: rule__Widget__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Widget__Group__3__Impl_in_rule__Widget__Group__3832);
            rule__Widget__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__3"


    // $ANTLR start "rule__Widget__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:429:1: rule__Widget__Group__3__Impl : ( '}' ) ;
    public final void rule__Widget__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:433:1: ( ( '}' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:434:1: ( '}' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:434:1: ( '}' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:435:1: '}'
            {
             before(grammarAccess.getWidgetAccess().getRightCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_13_in_rule__Widget__Group__3__Impl860); 
             after(grammarAccess.getWidgetAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Group__3__Impl"


    // $ANTLR start "rule__Image__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:456:1: rule__Image__Group__0 : rule__Image__Group__0__Impl rule__Image__Group__1 ;
    public final void rule__Image__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:460:1: ( rule__Image__Group__0__Impl rule__Image__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:461:2: rule__Image__Group__0__Impl rule__Image__Group__1
            {
            pushFollow(FOLLOW_rule__Image__Group__0__Impl_in_rule__Image__Group__0899);
            rule__Image__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__1_in_rule__Image__Group__0902);
            rule__Image__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0"


    // $ANTLR start "rule__Image__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:468:1: rule__Image__Group__0__Impl : ( 'Image' ) ;
    public final void rule__Image__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:472:1: ( ( 'Image' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:473:1: ( 'Image' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:473:1: ( 'Image' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:474:1: 'Image'
            {
             before(grammarAccess.getImageAccess().getImageKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Image__Group__0__Impl930); 
             after(grammarAccess.getImageAccess().getImageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0__Impl"


    // $ANTLR start "rule__Image__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:487:1: rule__Image__Group__1 : rule__Image__Group__1__Impl rule__Image__Group__2 ;
    public final void rule__Image__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:491:1: ( rule__Image__Group__1__Impl rule__Image__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:492:2: rule__Image__Group__1__Impl rule__Image__Group__2
            {
            pushFollow(FOLLOW_rule__Image__Group__1__Impl_in_rule__Image__Group__1961);
            rule__Image__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__2_in_rule__Image__Group__1964);
            rule__Image__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1"


    // $ANTLR start "rule__Image__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:499:1: rule__Image__Group__1__Impl : ( '(' ) ;
    public final void rule__Image__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:503:1: ( ( '(' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:504:1: ( '(' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:504:1: ( '(' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:505:1: '('
            {
             before(grammarAccess.getImageAccess().getLeftParenthesisKeyword_1()); 
            match(input,15,FOLLOW_15_in_rule__Image__Group__1__Impl992); 
             after(grammarAccess.getImageAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1__Impl"


    // $ANTLR start "rule__Image__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:518:1: rule__Image__Group__2 : rule__Image__Group__2__Impl rule__Image__Group__3 ;
    public final void rule__Image__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:522:1: ( rule__Image__Group__2__Impl rule__Image__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:523:2: rule__Image__Group__2__Impl rule__Image__Group__3
            {
            pushFollow(FOLLOW_rule__Image__Group__2__Impl_in_rule__Image__Group__21023);
            rule__Image__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__3_in_rule__Image__Group__21026);
            rule__Image__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2"


    // $ANTLR start "rule__Image__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:530:1: rule__Image__Group__2__Impl : ( ( rule__Image__SourceAssignment_2 ) ) ;
    public final void rule__Image__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:534:1: ( ( ( rule__Image__SourceAssignment_2 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:535:1: ( ( rule__Image__SourceAssignment_2 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:535:1: ( ( rule__Image__SourceAssignment_2 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:536:1: ( rule__Image__SourceAssignment_2 )
            {
             before(grammarAccess.getImageAccess().getSourceAssignment_2()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:537:1: ( rule__Image__SourceAssignment_2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:537:2: rule__Image__SourceAssignment_2
            {
            pushFollow(FOLLOW_rule__Image__SourceAssignment_2_in_rule__Image__Group__2__Impl1053);
            rule__Image__SourceAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getSourceAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__2__Impl"


    // $ANTLR start "rule__Image__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:547:1: rule__Image__Group__3 : rule__Image__Group__3__Impl rule__Image__Group__4 ;
    public final void rule__Image__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:551:1: ( rule__Image__Group__3__Impl rule__Image__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:552:2: rule__Image__Group__3__Impl rule__Image__Group__4
            {
            pushFollow(FOLLOW_rule__Image__Group__3__Impl_in_rule__Image__Group__31083);
            rule__Image__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__4_in_rule__Image__Group__31086);
            rule__Image__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3"


    // $ANTLR start "rule__Image__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:559:1: rule__Image__Group__3__Impl : ( ',' ) ;
    public final void rule__Image__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:563:1: ( ( ',' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:564:1: ( ',' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:564:1: ( ',' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:565:1: ','
            {
             before(grammarAccess.getImageAccess().getCommaKeyword_3()); 
            match(input,16,FOLLOW_16_in_rule__Image__Group__3__Impl1114); 
             after(grammarAccess.getImageAccess().getCommaKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__3__Impl"


    // $ANTLR start "rule__Image__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:578:1: rule__Image__Group__4 : rule__Image__Group__4__Impl rule__Image__Group__5 ;
    public final void rule__Image__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:582:1: ( rule__Image__Group__4__Impl rule__Image__Group__5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:583:2: rule__Image__Group__4__Impl rule__Image__Group__5
            {
            pushFollow(FOLLOW_rule__Image__Group__4__Impl_in_rule__Image__Group__41145);
            rule__Image__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__5_in_rule__Image__Group__41148);
            rule__Image__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__4"


    // $ANTLR start "rule__Image__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:590:1: rule__Image__Group__4__Impl : ( ( rule__Image__AltAssignment_4 ) ) ;
    public final void rule__Image__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:594:1: ( ( ( rule__Image__AltAssignment_4 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:595:1: ( ( rule__Image__AltAssignment_4 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:595:1: ( ( rule__Image__AltAssignment_4 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:596:1: ( rule__Image__AltAssignment_4 )
            {
             before(grammarAccess.getImageAccess().getAltAssignment_4()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:597:1: ( rule__Image__AltAssignment_4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:597:2: rule__Image__AltAssignment_4
            {
            pushFollow(FOLLOW_rule__Image__AltAssignment_4_in_rule__Image__Group__4__Impl1175);
            rule__Image__AltAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getAltAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__4__Impl"


    // $ANTLR start "rule__Image__Group__5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:607:1: rule__Image__Group__5 : rule__Image__Group__5__Impl ;
    public final void rule__Image__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:611:1: ( rule__Image__Group__5__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:612:2: rule__Image__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Image__Group__5__Impl_in_rule__Image__Group__51205);
            rule__Image__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__5"


    // $ANTLR start "rule__Image__Group__5__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:618:1: rule__Image__Group__5__Impl : ( ')' ) ;
    public final void rule__Image__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:622:1: ( ( ')' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:623:1: ( ')' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:623:1: ( ')' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:624:1: ')'
            {
             before(grammarAccess.getImageAccess().getRightParenthesisKeyword_5()); 
            match(input,17,FOLLOW_17_in_rule__Image__Group__5__Impl1233); 
             after(grammarAccess.getImageAccess().getRightParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__5__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:649:1: rule__CheckboxGroup__Group__0 : rule__CheckboxGroup__Group__0__Impl rule__CheckboxGroup__Group__1 ;
    public final void rule__CheckboxGroup__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:653:1: ( rule__CheckboxGroup__Group__0__Impl rule__CheckboxGroup__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:654:2: rule__CheckboxGroup__Group__0__Impl rule__CheckboxGroup__Group__1
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__0__Impl_in_rule__CheckboxGroup__Group__01276);
            rule__CheckboxGroup__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckboxGroup__Group__1_in_rule__CheckboxGroup__Group__01279);
            rule__CheckboxGroup__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__0"


    // $ANTLR start "rule__CheckboxGroup__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:661:1: rule__CheckboxGroup__Group__0__Impl : ( 'CheckboxGroup' ) ;
    public final void rule__CheckboxGroup__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:665:1: ( ( 'CheckboxGroup' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:666:1: ( 'CheckboxGroup' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:666:1: ( 'CheckboxGroup' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:667:1: 'CheckboxGroup'
            {
             before(grammarAccess.getCheckboxGroupAccess().getCheckboxGroupKeyword_0()); 
            match(input,18,FOLLOW_18_in_rule__CheckboxGroup__Group__0__Impl1307); 
             after(grammarAccess.getCheckboxGroupAccess().getCheckboxGroupKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__0__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:680:1: rule__CheckboxGroup__Group__1 : rule__CheckboxGroup__Group__1__Impl rule__CheckboxGroup__Group__2 ;
    public final void rule__CheckboxGroup__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:684:1: ( rule__CheckboxGroup__Group__1__Impl rule__CheckboxGroup__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:685:2: rule__CheckboxGroup__Group__1__Impl rule__CheckboxGroup__Group__2
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__1__Impl_in_rule__CheckboxGroup__Group__11338);
            rule__CheckboxGroup__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckboxGroup__Group__2_in_rule__CheckboxGroup__Group__11341);
            rule__CheckboxGroup__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__1"


    // $ANTLR start "rule__CheckboxGroup__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:692:1: rule__CheckboxGroup__Group__1__Impl : ( ( rule__CheckboxGroup__NameAssignment_1 )? ) ;
    public final void rule__CheckboxGroup__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:696:1: ( ( ( rule__CheckboxGroup__NameAssignment_1 )? ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:697:1: ( ( rule__CheckboxGroup__NameAssignment_1 )? )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:697:1: ( ( rule__CheckboxGroup__NameAssignment_1 )? )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:698:1: ( rule__CheckboxGroup__NameAssignment_1 )?
            {
             before(grammarAccess.getCheckboxGroupAccess().getNameAssignment_1()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:699:1: ( rule__CheckboxGroup__NameAssignment_1 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:699:2: rule__CheckboxGroup__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__CheckboxGroup__NameAssignment_1_in_rule__CheckboxGroup__Group__1__Impl1368);
                    rule__CheckboxGroup__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCheckboxGroupAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__1__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:709:1: rule__CheckboxGroup__Group__2 : rule__CheckboxGroup__Group__2__Impl rule__CheckboxGroup__Group__3 ;
    public final void rule__CheckboxGroup__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:713:1: ( rule__CheckboxGroup__Group__2__Impl rule__CheckboxGroup__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:714:2: rule__CheckboxGroup__Group__2__Impl rule__CheckboxGroup__Group__3
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__2__Impl_in_rule__CheckboxGroup__Group__21399);
            rule__CheckboxGroup__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckboxGroup__Group__3_in_rule__CheckboxGroup__Group__21402);
            rule__CheckboxGroup__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__2"


    // $ANTLR start "rule__CheckboxGroup__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:721:1: rule__CheckboxGroup__Group__2__Impl : ( '{' ) ;
    public final void rule__CheckboxGroup__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:725:1: ( ( '{' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:726:1: ( '{' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:726:1: ( '{' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:727:1: '{'
            {
             before(grammarAccess.getCheckboxGroupAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__CheckboxGroup__Group__2__Impl1430); 
             after(grammarAccess.getCheckboxGroupAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__2__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:740:1: rule__CheckboxGroup__Group__3 : rule__CheckboxGroup__Group__3__Impl rule__CheckboxGroup__Group__4 ;
    public final void rule__CheckboxGroup__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:744:1: ( rule__CheckboxGroup__Group__3__Impl rule__CheckboxGroup__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:745:2: rule__CheckboxGroup__Group__3__Impl rule__CheckboxGroup__Group__4
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__3__Impl_in_rule__CheckboxGroup__Group__31461);
            rule__CheckboxGroup__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckboxGroup__Group__4_in_rule__CheckboxGroup__Group__31464);
            rule__CheckboxGroup__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__3"


    // $ANTLR start "rule__CheckboxGroup__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:752:1: rule__CheckboxGroup__Group__3__Impl : ( ( rule__CheckboxGroup__LabelAssignment_3 ) ) ;
    public final void rule__CheckboxGroup__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:756:1: ( ( ( rule__CheckboxGroup__LabelAssignment_3 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:757:1: ( ( rule__CheckboxGroup__LabelAssignment_3 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:757:1: ( ( rule__CheckboxGroup__LabelAssignment_3 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:758:1: ( rule__CheckboxGroup__LabelAssignment_3 )
            {
             before(grammarAccess.getCheckboxGroupAccess().getLabelAssignment_3()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:759:1: ( rule__CheckboxGroup__LabelAssignment_3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:759:2: rule__CheckboxGroup__LabelAssignment_3
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__LabelAssignment_3_in_rule__CheckboxGroup__Group__3__Impl1491);
            rule__CheckboxGroup__LabelAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxGroupAccess().getLabelAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__3__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:769:1: rule__CheckboxGroup__Group__4 : rule__CheckboxGroup__Group__4__Impl rule__CheckboxGroup__Group__5 ;
    public final void rule__CheckboxGroup__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:773:1: ( rule__CheckboxGroup__Group__4__Impl rule__CheckboxGroup__Group__5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:774:2: rule__CheckboxGroup__Group__4__Impl rule__CheckboxGroup__Group__5
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__4__Impl_in_rule__CheckboxGroup__Group__41521);
            rule__CheckboxGroup__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckboxGroup__Group__5_in_rule__CheckboxGroup__Group__41524);
            rule__CheckboxGroup__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__4"


    // $ANTLR start "rule__CheckboxGroup__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:781:1: rule__CheckboxGroup__Group__4__Impl : ( ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) ) ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* ) ) ;
    public final void rule__CheckboxGroup__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:785:1: ( ( ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) ) ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:786:1: ( ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) ) ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:786:1: ( ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) ) ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:787:1: ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) ) ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:787:1: ( ( rule__CheckboxGroup__CheckboxsAssignment_4 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:788:1: ( rule__CheckboxGroup__CheckboxsAssignment_4 )
            {
             before(grammarAccess.getCheckboxGroupAccess().getCheckboxsAssignment_4()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:789:1: ( rule__CheckboxGroup__CheckboxsAssignment_4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:789:2: rule__CheckboxGroup__CheckboxsAssignment_4
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__CheckboxsAssignment_4_in_rule__CheckboxGroup__Group__4__Impl1553);
            rule__CheckboxGroup__CheckboxsAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxGroupAccess().getCheckboxsAssignment_4()); 

            }

            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:792:1: ( ( rule__CheckboxGroup__CheckboxsAssignment_4 )* )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:793:1: ( rule__CheckboxGroup__CheckboxsAssignment_4 )*
            {
             before(grammarAccess.getCheckboxGroupAccess().getCheckboxsAssignment_4()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:794:1: ( rule__CheckboxGroup__CheckboxsAssignment_4 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==19) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:794:2: rule__CheckboxGroup__CheckboxsAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__CheckboxGroup__CheckboxsAssignment_4_in_rule__CheckboxGroup__Group__4__Impl1565);
            	    rule__CheckboxGroup__CheckboxsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getCheckboxGroupAccess().getCheckboxsAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__4__Impl"


    // $ANTLR start "rule__CheckboxGroup__Group__5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:805:1: rule__CheckboxGroup__Group__5 : rule__CheckboxGroup__Group__5__Impl ;
    public final void rule__CheckboxGroup__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:809:1: ( rule__CheckboxGroup__Group__5__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:810:2: rule__CheckboxGroup__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__CheckboxGroup__Group__5__Impl_in_rule__CheckboxGroup__Group__51598);
            rule__CheckboxGroup__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__5"


    // $ANTLR start "rule__CheckboxGroup__Group__5__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:816:1: rule__CheckboxGroup__Group__5__Impl : ( '}' ) ;
    public final void rule__CheckboxGroup__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:820:1: ( ( '}' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:821:1: ( '}' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:821:1: ( '}' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:822:1: '}'
            {
             before(grammarAccess.getCheckboxGroupAccess().getRightCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_13_in_rule__CheckboxGroup__Group__5__Impl1626); 
             after(grammarAccess.getCheckboxGroupAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__Group__5__Impl"


    // $ANTLR start "rule__Checkbox__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:847:1: rule__Checkbox__Group__0 : rule__Checkbox__Group__0__Impl rule__Checkbox__Group__1 ;
    public final void rule__Checkbox__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:851:1: ( rule__Checkbox__Group__0__Impl rule__Checkbox__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:852:2: rule__Checkbox__Group__0__Impl rule__Checkbox__Group__1
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__0__Impl_in_rule__Checkbox__Group__01669);
            rule__Checkbox__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__1_in_rule__Checkbox__Group__01672);
            rule__Checkbox__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__0"


    // $ANTLR start "rule__Checkbox__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:859:1: rule__Checkbox__Group__0__Impl : ( 'Checkbox' ) ;
    public final void rule__Checkbox__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:863:1: ( ( 'Checkbox' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:864:1: ( 'Checkbox' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:864:1: ( 'Checkbox' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:865:1: 'Checkbox'
            {
             before(grammarAccess.getCheckboxAccess().getCheckboxKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__Checkbox__Group__0__Impl1700); 
             after(grammarAccess.getCheckboxAccess().getCheckboxKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__0__Impl"


    // $ANTLR start "rule__Checkbox__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:878:1: rule__Checkbox__Group__1 : rule__Checkbox__Group__1__Impl rule__Checkbox__Group__2 ;
    public final void rule__Checkbox__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:882:1: ( rule__Checkbox__Group__1__Impl rule__Checkbox__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:883:2: rule__Checkbox__Group__1__Impl rule__Checkbox__Group__2
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__1__Impl_in_rule__Checkbox__Group__11731);
            rule__Checkbox__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__2_in_rule__Checkbox__Group__11734);
            rule__Checkbox__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__1"


    // $ANTLR start "rule__Checkbox__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:890:1: rule__Checkbox__Group__1__Impl : ( ( rule__Checkbox__NameAssignment_1 )? ) ;
    public final void rule__Checkbox__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:894:1: ( ( ( rule__Checkbox__NameAssignment_1 )? ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:895:1: ( ( rule__Checkbox__NameAssignment_1 )? )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:895:1: ( ( rule__Checkbox__NameAssignment_1 )? )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:896:1: ( rule__Checkbox__NameAssignment_1 )?
            {
             before(grammarAccess.getCheckboxAccess().getNameAssignment_1()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:897:1: ( rule__Checkbox__NameAssignment_1 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:897:2: rule__Checkbox__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Checkbox__NameAssignment_1_in_rule__Checkbox__Group__1__Impl1761);
                    rule__Checkbox__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCheckboxAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__1__Impl"


    // $ANTLR start "rule__Checkbox__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:907:1: rule__Checkbox__Group__2 : rule__Checkbox__Group__2__Impl rule__Checkbox__Group__3 ;
    public final void rule__Checkbox__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:911:1: ( rule__Checkbox__Group__2__Impl rule__Checkbox__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:912:2: rule__Checkbox__Group__2__Impl rule__Checkbox__Group__3
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__2__Impl_in_rule__Checkbox__Group__21792);
            rule__Checkbox__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__3_in_rule__Checkbox__Group__21795);
            rule__Checkbox__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__2"


    // $ANTLR start "rule__Checkbox__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:919:1: rule__Checkbox__Group__2__Impl : ( '(' ) ;
    public final void rule__Checkbox__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:923:1: ( ( '(' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:924:1: ( '(' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:924:1: ( '(' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:925:1: '('
            {
             before(grammarAccess.getCheckboxAccess().getLeftParenthesisKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__Checkbox__Group__2__Impl1823); 
             after(grammarAccess.getCheckboxAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__2__Impl"


    // $ANTLR start "rule__Checkbox__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:938:1: rule__Checkbox__Group__3 : rule__Checkbox__Group__3__Impl rule__Checkbox__Group__4 ;
    public final void rule__Checkbox__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:942:1: ( rule__Checkbox__Group__3__Impl rule__Checkbox__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:943:2: rule__Checkbox__Group__3__Impl rule__Checkbox__Group__4
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__3__Impl_in_rule__Checkbox__Group__31854);
            rule__Checkbox__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__4_in_rule__Checkbox__Group__31857);
            rule__Checkbox__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__3"


    // $ANTLR start "rule__Checkbox__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:950:1: rule__Checkbox__Group__3__Impl : ( ( rule__Checkbox__LabelAssignment_3 ) ) ;
    public final void rule__Checkbox__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:954:1: ( ( ( rule__Checkbox__LabelAssignment_3 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:955:1: ( ( rule__Checkbox__LabelAssignment_3 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:955:1: ( ( rule__Checkbox__LabelAssignment_3 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:956:1: ( rule__Checkbox__LabelAssignment_3 )
            {
             before(grammarAccess.getCheckboxAccess().getLabelAssignment_3()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:957:1: ( rule__Checkbox__LabelAssignment_3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:957:2: rule__Checkbox__LabelAssignment_3
            {
            pushFollow(FOLLOW_rule__Checkbox__LabelAssignment_3_in_rule__Checkbox__Group__3__Impl1884);
            rule__Checkbox__LabelAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxAccess().getLabelAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__3__Impl"


    // $ANTLR start "rule__Checkbox__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:967:1: rule__Checkbox__Group__4 : rule__Checkbox__Group__4__Impl rule__Checkbox__Group__5 ;
    public final void rule__Checkbox__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:971:1: ( rule__Checkbox__Group__4__Impl rule__Checkbox__Group__5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:972:2: rule__Checkbox__Group__4__Impl rule__Checkbox__Group__5
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__4__Impl_in_rule__Checkbox__Group__41914);
            rule__Checkbox__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__5_in_rule__Checkbox__Group__41917);
            rule__Checkbox__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__4"


    // $ANTLR start "rule__Checkbox__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:979:1: rule__Checkbox__Group__4__Impl : ( ',' ) ;
    public final void rule__Checkbox__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:983:1: ( ( ',' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:984:1: ( ',' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:984:1: ( ',' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:985:1: ','
            {
             before(grammarAccess.getCheckboxAccess().getCommaKeyword_4()); 
            match(input,16,FOLLOW_16_in_rule__Checkbox__Group__4__Impl1945); 
             after(grammarAccess.getCheckboxAccess().getCommaKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__4__Impl"


    // $ANTLR start "rule__Checkbox__Group__5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:998:1: rule__Checkbox__Group__5 : rule__Checkbox__Group__5__Impl rule__Checkbox__Group__6 ;
    public final void rule__Checkbox__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1002:1: ( rule__Checkbox__Group__5__Impl rule__Checkbox__Group__6 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1003:2: rule__Checkbox__Group__5__Impl rule__Checkbox__Group__6
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__5__Impl_in_rule__Checkbox__Group__51976);
            rule__Checkbox__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Checkbox__Group__6_in_rule__Checkbox__Group__51979);
            rule__Checkbox__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__5"


    // $ANTLR start "rule__Checkbox__Group__5__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1010:1: rule__Checkbox__Group__5__Impl : ( ( rule__Checkbox__ValueAssignment_5 ) ) ;
    public final void rule__Checkbox__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1014:1: ( ( ( rule__Checkbox__ValueAssignment_5 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1015:1: ( ( rule__Checkbox__ValueAssignment_5 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1015:1: ( ( rule__Checkbox__ValueAssignment_5 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1016:1: ( rule__Checkbox__ValueAssignment_5 )
            {
             before(grammarAccess.getCheckboxAccess().getValueAssignment_5()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1017:1: ( rule__Checkbox__ValueAssignment_5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1017:2: rule__Checkbox__ValueAssignment_5
            {
            pushFollow(FOLLOW_rule__Checkbox__ValueAssignment_5_in_rule__Checkbox__Group__5__Impl2006);
            rule__Checkbox__ValueAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getCheckboxAccess().getValueAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__5__Impl"


    // $ANTLR start "rule__Checkbox__Group__6"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1027:1: rule__Checkbox__Group__6 : rule__Checkbox__Group__6__Impl ;
    public final void rule__Checkbox__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1031:1: ( rule__Checkbox__Group__6__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1032:2: rule__Checkbox__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Checkbox__Group__6__Impl_in_rule__Checkbox__Group__62036);
            rule__Checkbox__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__6"


    // $ANTLR start "rule__Checkbox__Group__6__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1038:1: rule__Checkbox__Group__6__Impl : ( ')' ) ;
    public final void rule__Checkbox__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1042:1: ( ( ')' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1043:1: ( ')' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1043:1: ( ')' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1044:1: ')'
            {
             before(grammarAccess.getCheckboxAccess().getRightParenthesisKeyword_6()); 
            match(input,17,FOLLOW_17_in_rule__Checkbox__Group__6__Impl2064); 
             after(grammarAccess.getCheckboxAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__Group__6__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1071:1: rule__RadioButtonGroup__Group__0 : rule__RadioButtonGroup__Group__0__Impl rule__RadioButtonGroup__Group__1 ;
    public final void rule__RadioButtonGroup__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1075:1: ( rule__RadioButtonGroup__Group__0__Impl rule__RadioButtonGroup__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1076:2: rule__RadioButtonGroup__Group__0__Impl rule__RadioButtonGroup__Group__1
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__0__Impl_in_rule__RadioButtonGroup__Group__02109);
            rule__RadioButtonGroup__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__1_in_rule__RadioButtonGroup__Group__02112);
            rule__RadioButtonGroup__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__0"


    // $ANTLR start "rule__RadioButtonGroup__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1083:1: rule__RadioButtonGroup__Group__0__Impl : ( 'RadioButtonGroup' ) ;
    public final void rule__RadioButtonGroup__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1087:1: ( ( 'RadioButtonGroup' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1088:1: ( 'RadioButtonGroup' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1088:1: ( 'RadioButtonGroup' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1089:1: 'RadioButtonGroup'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getRadioButtonGroupKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__RadioButtonGroup__Group__0__Impl2140); 
             after(grammarAccess.getRadioButtonGroupAccess().getRadioButtonGroupKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__0__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1102:1: rule__RadioButtonGroup__Group__1 : rule__RadioButtonGroup__Group__1__Impl rule__RadioButtonGroup__Group__2 ;
    public final void rule__RadioButtonGroup__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1106:1: ( rule__RadioButtonGroup__Group__1__Impl rule__RadioButtonGroup__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1107:2: rule__RadioButtonGroup__Group__1__Impl rule__RadioButtonGroup__Group__2
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__1__Impl_in_rule__RadioButtonGroup__Group__12171);
            rule__RadioButtonGroup__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__2_in_rule__RadioButtonGroup__Group__12174);
            rule__RadioButtonGroup__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__1"


    // $ANTLR start "rule__RadioButtonGroup__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1114:1: rule__RadioButtonGroup__Group__1__Impl : ( ( rule__RadioButtonGroup__NameAssignment_1 )? ) ;
    public final void rule__RadioButtonGroup__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1118:1: ( ( ( rule__RadioButtonGroup__NameAssignment_1 )? ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1119:1: ( ( rule__RadioButtonGroup__NameAssignment_1 )? )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1119:1: ( ( rule__RadioButtonGroup__NameAssignment_1 )? )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1120:1: ( rule__RadioButtonGroup__NameAssignment_1 )?
            {
             before(grammarAccess.getRadioButtonGroupAccess().getNameAssignment_1()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1121:1: ( rule__RadioButtonGroup__NameAssignment_1 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1121:2: rule__RadioButtonGroup__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__RadioButtonGroup__NameAssignment_1_in_rule__RadioButtonGroup__Group__1__Impl2201);
                    rule__RadioButtonGroup__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRadioButtonGroupAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__1__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1131:1: rule__RadioButtonGroup__Group__2 : rule__RadioButtonGroup__Group__2__Impl rule__RadioButtonGroup__Group__3 ;
    public final void rule__RadioButtonGroup__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1135:1: ( rule__RadioButtonGroup__Group__2__Impl rule__RadioButtonGroup__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1136:2: rule__RadioButtonGroup__Group__2__Impl rule__RadioButtonGroup__Group__3
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__2__Impl_in_rule__RadioButtonGroup__Group__22232);
            rule__RadioButtonGroup__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__3_in_rule__RadioButtonGroup__Group__22235);
            rule__RadioButtonGroup__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__2"


    // $ANTLR start "rule__RadioButtonGroup__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1143:1: rule__RadioButtonGroup__Group__2__Impl : ( '{' ) ;
    public final void rule__RadioButtonGroup__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1147:1: ( ( '{' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1148:1: ( '{' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1148:1: ( '{' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1149:1: '{'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__RadioButtonGroup__Group__2__Impl2263); 
             after(grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__2__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1162:1: rule__RadioButtonGroup__Group__3 : rule__RadioButtonGroup__Group__3__Impl rule__RadioButtonGroup__Group__4 ;
    public final void rule__RadioButtonGroup__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1166:1: ( rule__RadioButtonGroup__Group__3__Impl rule__RadioButtonGroup__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1167:2: rule__RadioButtonGroup__Group__3__Impl rule__RadioButtonGroup__Group__4
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__3__Impl_in_rule__RadioButtonGroup__Group__32294);
            rule__RadioButtonGroup__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__4_in_rule__RadioButtonGroup__Group__32297);
            rule__RadioButtonGroup__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__3"


    // $ANTLR start "rule__RadioButtonGroup__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1174:1: rule__RadioButtonGroup__Group__3__Impl : ( ( rule__RadioButtonGroup__LabelAssignment_3 ) ) ;
    public final void rule__RadioButtonGroup__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1178:1: ( ( ( rule__RadioButtonGroup__LabelAssignment_3 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1179:1: ( ( rule__RadioButtonGroup__LabelAssignment_3 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1179:1: ( ( rule__RadioButtonGroup__LabelAssignment_3 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1180:1: ( rule__RadioButtonGroup__LabelAssignment_3 )
            {
             before(grammarAccess.getRadioButtonGroupAccess().getLabelAssignment_3()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1181:1: ( rule__RadioButtonGroup__LabelAssignment_3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1181:2: rule__RadioButtonGroup__LabelAssignment_3
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__LabelAssignment_3_in_rule__RadioButtonGroup__Group__3__Impl2324);
            rule__RadioButtonGroup__LabelAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonGroupAccess().getLabelAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__3__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1191:1: rule__RadioButtonGroup__Group__4 : rule__RadioButtonGroup__Group__4__Impl rule__RadioButtonGroup__Group__5 ;
    public final void rule__RadioButtonGroup__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1195:1: ( rule__RadioButtonGroup__Group__4__Impl rule__RadioButtonGroup__Group__5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1196:2: rule__RadioButtonGroup__Group__4__Impl rule__RadioButtonGroup__Group__5
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__4__Impl_in_rule__RadioButtonGroup__Group__42354);
            rule__RadioButtonGroup__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__5_in_rule__RadioButtonGroup__Group__42357);
            rule__RadioButtonGroup__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__4"


    // $ANTLR start "rule__RadioButtonGroup__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1203:1: rule__RadioButtonGroup__Group__4__Impl : ( 'Checked' ) ;
    public final void rule__RadioButtonGroup__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1207:1: ( ( 'Checked' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1208:1: ( 'Checked' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1208:1: ( 'Checked' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1209:1: 'Checked'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getCheckedKeyword_4()); 
            match(input,21,FOLLOW_21_in_rule__RadioButtonGroup__Group__4__Impl2385); 
             after(grammarAccess.getRadioButtonGroupAccess().getCheckedKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__4__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1222:1: rule__RadioButtonGroup__Group__5 : rule__RadioButtonGroup__Group__5__Impl rule__RadioButtonGroup__Group__6 ;
    public final void rule__RadioButtonGroup__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1226:1: ( rule__RadioButtonGroup__Group__5__Impl rule__RadioButtonGroup__Group__6 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1227:2: rule__RadioButtonGroup__Group__5__Impl rule__RadioButtonGroup__Group__6
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__5__Impl_in_rule__RadioButtonGroup__Group__52416);
            rule__RadioButtonGroup__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__6_in_rule__RadioButtonGroup__Group__52419);
            rule__RadioButtonGroup__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__5"


    // $ANTLR start "rule__RadioButtonGroup__Group__5__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1234:1: rule__RadioButtonGroup__Group__5__Impl : ( ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 ) ) ;
    public final void rule__RadioButtonGroup__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1238:1: ( ( ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1239:1: ( ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1239:1: ( ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1240:1: ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 )
            {
             before(grammarAccess.getRadioButtonGroupAccess().getCheckedRadioButtonAssignment_5()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1241:1: ( rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1241:2: rule__RadioButtonGroup__CheckedRadioButtonAssignment_5
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__CheckedRadioButtonAssignment_5_in_rule__RadioButtonGroup__Group__5__Impl2446);
            rule__RadioButtonGroup__CheckedRadioButtonAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonGroupAccess().getCheckedRadioButtonAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__5__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__6"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1251:1: rule__RadioButtonGroup__Group__6 : rule__RadioButtonGroup__Group__6__Impl rule__RadioButtonGroup__Group__7 ;
    public final void rule__RadioButtonGroup__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1255:1: ( rule__RadioButtonGroup__Group__6__Impl rule__RadioButtonGroup__Group__7 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1256:2: rule__RadioButtonGroup__Group__6__Impl rule__RadioButtonGroup__Group__7
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__6__Impl_in_rule__RadioButtonGroup__Group__62476);
            rule__RadioButtonGroup__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__7_in_rule__RadioButtonGroup__Group__62479);
            rule__RadioButtonGroup__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__6"


    // $ANTLR start "rule__RadioButtonGroup__Group__6__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1263:1: rule__RadioButtonGroup__Group__6__Impl : ( 'Unchecked' ) ;
    public final void rule__RadioButtonGroup__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1267:1: ( ( 'Unchecked' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1268:1: ( 'Unchecked' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1268:1: ( 'Unchecked' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1269:1: 'Unchecked'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getUncheckedKeyword_6()); 
            match(input,22,FOLLOW_22_in_rule__RadioButtonGroup__Group__6__Impl2507); 
             after(grammarAccess.getRadioButtonGroupAccess().getUncheckedKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__6__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__7"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1282:1: rule__RadioButtonGroup__Group__7 : rule__RadioButtonGroup__Group__7__Impl rule__RadioButtonGroup__Group__8 ;
    public final void rule__RadioButtonGroup__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1286:1: ( rule__RadioButtonGroup__Group__7__Impl rule__RadioButtonGroup__Group__8 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1287:2: rule__RadioButtonGroup__Group__7__Impl rule__RadioButtonGroup__Group__8
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__7__Impl_in_rule__RadioButtonGroup__Group__72538);
            rule__RadioButtonGroup__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__8_in_rule__RadioButtonGroup__Group__72541);
            rule__RadioButtonGroup__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__7"


    // $ANTLR start "rule__RadioButtonGroup__Group__7__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1294:1: rule__RadioButtonGroup__Group__7__Impl : ( '{' ) ;
    public final void rule__RadioButtonGroup__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1298:1: ( ( '{' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1299:1: ( '{' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1299:1: ( '{' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1300:1: '{'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_7()); 
            match(input,12,FOLLOW_12_in_rule__RadioButtonGroup__Group__7__Impl2569); 
             after(grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__7__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__8"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1313:1: rule__RadioButtonGroup__Group__8 : rule__RadioButtonGroup__Group__8__Impl rule__RadioButtonGroup__Group__9 ;
    public final void rule__RadioButtonGroup__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1317:1: ( rule__RadioButtonGroup__Group__8__Impl rule__RadioButtonGroup__Group__9 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1318:2: rule__RadioButtonGroup__Group__8__Impl rule__RadioButtonGroup__Group__9
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__8__Impl_in_rule__RadioButtonGroup__Group__82600);
            rule__RadioButtonGroup__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__9_in_rule__RadioButtonGroup__Group__82603);
            rule__RadioButtonGroup__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__8"


    // $ANTLR start "rule__RadioButtonGroup__Group__8__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1325:1: rule__RadioButtonGroup__Group__8__Impl : ( ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )* ) ;
    public final void rule__RadioButtonGroup__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1329:1: ( ( ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )* ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1330:1: ( ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )* )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1330:1: ( ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )* )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1331:1: ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )*
            {
             before(grammarAccess.getRadioButtonGroupAccess().getUncheckedRadioButtonAssignment_8()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1332:1: ( rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1332:2: rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8
            	    {
            	    pushFollow(FOLLOW_rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8_in_rule__RadioButtonGroup__Group__8__Impl2630);
            	    rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getRadioButtonGroupAccess().getUncheckedRadioButtonAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__8__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__9"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1342:1: rule__RadioButtonGroup__Group__9 : rule__RadioButtonGroup__Group__9__Impl rule__RadioButtonGroup__Group__10 ;
    public final void rule__RadioButtonGroup__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1346:1: ( rule__RadioButtonGroup__Group__9__Impl rule__RadioButtonGroup__Group__10 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1347:2: rule__RadioButtonGroup__Group__9__Impl rule__RadioButtonGroup__Group__10
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__9__Impl_in_rule__RadioButtonGroup__Group__92661);
            rule__RadioButtonGroup__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__10_in_rule__RadioButtonGroup__Group__92664);
            rule__RadioButtonGroup__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__9"


    // $ANTLR start "rule__RadioButtonGroup__Group__9__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1354:1: rule__RadioButtonGroup__Group__9__Impl : ( '}' ) ;
    public final void rule__RadioButtonGroup__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1358:1: ( ( '}' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1359:1: ( '}' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1359:1: ( '}' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1360:1: '}'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_9()); 
            match(input,13,FOLLOW_13_in_rule__RadioButtonGroup__Group__9__Impl2692); 
             after(grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__9__Impl"


    // $ANTLR start "rule__RadioButtonGroup__Group__10"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1373:1: rule__RadioButtonGroup__Group__10 : rule__RadioButtonGroup__Group__10__Impl ;
    public final void rule__RadioButtonGroup__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1377:1: ( rule__RadioButtonGroup__Group__10__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1378:2: rule__RadioButtonGroup__Group__10__Impl
            {
            pushFollow(FOLLOW_rule__RadioButtonGroup__Group__10__Impl_in_rule__RadioButtonGroup__Group__102723);
            rule__RadioButtonGroup__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__10"


    // $ANTLR start "rule__RadioButtonGroup__Group__10__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1384:1: rule__RadioButtonGroup__Group__10__Impl : ( '}' ) ;
    public final void rule__RadioButtonGroup__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1388:1: ( ( '}' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1389:1: ( '}' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1389:1: ( '}' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1390:1: '}'
            {
             before(grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_10()); 
            match(input,13,FOLLOW_13_in_rule__RadioButtonGroup__Group__10__Impl2751); 
             after(grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__Group__10__Impl"


    // $ANTLR start "rule__RadioButton__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1425:1: rule__RadioButton__Group__0 : rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1 ;
    public final void rule__RadioButton__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1429:1: ( rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1430:2: rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__0__Impl_in_rule__RadioButton__Group__02804);
            rule__RadioButton__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__1_in_rule__RadioButton__Group__02807);
            rule__RadioButton__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__0"


    // $ANTLR start "rule__RadioButton__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1437:1: rule__RadioButton__Group__0__Impl : ( 'RadioButton' ) ;
    public final void rule__RadioButton__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1441:1: ( ( 'RadioButton' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1442:1: ( 'RadioButton' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1442:1: ( 'RadioButton' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1443:1: 'RadioButton'
            {
             before(grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0()); 
            match(input,23,FOLLOW_23_in_rule__RadioButton__Group__0__Impl2835); 
             after(grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__0__Impl"


    // $ANTLR start "rule__RadioButton__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1456:1: rule__RadioButton__Group__1 : rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2 ;
    public final void rule__RadioButton__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1460:1: ( rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1461:2: rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__1__Impl_in_rule__RadioButton__Group__12866);
            rule__RadioButton__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__2_in_rule__RadioButton__Group__12869);
            rule__RadioButton__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__1"


    // $ANTLR start "rule__RadioButton__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1468:1: rule__RadioButton__Group__1__Impl : ( ( rule__RadioButton__NameAssignment_1 )? ) ;
    public final void rule__RadioButton__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1472:1: ( ( ( rule__RadioButton__NameAssignment_1 )? ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1473:1: ( ( rule__RadioButton__NameAssignment_1 )? )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1473:1: ( ( rule__RadioButton__NameAssignment_1 )? )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1474:1: ( rule__RadioButton__NameAssignment_1 )?
            {
             before(grammarAccess.getRadioButtonAccess().getNameAssignment_1()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1475:1: ( rule__RadioButton__NameAssignment_1 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1475:2: rule__RadioButton__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__RadioButton__NameAssignment_1_in_rule__RadioButton__Group__1__Impl2896);
                    rule__RadioButton__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRadioButtonAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__1__Impl"


    // $ANTLR start "rule__RadioButton__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1485:1: rule__RadioButton__Group__2 : rule__RadioButton__Group__2__Impl rule__RadioButton__Group__3 ;
    public final void rule__RadioButton__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1489:1: ( rule__RadioButton__Group__2__Impl rule__RadioButton__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1490:2: rule__RadioButton__Group__2__Impl rule__RadioButton__Group__3
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__2__Impl_in_rule__RadioButton__Group__22927);
            rule__RadioButton__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__3_in_rule__RadioButton__Group__22930);
            rule__RadioButton__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__2"


    // $ANTLR start "rule__RadioButton__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1497:1: rule__RadioButton__Group__2__Impl : ( '(' ) ;
    public final void rule__RadioButton__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1501:1: ( ( '(' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1502:1: ( '(' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1502:1: ( '(' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1503:1: '('
            {
             before(grammarAccess.getRadioButtonAccess().getLeftParenthesisKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__RadioButton__Group__2__Impl2958); 
             after(grammarAccess.getRadioButtonAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__2__Impl"


    // $ANTLR start "rule__RadioButton__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1516:1: rule__RadioButton__Group__3 : rule__RadioButton__Group__3__Impl rule__RadioButton__Group__4 ;
    public final void rule__RadioButton__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1520:1: ( rule__RadioButton__Group__3__Impl rule__RadioButton__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1521:2: rule__RadioButton__Group__3__Impl rule__RadioButton__Group__4
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__3__Impl_in_rule__RadioButton__Group__32989);
            rule__RadioButton__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__4_in_rule__RadioButton__Group__32992);
            rule__RadioButton__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__3"


    // $ANTLR start "rule__RadioButton__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1528:1: rule__RadioButton__Group__3__Impl : ( ( rule__RadioButton__LabelAssignment_3 ) ) ;
    public final void rule__RadioButton__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1532:1: ( ( ( rule__RadioButton__LabelAssignment_3 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1533:1: ( ( rule__RadioButton__LabelAssignment_3 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1533:1: ( ( rule__RadioButton__LabelAssignment_3 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1534:1: ( rule__RadioButton__LabelAssignment_3 )
            {
             before(grammarAccess.getRadioButtonAccess().getLabelAssignment_3()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1535:1: ( rule__RadioButton__LabelAssignment_3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1535:2: rule__RadioButton__LabelAssignment_3
            {
            pushFollow(FOLLOW_rule__RadioButton__LabelAssignment_3_in_rule__RadioButton__Group__3__Impl3019);
            rule__RadioButton__LabelAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonAccess().getLabelAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__3__Impl"


    // $ANTLR start "rule__RadioButton__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1545:1: rule__RadioButton__Group__4 : rule__RadioButton__Group__4__Impl ;
    public final void rule__RadioButton__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1549:1: ( rule__RadioButton__Group__4__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1550:2: rule__RadioButton__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__4__Impl_in_rule__RadioButton__Group__43049);
            rule__RadioButton__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__4"


    // $ANTLR start "rule__RadioButton__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1556:1: rule__RadioButton__Group__4__Impl : ( ')' ) ;
    public final void rule__RadioButton__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1560:1: ( ( ')' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1561:1: ( ')' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1561:1: ( ')' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1562:1: ')'
            {
             before(grammarAccess.getRadioButtonAccess().getRightParenthesisKeyword_4()); 
            match(input,17,FOLLOW_17_in_rule__RadioButton__Group__4__Impl3077); 
             after(grammarAccess.getRadioButtonAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__4__Impl"


    // $ANTLR start "rule__Input__Group__0"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1585:1: rule__Input__Group__0 : rule__Input__Group__0__Impl rule__Input__Group__1 ;
    public final void rule__Input__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1589:1: ( rule__Input__Group__0__Impl rule__Input__Group__1 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1590:2: rule__Input__Group__0__Impl rule__Input__Group__1
            {
            pushFollow(FOLLOW_rule__Input__Group__0__Impl_in_rule__Input__Group__03118);
            rule__Input__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__1_in_rule__Input__Group__03121);
            rule__Input__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__0"


    // $ANTLR start "rule__Input__Group__0__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1597:1: rule__Input__Group__0__Impl : ( 'Input' ) ;
    public final void rule__Input__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1601:1: ( ( 'Input' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1602:1: ( 'Input' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1602:1: ( 'Input' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1603:1: 'Input'
            {
             before(grammarAccess.getInputAccess().getInputKeyword_0()); 
            match(input,24,FOLLOW_24_in_rule__Input__Group__0__Impl3149); 
             after(grammarAccess.getInputAccess().getInputKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__0__Impl"


    // $ANTLR start "rule__Input__Group__1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1616:1: rule__Input__Group__1 : rule__Input__Group__1__Impl rule__Input__Group__2 ;
    public final void rule__Input__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1620:1: ( rule__Input__Group__1__Impl rule__Input__Group__2 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1621:2: rule__Input__Group__1__Impl rule__Input__Group__2
            {
            pushFollow(FOLLOW_rule__Input__Group__1__Impl_in_rule__Input__Group__13180);
            rule__Input__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__2_in_rule__Input__Group__13183);
            rule__Input__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__1"


    // $ANTLR start "rule__Input__Group__1__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1628:1: rule__Input__Group__1__Impl : ( ( rule__Input__NameAssignment_1 )? ) ;
    public final void rule__Input__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1632:1: ( ( ( rule__Input__NameAssignment_1 )? ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1633:1: ( ( rule__Input__NameAssignment_1 )? )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1633:1: ( ( rule__Input__NameAssignment_1 )? )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1634:1: ( rule__Input__NameAssignment_1 )?
            {
             before(grammarAccess.getInputAccess().getNameAssignment_1()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1635:1: ( rule__Input__NameAssignment_1 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1635:2: rule__Input__NameAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Input__NameAssignment_1_in_rule__Input__Group__1__Impl3210);
                    rule__Input__NameAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInputAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__1__Impl"


    // $ANTLR start "rule__Input__Group__2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1645:1: rule__Input__Group__2 : rule__Input__Group__2__Impl rule__Input__Group__3 ;
    public final void rule__Input__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1649:1: ( rule__Input__Group__2__Impl rule__Input__Group__3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1650:2: rule__Input__Group__2__Impl rule__Input__Group__3
            {
            pushFollow(FOLLOW_rule__Input__Group__2__Impl_in_rule__Input__Group__23241);
            rule__Input__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__3_in_rule__Input__Group__23244);
            rule__Input__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__2"


    // $ANTLR start "rule__Input__Group__2__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1657:1: rule__Input__Group__2__Impl : ( '(' ) ;
    public final void rule__Input__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1661:1: ( ( '(' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1662:1: ( '(' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1662:1: ( '(' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1663:1: '('
            {
             before(grammarAccess.getInputAccess().getLeftParenthesisKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__Input__Group__2__Impl3272); 
             after(grammarAccess.getInputAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__2__Impl"


    // $ANTLR start "rule__Input__Group__3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1676:1: rule__Input__Group__3 : rule__Input__Group__3__Impl rule__Input__Group__4 ;
    public final void rule__Input__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1680:1: ( rule__Input__Group__3__Impl rule__Input__Group__4 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1681:2: rule__Input__Group__3__Impl rule__Input__Group__4
            {
            pushFollow(FOLLOW_rule__Input__Group__3__Impl_in_rule__Input__Group__33303);
            rule__Input__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__4_in_rule__Input__Group__33306);
            rule__Input__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__3"


    // $ANTLR start "rule__Input__Group__3__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1688:1: rule__Input__Group__3__Impl : ( ( rule__Input__LabelAssignment_3 ) ) ;
    public final void rule__Input__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1692:1: ( ( ( rule__Input__LabelAssignment_3 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1693:1: ( ( rule__Input__LabelAssignment_3 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1693:1: ( ( rule__Input__LabelAssignment_3 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1694:1: ( rule__Input__LabelAssignment_3 )
            {
             before(grammarAccess.getInputAccess().getLabelAssignment_3()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1695:1: ( rule__Input__LabelAssignment_3 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1695:2: rule__Input__LabelAssignment_3
            {
            pushFollow(FOLLOW_rule__Input__LabelAssignment_3_in_rule__Input__Group__3__Impl3333);
            rule__Input__LabelAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getLabelAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__3__Impl"


    // $ANTLR start "rule__Input__Group__4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1705:1: rule__Input__Group__4 : rule__Input__Group__4__Impl rule__Input__Group__5 ;
    public final void rule__Input__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1709:1: ( rule__Input__Group__4__Impl rule__Input__Group__5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1710:2: rule__Input__Group__4__Impl rule__Input__Group__5
            {
            pushFollow(FOLLOW_rule__Input__Group__4__Impl_in_rule__Input__Group__43363);
            rule__Input__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__5_in_rule__Input__Group__43366);
            rule__Input__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__4"


    // $ANTLR start "rule__Input__Group__4__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1717:1: rule__Input__Group__4__Impl : ( ',' ) ;
    public final void rule__Input__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1721:1: ( ( ',' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1722:1: ( ',' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1722:1: ( ',' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1723:1: ','
            {
             before(grammarAccess.getInputAccess().getCommaKeyword_4()); 
            match(input,16,FOLLOW_16_in_rule__Input__Group__4__Impl3394); 
             after(grammarAccess.getInputAccess().getCommaKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__4__Impl"


    // $ANTLR start "rule__Input__Group__5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1736:1: rule__Input__Group__5 : rule__Input__Group__5__Impl rule__Input__Group__6 ;
    public final void rule__Input__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1740:1: ( rule__Input__Group__5__Impl rule__Input__Group__6 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1741:2: rule__Input__Group__5__Impl rule__Input__Group__6
            {
            pushFollow(FOLLOW_rule__Input__Group__5__Impl_in_rule__Input__Group__53425);
            rule__Input__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Input__Group__6_in_rule__Input__Group__53428);
            rule__Input__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__5"


    // $ANTLR start "rule__Input__Group__5__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1748:1: rule__Input__Group__5__Impl : ( ( rule__Input__ValueAssignment_5 ) ) ;
    public final void rule__Input__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1752:1: ( ( ( rule__Input__ValueAssignment_5 ) ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1753:1: ( ( rule__Input__ValueAssignment_5 ) )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1753:1: ( ( rule__Input__ValueAssignment_5 ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1754:1: ( rule__Input__ValueAssignment_5 )
            {
             before(grammarAccess.getInputAccess().getValueAssignment_5()); 
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1755:1: ( rule__Input__ValueAssignment_5 )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1755:2: rule__Input__ValueAssignment_5
            {
            pushFollow(FOLLOW_rule__Input__ValueAssignment_5_in_rule__Input__Group__5__Impl3455);
            rule__Input__ValueAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getValueAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__5__Impl"


    // $ANTLR start "rule__Input__Group__6"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1765:1: rule__Input__Group__6 : rule__Input__Group__6__Impl ;
    public final void rule__Input__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1769:1: ( rule__Input__Group__6__Impl )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1770:2: rule__Input__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Input__Group__6__Impl_in_rule__Input__Group__63485);
            rule__Input__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__6"


    // $ANTLR start "rule__Input__Group__6__Impl"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1776:1: rule__Input__Group__6__Impl : ( ')' ) ;
    public final void rule__Input__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1780:1: ( ( ')' ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1781:1: ( ')' )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1781:1: ( ')' )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1782:1: ')'
            {
             before(grammarAccess.getInputAccess().getRightParenthesisKeyword_6()); 
            match(input,17,FOLLOW_17_in_rule__Input__Group__6__Impl3513); 
             after(grammarAccess.getInputAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__6__Impl"


    // $ANTLR start "rule__Widget__ItemsAssignment_2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1810:1: rule__Widget__ItemsAssignment_2 : ( ruleItem ) ;
    public final void rule__Widget__ItemsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1814:1: ( ( ruleItem ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1815:1: ( ruleItem )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1815:1: ( ruleItem )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1816:1: ruleItem
            {
             before(grammarAccess.getWidgetAccess().getItemsItemParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleItem_in_rule__Widget__ItemsAssignment_23563);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getWidgetAccess().getItemsItemParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__ItemsAssignment_2"


    // $ANTLR start "rule__Image__SourceAssignment_2"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1825:1: rule__Image__SourceAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Image__SourceAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1829:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1830:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1830:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1831:1: RULE_STRING
            {
             before(grammarAccess.getImageAccess().getSourceSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Image__SourceAssignment_23594); 
             after(grammarAccess.getImageAccess().getSourceSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__SourceAssignment_2"


    // $ANTLR start "rule__Image__AltAssignment_4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1840:1: rule__Image__AltAssignment_4 : ( RULE_STRING ) ;
    public final void rule__Image__AltAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1844:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1845:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1845:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1846:1: RULE_STRING
            {
             before(grammarAccess.getImageAccess().getAltSTRINGTerminalRuleCall_4_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Image__AltAssignment_43625); 
             after(grammarAccess.getImageAccess().getAltSTRINGTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__AltAssignment_4"


    // $ANTLR start "rule__CheckboxGroup__NameAssignment_1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1855:1: rule__CheckboxGroup__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__CheckboxGroup__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1859:1: ( ( RULE_ID ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1860:1: ( RULE_ID )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1860:1: ( RULE_ID )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1861:1: RULE_ID
            {
             before(grammarAccess.getCheckboxGroupAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__CheckboxGroup__NameAssignment_13656); 
             after(grammarAccess.getCheckboxGroupAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__NameAssignment_1"


    // $ANTLR start "rule__CheckboxGroup__LabelAssignment_3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1870:1: rule__CheckboxGroup__LabelAssignment_3 : ( RULE_STRING ) ;
    public final void rule__CheckboxGroup__LabelAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1874:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1875:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1875:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1876:1: RULE_STRING
            {
             before(grammarAccess.getCheckboxGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__CheckboxGroup__LabelAssignment_33687); 
             after(grammarAccess.getCheckboxGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__LabelAssignment_3"


    // $ANTLR start "rule__CheckboxGroup__CheckboxsAssignment_4"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1885:1: rule__CheckboxGroup__CheckboxsAssignment_4 : ( ruleCheckbox ) ;
    public final void rule__CheckboxGroup__CheckboxsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1889:1: ( ( ruleCheckbox ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1890:1: ( ruleCheckbox )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1890:1: ( ruleCheckbox )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1891:1: ruleCheckbox
            {
             before(grammarAccess.getCheckboxGroupAccess().getCheckboxsCheckboxParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleCheckbox_in_rule__CheckboxGroup__CheckboxsAssignment_43718);
            ruleCheckbox();

            state._fsp--;

             after(grammarAccess.getCheckboxGroupAccess().getCheckboxsCheckboxParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckboxGroup__CheckboxsAssignment_4"


    // $ANTLR start "rule__Checkbox__NameAssignment_1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1900:1: rule__Checkbox__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Checkbox__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1904:1: ( ( RULE_ID ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1905:1: ( RULE_ID )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1905:1: ( RULE_ID )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1906:1: RULE_ID
            {
             before(grammarAccess.getCheckboxAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Checkbox__NameAssignment_13749); 
             after(grammarAccess.getCheckboxAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__NameAssignment_1"


    // $ANTLR start "rule__Checkbox__LabelAssignment_3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1915:1: rule__Checkbox__LabelAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Checkbox__LabelAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1919:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1920:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1920:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1921:1: RULE_STRING
            {
             before(grammarAccess.getCheckboxAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Checkbox__LabelAssignment_33780); 
             after(grammarAccess.getCheckboxAccess().getLabelSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__LabelAssignment_3"


    // $ANTLR start "rule__Checkbox__ValueAssignment_5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1930:1: rule__Checkbox__ValueAssignment_5 : ( RULE_INT ) ;
    public final void rule__Checkbox__ValueAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1934:1: ( ( RULE_INT ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1935:1: ( RULE_INT )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1935:1: ( RULE_INT )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1936:1: RULE_INT
            {
             before(grammarAccess.getCheckboxAccess().getValueINTTerminalRuleCall_5_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__Checkbox__ValueAssignment_53811); 
             after(grammarAccess.getCheckboxAccess().getValueINTTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Checkbox__ValueAssignment_5"


    // $ANTLR start "rule__RadioButtonGroup__NameAssignment_1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1945:1: rule__RadioButtonGroup__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__RadioButtonGroup__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1949:1: ( ( RULE_ID ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1950:1: ( RULE_ID )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1950:1: ( RULE_ID )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1951:1: RULE_ID
            {
             before(grammarAccess.getRadioButtonGroupAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RadioButtonGroup__NameAssignment_13842); 
             after(grammarAccess.getRadioButtonGroupAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__NameAssignment_1"


    // $ANTLR start "rule__RadioButtonGroup__LabelAssignment_3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1960:1: rule__RadioButtonGroup__LabelAssignment_3 : ( RULE_STRING ) ;
    public final void rule__RadioButtonGroup__LabelAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1964:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1965:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1965:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1966:1: RULE_STRING
            {
             before(grammarAccess.getRadioButtonGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__RadioButtonGroup__LabelAssignment_33873); 
             after(grammarAccess.getRadioButtonGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__LabelAssignment_3"


    // $ANTLR start "rule__RadioButtonGroup__CheckedRadioButtonAssignment_5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1975:1: rule__RadioButtonGroup__CheckedRadioButtonAssignment_5 : ( ruleRadioButton ) ;
    public final void rule__RadioButtonGroup__CheckedRadioButtonAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1979:1: ( ( ruleRadioButton ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1980:1: ( ruleRadioButton )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1980:1: ( ruleRadioButton )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1981:1: ruleRadioButton
            {
             before(grammarAccess.getRadioButtonGroupAccess().getCheckedRadioButtonRadioButtonParserRuleCall_5_0()); 
            pushFollow(FOLLOW_ruleRadioButton_in_rule__RadioButtonGroup__CheckedRadioButtonAssignment_53904);
            ruleRadioButton();

            state._fsp--;

             after(grammarAccess.getRadioButtonGroupAccess().getCheckedRadioButtonRadioButtonParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__CheckedRadioButtonAssignment_5"


    // $ANTLR start "rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1990:1: rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8 : ( ruleRadioButton ) ;
    public final void rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1994:1: ( ( ruleRadioButton ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1995:1: ( ruleRadioButton )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1995:1: ( ruleRadioButton )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:1996:1: ruleRadioButton
            {
             before(grammarAccess.getRadioButtonGroupAccess().getUncheckedRadioButtonRadioButtonParserRuleCall_8_0()); 
            pushFollow(FOLLOW_ruleRadioButton_in_rule__RadioButtonGroup__UncheckedRadioButtonAssignment_83935);
            ruleRadioButton();

            state._fsp--;

             after(grammarAccess.getRadioButtonGroupAccess().getUncheckedRadioButtonRadioButtonParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8"


    // $ANTLR start "rule__RadioButton__NameAssignment_1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2005:1: rule__RadioButton__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__RadioButton__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2009:1: ( ( RULE_ID ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2010:1: ( RULE_ID )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2010:1: ( RULE_ID )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2011:1: RULE_ID
            {
             before(grammarAccess.getRadioButtonAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RadioButton__NameAssignment_13966); 
             after(grammarAccess.getRadioButtonAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__NameAssignment_1"


    // $ANTLR start "rule__RadioButton__LabelAssignment_3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2020:1: rule__RadioButton__LabelAssignment_3 : ( RULE_STRING ) ;
    public final void rule__RadioButton__LabelAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2024:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2025:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2025:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2026:1: RULE_STRING
            {
             before(grammarAccess.getRadioButtonAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__RadioButton__LabelAssignment_33997); 
             after(grammarAccess.getRadioButtonAccess().getLabelSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__LabelAssignment_3"


    // $ANTLR start "rule__Input__NameAssignment_1"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2035:1: rule__Input__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Input__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2039:1: ( ( RULE_ID ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2040:1: ( RULE_ID )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2040:1: ( RULE_ID )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2041:1: RULE_ID
            {
             before(grammarAccess.getInputAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Input__NameAssignment_14028); 
             after(grammarAccess.getInputAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__NameAssignment_1"


    // $ANTLR start "rule__Input__LabelAssignment_3"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2050:1: rule__Input__LabelAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Input__LabelAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2054:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2055:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2055:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2056:1: RULE_STRING
            {
             before(grammarAccess.getInputAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Input__LabelAssignment_34059); 
             after(grammarAccess.getInputAccess().getLabelSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__LabelAssignment_3"


    // $ANTLR start "rule__Input__ValueAssignment_5"
    // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2065:1: rule__Input__ValueAssignment_5 : ( RULE_STRING ) ;
    public final void rule__Input__ValueAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2069:1: ( ( RULE_STRING ) )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2070:1: ( RULE_STRING )
            {
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2070:1: ( RULE_STRING )
            // ../widget.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDsl.g:2071:1: RULE_STRING
            {
             before(grammarAccess.getInputAccess().getValueSTRINGTerminalRuleCall_5_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Input__ValueAssignment_54090); 
             after(grammarAccess.getInputAccess().getValueSTRINGTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__ValueAssignment_5"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleWidget_in_entryRuleWidget61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidget68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__Group__0_in_ruleWidget94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleItem_in_entryRuleItem121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleItem128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Item__Alternatives_in_ruleItem154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__0_in_ruleImage214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckboxGroup_in_entryRuleCheckboxGroup241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckboxGroup248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__0_in_ruleCheckboxGroup274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__0_in_ruleCheckbox334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButtonGroup_in_entryRuleRadioButtonGroup361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButtonGroup368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__0_in_ruleRadioButtonGroup394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_entryRuleRadioButton421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButton428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__0_in_ruleRadioButton454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__0_in_ruleInput514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_rule__Item__Alternatives550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckboxGroup_in_rule__Item__Alternatives567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButtonGroup_in_rule__Item__Alternatives584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_rule__Item__Alternatives601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__Group__0__Impl_in_rule__Widget__Group__0631 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Widget__Group__1_in_rule__Widget__Group__0634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__Widget__Group__0__Impl662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__Group__1__Impl_in_rule__Widget__Group__1693 = new BitSet(new long[]{0x0000000001144000L});
    public static final BitSet FOLLOW_rule__Widget__Group__2_in_rule__Widget__Group__1696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Widget__Group__1__Impl724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__Group__2__Impl_in_rule__Widget__Group__2755 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Widget__Group__3_in_rule__Widget__Group__2758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__ItemsAssignment_2_in_rule__Widget__Group__2__Impl787 = new BitSet(new long[]{0x0000000001144002L});
    public static final BitSet FOLLOW_rule__Widget__ItemsAssignment_2_in_rule__Widget__Group__2__Impl799 = new BitSet(new long[]{0x0000000001144002L});
    public static final BitSet FOLLOW_rule__Widget__Group__3__Impl_in_rule__Widget__Group__3832 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Widget__Group__3__Impl860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__0__Impl_in_rule__Image__Group__0899 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Image__Group__1_in_rule__Image__Group__0902 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Image__Group__0__Impl930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__1__Impl_in_rule__Image__Group__1961 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Image__Group__2_in_rule__Image__Group__1964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Image__Group__1__Impl992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__2__Impl_in_rule__Image__Group__21023 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Image__Group__3_in_rule__Image__Group__21026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__SourceAssignment_2_in_rule__Image__Group__2__Impl1053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__3__Impl_in_rule__Image__Group__31083 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Image__Group__4_in_rule__Image__Group__31086 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Image__Group__3__Impl1114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__4__Impl_in_rule__Image__Group__41145 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Image__Group__5_in_rule__Image__Group__41148 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__AltAssignment_4_in_rule__Image__Group__4__Impl1175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__5__Impl_in_rule__Image__Group__51205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Image__Group__5__Impl1233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__0__Impl_in_rule__CheckboxGroup__Group__01276 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__1_in_rule__CheckboxGroup__Group__01279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__CheckboxGroup__Group__0__Impl1307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__1__Impl_in_rule__CheckboxGroup__Group__11338 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__2_in_rule__CheckboxGroup__Group__11341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__NameAssignment_1_in_rule__CheckboxGroup__Group__1__Impl1368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__2__Impl_in_rule__CheckboxGroup__Group__21399 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__3_in_rule__CheckboxGroup__Group__21402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__CheckboxGroup__Group__2__Impl1430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__3__Impl_in_rule__CheckboxGroup__Group__31461 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__4_in_rule__CheckboxGroup__Group__31464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__LabelAssignment_3_in_rule__CheckboxGroup__Group__3__Impl1491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__4__Impl_in_rule__CheckboxGroup__Group__41521 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__5_in_rule__CheckboxGroup__Group__41524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__CheckboxsAssignment_4_in_rule__CheckboxGroup__Group__4__Impl1553 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__CheckboxsAssignment_4_in_rule__CheckboxGroup__Group__4__Impl1565 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__CheckboxGroup__Group__5__Impl_in_rule__CheckboxGroup__Group__51598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__CheckboxGroup__Group__5__Impl1626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__0__Impl_in_rule__Checkbox__Group__01669 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__1_in_rule__Checkbox__Group__01672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Checkbox__Group__0__Impl1700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__1__Impl_in_rule__Checkbox__Group__11731 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__2_in_rule__Checkbox__Group__11734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__NameAssignment_1_in_rule__Checkbox__Group__1__Impl1761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__2__Impl_in_rule__Checkbox__Group__21792 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__3_in_rule__Checkbox__Group__21795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Checkbox__Group__2__Impl1823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__3__Impl_in_rule__Checkbox__Group__31854 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__4_in_rule__Checkbox__Group__31857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__LabelAssignment_3_in_rule__Checkbox__Group__3__Impl1884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__4__Impl_in_rule__Checkbox__Group__41914 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__5_in_rule__Checkbox__Group__41917 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Checkbox__Group__4__Impl1945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__5__Impl_in_rule__Checkbox__Group__51976 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__6_in_rule__Checkbox__Group__51979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__ValueAssignment_5_in_rule__Checkbox__Group__5__Impl2006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Checkbox__Group__6__Impl_in_rule__Checkbox__Group__62036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Checkbox__Group__6__Impl2064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__0__Impl_in_rule__RadioButtonGroup__Group__02109 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__1_in_rule__RadioButtonGroup__Group__02112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__RadioButtonGroup__Group__0__Impl2140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__1__Impl_in_rule__RadioButtonGroup__Group__12171 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__2_in_rule__RadioButtonGroup__Group__12174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__NameAssignment_1_in_rule__RadioButtonGroup__Group__1__Impl2201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__2__Impl_in_rule__RadioButtonGroup__Group__22232 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__3_in_rule__RadioButtonGroup__Group__22235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__RadioButtonGroup__Group__2__Impl2263 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__3__Impl_in_rule__RadioButtonGroup__Group__32294 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__4_in_rule__RadioButtonGroup__Group__32297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__LabelAssignment_3_in_rule__RadioButtonGroup__Group__3__Impl2324 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__4__Impl_in_rule__RadioButtonGroup__Group__42354 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__5_in_rule__RadioButtonGroup__Group__42357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__RadioButtonGroup__Group__4__Impl2385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__5__Impl_in_rule__RadioButtonGroup__Group__52416 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__6_in_rule__RadioButtonGroup__Group__52419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__CheckedRadioButtonAssignment_5_in_rule__RadioButtonGroup__Group__5__Impl2446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__6__Impl_in_rule__RadioButtonGroup__Group__62476 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__7_in_rule__RadioButtonGroup__Group__62479 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__RadioButtonGroup__Group__6__Impl2507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__7__Impl_in_rule__RadioButtonGroup__Group__72538 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__8_in_rule__RadioButtonGroup__Group__72541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__RadioButtonGroup__Group__7__Impl2569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__8__Impl_in_rule__RadioButtonGroup__Group__82600 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__9_in_rule__RadioButtonGroup__Group__82603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__UncheckedRadioButtonAssignment_8_in_rule__RadioButtonGroup__Group__8__Impl2630 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__9__Impl_in_rule__RadioButtonGroup__Group__92661 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__10_in_rule__RadioButtonGroup__Group__92664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__RadioButtonGroup__Group__9__Impl2692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButtonGroup__Group__10__Impl_in_rule__RadioButtonGroup__Group__102723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__RadioButtonGroup__Group__10__Impl2751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__0__Impl_in_rule__RadioButton__Group__02804 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__1_in_rule__RadioButton__Group__02807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__RadioButton__Group__0__Impl2835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__1__Impl_in_rule__RadioButton__Group__12866 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__2_in_rule__RadioButton__Group__12869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__NameAssignment_1_in_rule__RadioButton__Group__1__Impl2896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__2__Impl_in_rule__RadioButton__Group__22927 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__3_in_rule__RadioButton__Group__22930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__RadioButton__Group__2__Impl2958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__3__Impl_in_rule__RadioButton__Group__32989 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__4_in_rule__RadioButton__Group__32992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__LabelAssignment_3_in_rule__RadioButton__Group__3__Impl3019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__4__Impl_in_rule__RadioButton__Group__43049 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__RadioButton__Group__4__Impl3077 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__0__Impl_in_rule__Input__Group__03118 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__Input__Group__1_in_rule__Input__Group__03121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Input__Group__0__Impl3149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__1__Impl_in_rule__Input__Group__13180 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_rule__Input__Group__2_in_rule__Input__Group__13183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__NameAssignment_1_in_rule__Input__Group__1__Impl3210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__2__Impl_in_rule__Input__Group__23241 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Input__Group__3_in_rule__Input__Group__23244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Input__Group__2__Impl3272 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__3__Impl_in_rule__Input__Group__33303 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Input__Group__4_in_rule__Input__Group__33306 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__LabelAssignment_3_in_rule__Input__Group__3__Impl3333 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__4__Impl_in_rule__Input__Group__43363 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Input__Group__5_in_rule__Input__Group__43366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Input__Group__4__Impl3394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__5__Impl_in_rule__Input__Group__53425 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Input__Group__6_in_rule__Input__Group__53428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__ValueAssignment_5_in_rule__Input__Group__5__Impl3455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Input__Group__6__Impl_in_rule__Input__Group__63485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Input__Group__6__Impl3513 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleItem_in_rule__Widget__ItemsAssignment_23563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Image__SourceAssignment_23594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Image__AltAssignment_43625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__CheckboxGroup__NameAssignment_13656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__CheckboxGroup__LabelAssignment_33687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_rule__CheckboxGroup__CheckboxsAssignment_43718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Checkbox__NameAssignment_13749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Checkbox__LabelAssignment_33780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__Checkbox__ValueAssignment_53811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RadioButtonGroup__NameAssignment_13842 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__RadioButtonGroup__LabelAssignment_33873 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_rule__RadioButtonGroup__CheckedRadioButtonAssignment_53904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_rule__RadioButtonGroup__UncheckedRadioButtonAssignment_83935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RadioButton__NameAssignment_13966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__RadioButton__LabelAssignment_33997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Input__NameAssignment_14028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Input__LabelAssignment_34059 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Input__ValueAssignment_54090 = new BitSet(new long[]{0x0000000000000002L});

}