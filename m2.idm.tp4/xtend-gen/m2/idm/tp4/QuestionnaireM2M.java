package m2.idm.tp4;

import java.io.FileWriter;
import java.util.HashMap;
import m2.idm.tp2.QuestionnaireStandaloneSetupGenerated;
import m2.idm.tp2.questionnaire.Poll;
import m2.idm.tp2.questionnaire.PollSystem;
import m2.idm.tp3.UIQuestionnaireStandaloneSetupGenerated;
import m2.idm.tp3.uIQuestionnaire.UIQuestionnaire;
import m2.idm.tp4.HTMLGenerator;
import m2.idm.tp4.ModelToModel;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class QuestionnaireM2M {
  public PollSystem loadQuestPollS(final URI uri) {
    PollSystem _xblockexpression = null;
    {
      QuestionnaireStandaloneSetupGenerated _questionnaireStandaloneSetupGenerated = new QuestionnaireStandaloneSetupGenerated();
      _questionnaireStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource res = _resourceSetImpl.getResource(uri, true);
      EList<EObject> _contents = res.getContents();
      EObject _get = _contents.get(0);
      _xblockexpression = (((PollSystem) _get));
    }
    return _xblockexpression;
  }
  
  public UIQuestionnaire loadUIQuestPollS(final URI uri) {
    UIQuestionnaire _xblockexpression = null;
    {
      UIQuestionnaireStandaloneSetupGenerated _uIQuestionnaireStandaloneSetupGenerated = new UIQuestionnaireStandaloneSetupGenerated();
      _uIQuestionnaireStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource res = _resourceSetImpl.getResource(uri, true);
      EList<EObject> _contents = res.getContents();
      EObject _get = _contents.get(0);
      _xblockexpression = (((UIQuestionnaire) _get));
    }
    return _xblockexpression;
  }
  
  public void savePollSystem(final URI uri, final m2.idm.tp3.uIMM.PollSystem pollS) {
    try {
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource rs = _resourceSetImpl.createResource(uri);
      EList<EObject> _contents = rs.getContents();
      _contents.add(pollS);
      HashMap<Object,Object> _hashMap = new HashMap<Object, Object>();
      rs.save(_hashMap);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void test1() {
    try {
      URI _createURI = URI.createURI("foo1.poll");
      PollSystem pollS = this.loadQuestPollS(_createURI);
      URI _createURI_1 = URI.createURI("foo1.uiPoll");
      UIQuestionnaire ui = this.loadUIQuestPollS(_createURI_1);
      Assert.assertNotNull(pollS);
      Assert.assertNotNull(ui);
      EList<Poll> _polls = pollS.getPolls();
      int _size = _polls.size();
      Assert.assertEquals(2, _size);
      ModelToModel _modelToModel = new ModelToModel();
      ModelToModel m2m = _modelToModel;
      m2.idm.tp3.uIMM.PollSystem dispoll = m2m.questToUIMM(pollS, ui);
      URI _createURI_2 = URI.createURI("foo1.xmi");
      this.savePollSystem(_createURI_2, dispoll);
      HTMLGenerator _hTMLGenerator = new HTMLGenerator();
      HTMLGenerator htmlGenerator = _hTMLGenerator;
      CharSequence html = htmlGenerator.generate(dispoll);
      Assert.assertNotNull(html);
      FileWriter _fileWriter = new FileWriter("foo1.html");
      final FileWriter fw = _fileWriter;
      String _string = html.toString();
      fw.write(_string);
      fw.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
