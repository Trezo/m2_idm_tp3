/*
 * generated by Xtext
 */
package m2.idm.tp3.scoping

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class UIMMScopeProvider extends org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider {

}
