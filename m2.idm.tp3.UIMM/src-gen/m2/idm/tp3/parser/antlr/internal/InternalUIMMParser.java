package m2.idm.tp3.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import m2.idm.tp3.services.UIMMGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUIMMParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Poll'", "'{'", "'}'", "'Question'", "'type:'", "'options'", "'=>'", "'Image'", "'Checkbox'", "'Radio'", "'Select'", "'Input'"
    };
    public static final int RULE_ID=4;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUIMMParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUIMMParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUIMMParser.tokenNames; }
    public String getGrammarFileName() { return "../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g"; }



     	private UIMMGrammarAccess grammarAccess;
     	
        public InternalUIMMParser(TokenStream input, UIMMGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "PollSystem";	
       	}
       	
       	@Override
       	protected UIMMGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulePollSystem"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:67:1: entryRulePollSystem returns [EObject current=null] : iv_rulePollSystem= rulePollSystem EOF ;
    public final EObject entryRulePollSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePollSystem = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:68:2: (iv_rulePollSystem= rulePollSystem EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:69:2: iv_rulePollSystem= rulePollSystem EOF
            {
             newCompositeNode(grammarAccess.getPollSystemRule()); 
            pushFollow(FOLLOW_rulePollSystem_in_entryRulePollSystem75);
            iv_rulePollSystem=rulePollSystem();

            state._fsp--;

             current =iv_rulePollSystem; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePollSystem85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePollSystem"


    // $ANTLR start "rulePollSystem"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:76:1: rulePollSystem returns [EObject current=null] : ( (lv_polls_0_0= rulePoll ) )* ;
    public final EObject rulePollSystem() throws RecognitionException {
        EObject current = null;

        EObject lv_polls_0_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:79:28: ( ( (lv_polls_0_0= rulePoll ) )* )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:80:1: ( (lv_polls_0_0= rulePoll ) )*
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:80:1: ( (lv_polls_0_0= rulePoll ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:81:1: (lv_polls_0_0= rulePoll )
            	    {
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:81:1: (lv_polls_0_0= rulePoll )
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:82:3: lv_polls_0_0= rulePoll
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePoll_in_rulePollSystem130);
            	    lv_polls_0_0=rulePoll();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPollSystemRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"polls",
            	            		lv_polls_0_0, 
            	            		"Poll");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePollSystem"


    // $ANTLR start "entryRulePoll"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:106:1: entryRulePoll returns [EObject current=null] : iv_rulePoll= rulePoll EOF ;
    public final EObject entryRulePoll() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePoll = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:107:2: (iv_rulePoll= rulePoll EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:108:2: iv_rulePoll= rulePoll EOF
            {
             newCompositeNode(grammarAccess.getPollRule()); 
            pushFollow(FOLLOW_rulePoll_in_entryRulePoll166);
            iv_rulePoll=rulePoll();

            state._fsp--;

             current =iv_rulePoll; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePoll176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePoll"


    // $ANTLR start "rulePoll"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:115:1: rulePoll returns [EObject current=null] : (otherlv_0= 'Poll' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_questions_3_0= ruleQuestion ) )+ otherlv_4= '}' ) ;
    public final EObject rulePoll() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_questions_3_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:118:28: ( (otherlv_0= 'Poll' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_questions_3_0= ruleQuestion ) )+ otherlv_4= '}' ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:119:1: (otherlv_0= 'Poll' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_questions_3_0= ruleQuestion ) )+ otherlv_4= '}' )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:119:1: (otherlv_0= 'Poll' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_questions_3_0= ruleQuestion ) )+ otherlv_4= '}' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:119:3: otherlv_0= 'Poll' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_questions_3_0= ruleQuestion ) )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulePoll213); 

                	newLeafNode(otherlv_0, grammarAccess.getPollAccess().getPollKeyword_0());
                
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:123:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:124:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:124:1: (lv_name_1_0= RULE_ID )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:125:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePoll230); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getPollAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPollRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_rulePoll248); 

                	newLeafNode(otherlv_2, grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2());
                
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:145:1: ( (lv_questions_3_0= ruleQuestion ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==14) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:146:1: (lv_questions_3_0= ruleQuestion )
            	    {
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:146:1: (lv_questions_3_0= ruleQuestion )
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:147:3: lv_questions_3_0= ruleQuestion
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPollAccess().getQuestionsQuestionParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleQuestion_in_rulePoll269);
            	    lv_questions_3_0=ruleQuestion();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPollRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"questions",
            	            		lv_questions_3_0, 
            	            		"Question");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_rulePoll282); 

                	newLeafNode(otherlv_4, grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePoll"


    // $ANTLR start "entryRuleQuestion"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:175:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:176:2: (iv_ruleQuestion= ruleQuestion EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:177:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion318);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion328); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:184:1: ruleQuestion returns [EObject current=null] : (otherlv_0= 'Question' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )? otherlv_6= 'options' ( (lv_options_7_0= ruleOption ) )+ otherlv_8= '}' ) ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_text_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_widget_5_0 = null;

        EObject lv_options_7_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:187:28: ( (otherlv_0= 'Question' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )? otherlv_6= 'options' ( (lv_options_7_0= ruleOption ) )+ otherlv_8= '}' ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:188:1: (otherlv_0= 'Question' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )? otherlv_6= 'options' ( (lv_options_7_0= ruleOption ) )+ otherlv_8= '}' )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:188:1: (otherlv_0= 'Question' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )? otherlv_6= 'options' ( (lv_options_7_0= ruleOption ) )+ otherlv_8= '}' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:188:3: otherlv_0= 'Question' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )? otherlv_6= 'options' ( (lv_options_7_0= ruleOption ) )+ otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleQuestion365); 

                	newLeafNode(otherlv_0, grammarAccess.getQuestionAccess().getQuestionKeyword_0());
                
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:192:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:193:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:193:1: (lv_name_1_0= RULE_ID )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:194:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQuestion382); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getQuestionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleQuestion400); 

                	newLeafNode(otherlv_2, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2());
                
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:214:1: ( (lv_text_3_0= RULE_STRING ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:215:1: (lv_text_3_0= RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:215:1: (lv_text_3_0= RULE_STRING )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:216:3: lv_text_3_0= RULE_STRING
            {
            lv_text_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleQuestion417); 

            			newLeafNode(lv_text_3_0, grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuestionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_3_0, 
                    		"STRING");
            	    

            }


            }

            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:232:2: (otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:232:4: otherlv_4= 'type:' ( (lv_widget_5_0= ruleWidgetQuestType ) )
                    {
                    otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleQuestion435); 

                        	newLeafNode(otherlv_4, grammarAccess.getQuestionAccess().getTypeKeyword_4_0());
                        
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:236:1: ( (lv_widget_5_0= ruleWidgetQuestType ) )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:237:1: (lv_widget_5_0= ruleWidgetQuestType )
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:237:1: (lv_widget_5_0= ruleWidgetQuestType )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:238:3: lv_widget_5_0= ruleWidgetQuestType
                    {
                     
                    	        newCompositeNode(grammarAccess.getQuestionAccess().getWidgetWidgetQuestTypeParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWidgetQuestType_in_ruleQuestion456);
                    lv_widget_5_0=ruleWidgetQuestType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getQuestionRule());
                    	        }
                           		set(
                           			current, 
                           			"widget",
                            		lv_widget_5_0, 
                            		"WidgetQuestType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,16,FOLLOW_16_in_ruleQuestion470); 

                	newLeafNode(otherlv_6, grammarAccess.getQuestionAccess().getOptionsKeyword_5());
                
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:258:1: ( (lv_options_7_0= ruleOption ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_ID && LA6_0<=RULE_STRING)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:259:1: (lv_options_7_0= ruleOption )
            	    {
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:259:1: (lv_options_7_0= ruleOption )
            	    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:260:3: lv_options_7_0= ruleOption
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOption_in_ruleQuestion491);
            	    lv_options_7_0=ruleOption();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQuestionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"options",
            	            		lv_options_7_0, 
            	            		"Option");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            otherlv_8=(Token)match(input,13,FOLLOW_13_in_ruleQuestion504); 

                	newLeafNode(otherlv_8, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:288:1: entryRuleOption returns [EObject current=null] : iv_ruleOption= ruleOption EOF ;
    public final EObject entryRuleOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOption = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:289:2: (iv_ruleOption= ruleOption EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:290:2: iv_ruleOption= ruleOption EOF
            {
             newCompositeNode(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption540);
            iv_ruleOption=ruleOption();

            state._fsp--;

             current =iv_ruleOption; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption550); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:297:1: ruleOption returns [EObject current=null] : ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )? ) ;
    public final EObject ruleOption() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_text_2_0=null;
        Token otherlv_3=null;
        EObject lv_widget_4_0 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:300:28: ( ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )? ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:1: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )? )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:1: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )? )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )?
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=>'
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:301:3: ( (lv_name_0_0= RULE_ID ) )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:302:1: (lv_name_0_0= RULE_ID )
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:302:1: (lv_name_0_0= RULE_ID )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:303:3: lv_name_0_0= RULE_ID
                    {
                    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOption593); 

                    			newLeafNode(lv_name_0_0, grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOptionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_0_0, 
                            		"ID");
                    	    

                    }


                    }

                    otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleOption610); 

                        	newLeafNode(otherlv_1, grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1());
                        

                    }
                    break;

            }

            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:323:3: ( (lv_text_2_0= RULE_STRING ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:324:1: (lv_text_2_0= RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:324:1: (lv_text_2_0= RULE_STRING )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:325:3: lv_text_2_0= RULE_STRING
            {
            lv_text_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleOption629); 

            			newLeafNode(lv_text_2_0, grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOptionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_2_0, 
                    		"STRING");
            	    

            }


            }

            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:341:2: (otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:341:4: otherlv_3= 'type:' ( (lv_widget_4_0= ruleWidgetOptType ) )
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_15_in_ruleOption647); 

                        	newLeafNode(otherlv_3, grammarAccess.getOptionAccess().getTypeKeyword_2_0());
                        
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:345:1: ( (lv_widget_4_0= ruleWidgetOptType ) )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:346:1: (lv_widget_4_0= ruleWidgetOptType )
                    {
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:346:1: (lv_widget_4_0= ruleWidgetOptType )
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:347:3: lv_widget_4_0= ruleWidgetOptType
                    {
                     
                    	        newCompositeNode(grammarAccess.getOptionAccess().getWidgetWidgetOptTypeParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleWidgetOptType_in_ruleOption668);
                    lv_widget_4_0=ruleWidgetOptType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getOptionRule());
                    	        }
                           		set(
                           			current, 
                           			"widget",
                            		lv_widget_4_0, 
                            		"WidgetOptType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOption"


    // $ANTLR start "entryRuleWidgetQuestType"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:371:1: entryRuleWidgetQuestType returns [String current=null] : iv_ruleWidgetQuestType= ruleWidgetQuestType EOF ;
    public final String entryRuleWidgetQuestType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleWidgetQuestType = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:372:2: (iv_ruleWidgetQuestType= ruleWidgetQuestType EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:373:2: iv_ruleWidgetQuestType= ruleWidgetQuestType EOF
            {
             newCompositeNode(grammarAccess.getWidgetQuestTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType707);
            iv_ruleWidgetQuestType=ruleWidgetQuestType();

            state._fsp--;

             current =iv_ruleWidgetQuestType.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuestType718); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetQuestType"


    // $ANTLR start "ruleWidgetQuestType"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:380:1: ruleWidgetQuestType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect ) ;
    public final AntlrDatatypeRuleToken ruleWidgetQuestType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_Checkbox_0 = null;

        AntlrDatatypeRuleToken this_Radio_1 = null;

        AntlrDatatypeRuleToken this_Select_2 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:383:28: ( (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:384:1: (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:384:1: (this_Checkbox_0= ruleCheckbox | this_Radio_1= ruleRadio | this_Select_2= ruleSelect )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt9=1;
                }
                break;
            case 20:
                {
                alt9=2;
                }
                break;
            case 21:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:385:5: this_Checkbox_0= ruleCheckbox
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleCheckbox_in_ruleWidgetQuestType765);
                    this_Checkbox_0=ruleCheckbox();

                    state._fsp--;


                    		current.merge(this_Checkbox_0);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:397:5: this_Radio_1= ruleRadio
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleRadio_in_ruleWidgetQuestType798);
                    this_Radio_1=ruleRadio();

                    state._fsp--;


                    		current.merge(this_Radio_1);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:409:5: this_Select_2= ruleSelect
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleSelect_in_ruleWidgetQuestType831);
                    this_Select_2=ruleSelect();

                    state._fsp--;


                    		current.merge(this_Select_2);
                        
                     
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetQuestType"


    // $ANTLR start "entryRuleWidgetOptType"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:427:1: entryRuleWidgetOptType returns [EObject current=null] : iv_ruleWidgetOptType= ruleWidgetOptType EOF ;
    public final EObject entryRuleWidgetOptType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidgetOptType = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:428:2: (iv_ruleWidgetOptType= ruleWidgetOptType EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:429:2: iv_ruleWidgetOptType= ruleWidgetOptType EOF
            {
             newCompositeNode(grammarAccess.getWidgetOptTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType876);
            iv_ruleWidgetOptType=ruleWidgetOptType();

            state._fsp--;

             current =iv_ruleWidgetOptType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptType886); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidgetOptType"


    // $ANTLR start "ruleWidgetOptType"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:436:1: ruleWidgetOptType returns [EObject current=null] : ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleWidgetOptType() throws RecognitionException {
        EObject current = null;

        Token lv_param_1_0=null;
        AntlrDatatypeRuleToken lv_type_0_1 = null;

        AntlrDatatypeRuleToken lv_type_0_2 = null;


         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:439:28: ( ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:440:1: ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:440:1: ( ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:440:2: ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) ) ( (lv_param_1_0= RULE_STRING ) )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:440:2: ( ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:441:1: ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:441:1: ( (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:442:1: (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:442:1: (lv_type_0_1= ruleImage | lv_type_0_2= ruleInput )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==18) ) {
                alt10=1;
            }
            else if ( (LA10_0==22) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:443:3: lv_type_0_1= ruleImage
                    {
                     
                    	        newCompositeNode(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleImage_in_ruleWidgetOptType934);
                    lv_type_0_1=ruleImage();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWidgetOptTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_0_1, 
                            		"Image");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:458:8: lv_type_0_2= ruleInput
                    {
                     
                    	        newCompositeNode(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 
                    	    
                    pushFollow(FOLLOW_ruleInput_in_ruleWidgetOptType953);
                    lv_type_0_2=ruleInput();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getWidgetOptTypeRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_0_2, 
                            		"Input");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }

            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:476:2: ( (lv_param_1_0= RULE_STRING ) )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:477:1: (lv_param_1_0= RULE_STRING )
            {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:477:1: (lv_param_1_0= RULE_STRING )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:478:3: lv_param_1_0= RULE_STRING
            {
            lv_param_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleWidgetOptType973); 

            			newLeafNode(lv_param_1_0, grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getWidgetOptTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"param",
                    		lv_param_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidgetOptType"


    // $ANTLR start "entryRuleImage"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:502:1: entryRuleImage returns [String current=null] : iv_ruleImage= ruleImage EOF ;
    public final String entryRuleImage() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleImage = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:503:2: (iv_ruleImage= ruleImage EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:504:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage1015);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage1026); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:511:1: ruleImage returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Image' ;
    public final AntlrDatatypeRuleToken ruleImage() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:514:28: (kw= 'Image' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:516:2: kw= 'Image'
            {
            kw=(Token)match(input,18,FOLLOW_18_in_ruleImage1063); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getImageAccess().getImageKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckbox"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:529:1: entryRuleCheckbox returns [String current=null] : iv_ruleCheckbox= ruleCheckbox EOF ;
    public final String entryRuleCheckbox() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCheckbox = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:530:2: (iv_ruleCheckbox= ruleCheckbox EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:531:2: iv_ruleCheckbox= ruleCheckbox EOF
            {
             newCompositeNode(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox1103);
            iv_ruleCheckbox=ruleCheckbox();

            state._fsp--;

             current =iv_ruleCheckbox.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox1114); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:538:1: ruleCheckbox returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Checkbox' ;
    public final AntlrDatatypeRuleToken ruleCheckbox() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:541:28: (kw= 'Checkbox' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:543:2: kw= 'Checkbox'
            {
            kw=(Token)match(input,19,FOLLOW_19_in_ruleCheckbox1151); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadio"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:556:1: entryRuleRadio returns [String current=null] : iv_ruleRadio= ruleRadio EOF ;
    public final String entryRuleRadio() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRadio = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:557:2: (iv_ruleRadio= ruleRadio EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:558:2: iv_ruleRadio= ruleRadio EOF
            {
             newCompositeNode(grammarAccess.getRadioRule()); 
            pushFollow(FOLLOW_ruleRadio_in_entryRuleRadio1191);
            iv_ruleRadio=ruleRadio();

            state._fsp--;

             current =iv_ruleRadio.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadio1202); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRadio"


    // $ANTLR start "ruleRadio"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:565:1: ruleRadio returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Radio' ;
    public final AntlrDatatypeRuleToken ruleRadio() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:568:28: (kw= 'Radio' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:570:2: kw= 'Radio'
            {
            kw=(Token)match(input,20,FOLLOW_20_in_ruleRadio1239); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getRadioAccess().getRadioKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRadio"


    // $ANTLR start "entryRuleSelect"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:583:1: entryRuleSelect returns [String current=null] : iv_ruleSelect= ruleSelect EOF ;
    public final String entryRuleSelect() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSelect = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:584:2: (iv_ruleSelect= ruleSelect EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:585:2: iv_ruleSelect= ruleSelect EOF
            {
             newCompositeNode(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_ruleSelect_in_entryRuleSelect1279);
            iv_ruleSelect=ruleSelect();

            state._fsp--;

             current =iv_ruleSelect.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelect1290); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:592:1: ruleSelect returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Select' ;
    public final AntlrDatatypeRuleToken ruleSelect() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:595:28: (kw= 'Select' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:597:2: kw= 'Select'
            {
            kw=(Token)match(input,21,FOLLOW_21_in_ruleSelect1327); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getSelectAccess().getSelectKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleInput"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:610:1: entryRuleInput returns [String current=null] : iv_ruleInput= ruleInput EOF ;
    public final String entryRuleInput() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInput = null;


        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:611:2: (iv_ruleInput= ruleInput EOF )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:612:2: iv_ruleInput= ruleInput EOF
            {
             newCompositeNode(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput1367);
            iv_ruleInput=ruleInput();

            state._fsp--;

             current =iv_ruleInput.getText(); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput1378); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:619:1: ruleInput returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= 'Input' ;
    public final AntlrDatatypeRuleToken ruleInput() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:622:28: (kw= 'Input' )
            // ../m2.idm.tp3.UIMM/src-gen/m2/idm/tp3/parser/antlr/internal/InternalUIMM.g:624:2: kw= 'Input'
            {
            kw=(Token)match(input,22,FOLLOW_22_in_ruleInput1415); 

                    current.merge(kw);
                    newLeafNode(kw, grammarAccess.getInputAccess().getInputKeyword()); 
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePollSystem_in_entryRulePollSystem75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePollSystem85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePoll_in_rulePollSystem130 = new BitSet(new long[]{0x0000000000000802L});
    public static final BitSet FOLLOW_rulePoll_in_entryRulePoll166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePoll176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulePoll213 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePoll230 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePoll248 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_ruleQuestion_in_rulePoll269 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_13_in_rulePoll282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion318 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleQuestion365 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQuestion382 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleQuestion400 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleQuestion417 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_ruleQuestion435 = new BitSet(new long[]{0x0000000000380000L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_ruleQuestion456 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleQuestion470 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_ruleOption_in_ruleQuestion491 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_13_in_ruleQuestion504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption540 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOption593 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleOption610 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleOption629 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_15_in_ruleOption647 = new BitSet(new long[]{0x0000000000440000L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_ruleOption668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType707 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuestType718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_ruleWidgetQuestType765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_ruleWidgetQuestType798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_ruleWidgetQuestType831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType876 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptType886 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_ruleWidgetOptType934 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ruleInput_in_ruleWidgetOptType953 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleWidgetOptType973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage1015 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage1026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleImage1063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox1103 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox1114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleCheckbox1151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_entryRuleRadio1191 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadio1202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRadio1239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_entryRuleSelect1279 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelect1290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleSelect1327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput1367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput1378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleInput1415 = new BitSet(new long[]{0x0000000000000002L});

}