/*
* generated by Xtext
*/
package m2.idm.tp3.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class UIMMAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("m2/idm/tp3/parser/antlr/internal/InternalUIMM.tokens");
	}
}
