/*
* generated by Xtext
*/
package m2.idm.tp3.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import m2.idm.tp3.services.UIMMGrammarAccess;

public class UIMMParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private UIMMGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected m2.idm.tp3.parser.antlr.internal.InternalUIMMParser createParser(XtextTokenStream stream) {
		return new m2.idm.tp3.parser.antlr.internal.InternalUIMMParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "PollSystem";
	}
	
	public UIMMGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(UIMMGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
