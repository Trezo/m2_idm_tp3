package m2.idm.tp3.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import m2.idm.tp3.services.UIMMGrammarAccess;
import m2.idm.tp3.uIMM.Option;
import m2.idm.tp3.uIMM.Poll;
import m2.idm.tp3.uIMM.PollSystem;
import m2.idm.tp3.uIMM.Question;
import m2.idm.tp3.uIMM.UIMMPackage;
import m2.idm.tp3.uIMM.WidgetOptType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;

@SuppressWarnings("all")
public class UIMMSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private UIMMGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == UIMMPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case UIMMPackage.OPTION:
				if(context == grammarAccess.getOptionRule()) {
					sequence_Option(context, (Option) semanticObject); 
					return; 
				}
				else break;
			case UIMMPackage.POLL:
				if(context == grammarAccess.getPollRule()) {
					sequence_Poll(context, (Poll) semanticObject); 
					return; 
				}
				else break;
			case UIMMPackage.POLL_SYSTEM:
				if(context == grammarAccess.getPollSystemRule()) {
					sequence_PollSystem(context, (PollSystem) semanticObject); 
					return; 
				}
				else break;
			case UIMMPackage.QUESTION:
				if(context == grammarAccess.getQuestionRule()) {
					sequence_Question(context, (Question) semanticObject); 
					return; 
				}
				else break;
			case UIMMPackage.WIDGET_OPT_TYPE:
				if(context == grammarAccess.getWidgetOptTypeRule()) {
					sequence_WidgetOptType(context, (WidgetOptType) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID? text=STRING widget=WidgetOptType?)
	 */
	protected void sequence_Option(EObject context, Option semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     polls+=Poll*
	 */
	protected void sequence_PollSystem(EObject context, PollSystem semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? questions+=Question+)
	 */
	protected void sequence_Poll(EObject context, Poll semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? text=STRING widget=WidgetQuestType? options+=Option+)
	 */
	protected void sequence_Question(EObject context, Question semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((type=Image | type=Input) param=STRING)
	 */
	protected void sequence_WidgetOptType(EObject context, WidgetOptType semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
