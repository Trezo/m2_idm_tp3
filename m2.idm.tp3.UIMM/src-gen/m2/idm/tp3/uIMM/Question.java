/**
 */
package m2.idm.tp3.uIMM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Question</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIMM.Question#getName <em>Name</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.Question#getText <em>Text</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.Question#getWidget <em>Widget</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.Question#getOptions <em>Options</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIMM.UIMMPackage#getQuestion()
 * @model
 * @generated
 */
public interface Question extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getQuestion_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Question#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text</em>' attribute.
   * @see #setText(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getQuestion_Text()
   * @model
   * @generated
   */
  String getText();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Question#getText <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text</em>' attribute.
   * @see #getText()
   * @generated
   */
  void setText(String value);

  /**
   * Returns the value of the '<em><b>Widget</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Widget</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Widget</em>' attribute.
   * @see #setWidget(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getQuestion_Widget()
   * @model
   * @generated
   */
  String getWidget();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Question#getWidget <em>Widget</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Widget</em>' attribute.
   * @see #getWidget()
   * @generated
   */
  void setWidget(String value);

  /**
   * Returns the value of the '<em><b>Options</b></em>' containment reference list.
   * The list contents are of type {@link m2.idm.tp3.uIMM.Option}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Options</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Options</em>' containment reference list.
   * @see m2.idm.tp3.uIMM.UIMMPackage#getQuestion_Options()
   * @model containment="true"
   * @generated
   */
  EList<Option> getOptions();

} // Question
