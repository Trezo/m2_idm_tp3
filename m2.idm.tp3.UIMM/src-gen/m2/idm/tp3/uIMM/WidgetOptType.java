/**
 */
package m2.idm.tp3.uIMM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Widget Opt Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIMM.WidgetOptType#getType <em>Type</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.WidgetOptType#getParam <em>Param</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIMM.UIMMPackage#getWidgetOptType()
 * @model
 * @generated
 */
public interface WidgetOptType extends EObject
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' attribute.
   * @see #setType(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getWidgetOptType_Type()
   * @model
   * @generated
   */
  String getType();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.WidgetOptType#getType <em>Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' attribute.
   * @see #getType()
   * @generated
   */
  void setType(String value);

  /**
   * Returns the value of the '<em><b>Param</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param</em>' attribute.
   * @see #setParam(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getWidgetOptType_Param()
   * @model
   * @generated
   */
  String getParam();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.WidgetOptType#getParam <em>Param</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param</em>' attribute.
   * @see #getParam()
   * @generated
   */
  void setParam(String value);

} // WidgetOptType
