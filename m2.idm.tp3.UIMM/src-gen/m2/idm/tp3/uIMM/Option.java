/**
 */
package m2.idm.tp3.uIMM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Option</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link m2.idm.tp3.uIMM.Option#getName <em>Name</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.Option#getText <em>Text</em>}</li>
 *   <li>{@link m2.idm.tp3.uIMM.Option#getWidget <em>Widget</em>}</li>
 * </ul>
 * </p>
 *
 * @see m2.idm.tp3.uIMM.UIMMPackage#getOption()
 * @model
 * @generated
 */
public interface Option extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getOption_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Option#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text</em>' attribute.
   * @see #setText(String)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getOption_Text()
   * @model
   * @generated
   */
  String getText();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Option#getText <em>Text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text</em>' attribute.
   * @see #getText()
   * @generated
   */
  void setText(String value);

  /**
   * Returns the value of the '<em><b>Widget</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Widget</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Widget</em>' containment reference.
   * @see #setWidget(WidgetOptType)
   * @see m2.idm.tp3.uIMM.UIMMPackage#getOption_Widget()
   * @model containment="true"
   * @generated
   */
  WidgetOptType getWidget();

  /**
   * Sets the value of the '{@link m2.idm.tp3.uIMM.Option#getWidget <em>Widget</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Widget</em>' containment reference.
   * @see #getWidget()
   * @generated
   */
  void setWidget(WidgetOptType value);

} // Option
