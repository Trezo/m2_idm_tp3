package m2.idm.tp3.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import m2.idm.tp3.services.UIQuestionnaireGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalUIQuestionnaireParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Image'", "'Checkbox'", "'Radio'", "'Select'", "'Input'", "'Questions{'", "'}'", "'Question:Options{'", "'Options{'", "':'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int EOF=-1;
    public static final int RULE_SL_COMMENT=8;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalUIQuestionnaireParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalUIQuestionnaireParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalUIQuestionnaireParser.tokenNames; }
    public String getGrammarFileName() { return "../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g"; }


     
     	private UIQuestionnaireGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(UIQuestionnaireGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleUIQuestionnaire"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:60:1: entryRuleUIQuestionnaire : ruleUIQuestionnaire EOF ;
    public final void entryRuleUIQuestionnaire() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:61:1: ( ruleUIQuestionnaire EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:62:1: ruleUIQuestionnaire EOF
            {
             before(grammarAccess.getUIQuestionnaireRule()); 
            pushFollow(FOLLOW_ruleUIQuestionnaire_in_entryRuleUIQuestionnaire61);
            ruleUIQuestionnaire();

            state._fsp--;

             after(grammarAccess.getUIQuestionnaireRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUIQuestionnaire68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUIQuestionnaire"


    // $ANTLR start "ruleUIQuestionnaire"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:69:1: ruleUIQuestionnaire : ( ( rule__UIQuestionnaire__Group__0 ) ) ;
    public final void ruleUIQuestionnaire() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:73:2: ( ( ( rule__UIQuestionnaire__Group__0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:74:1: ( ( rule__UIQuestionnaire__Group__0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:74:1: ( ( rule__UIQuestionnaire__Group__0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:75:1: ( rule__UIQuestionnaire__Group__0 )
            {
             before(grammarAccess.getUIQuestionnaireAccess().getGroup()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:76:1: ( rule__UIQuestionnaire__Group__0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:76:2: rule__UIQuestionnaire__Group__0
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__0_in_ruleUIQuestionnaire94);
            rule__UIQuestionnaire__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUIQuestionnaireAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUIQuestionnaire"


    // $ANTLR start "entryRuleWidgetQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:88:1: entryRuleWidgetQuest : ruleWidgetQuest EOF ;
    public final void entryRuleWidgetQuest() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:89:1: ( ruleWidgetQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:90:1: ruleWidgetQuest EOF
            {
             before(grammarAccess.getWidgetQuestRule()); 
            pushFollow(FOLLOW_ruleWidgetQuest_in_entryRuleWidgetQuest121);
            ruleWidgetQuest();

            state._fsp--;

             after(grammarAccess.getWidgetQuestRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuest128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetQuest"


    // $ANTLR start "ruleWidgetQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:97:1: ruleWidgetQuest : ( ( rule__WidgetQuest__Group__0 ) ) ;
    public final void ruleWidgetQuest() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:101:2: ( ( ( rule__WidgetQuest__Group__0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:102:1: ( ( rule__WidgetQuest__Group__0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:102:1: ( ( rule__WidgetQuest__Group__0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:103:1: ( rule__WidgetQuest__Group__0 )
            {
             before(grammarAccess.getWidgetQuestAccess().getGroup()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:104:1: ( rule__WidgetQuest__Group__0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:104:2: rule__WidgetQuest__Group__0
            {
            pushFollow(FOLLOW_rule__WidgetQuest__Group__0_in_ruleWidgetQuest154);
            rule__WidgetQuest__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetQuestAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetQuest"


    // $ANTLR start "entryRuleWidgetOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:116:1: entryRuleWidgetOptQuest : ruleWidgetOptQuest EOF ;
    public final void entryRuleWidgetOptQuest() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:117:1: ( ruleWidgetOptQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:118:1: ruleWidgetOptQuest EOF
            {
             before(grammarAccess.getWidgetOptQuestRule()); 
            pushFollow(FOLLOW_ruleWidgetOptQuest_in_entryRuleWidgetOptQuest181);
            ruleWidgetOptQuest();

            state._fsp--;

             after(grammarAccess.getWidgetOptQuestRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptQuest188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetOptQuest"


    // $ANTLR start "ruleWidgetOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:125:1: ruleWidgetOptQuest : ( ( rule__WidgetOptQuest__Group__0 ) ) ;
    public final void ruleWidgetOptQuest() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:129:2: ( ( ( rule__WidgetOptQuest__Group__0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:130:1: ( ( rule__WidgetOptQuest__Group__0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:130:1: ( ( rule__WidgetOptQuest__Group__0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:131:1: ( rule__WidgetOptQuest__Group__0 )
            {
             before(grammarAccess.getWidgetOptQuestAccess().getGroup()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:132:1: ( rule__WidgetOptQuest__Group__0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:132:2: rule__WidgetOptQuest__Group__0
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__0_in_ruleWidgetOptQuest214);
            rule__WidgetOptQuest__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptQuestAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetOptQuest"


    // $ANTLR start "entryRuleOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:144:1: entryRuleOptQuest : ruleOptQuest EOF ;
    public final void entryRuleOptQuest() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:145:1: ( ruleOptQuest EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:146:1: ruleOptQuest EOF
            {
             before(grammarAccess.getOptQuestRule()); 
            pushFollow(FOLLOW_ruleOptQuest_in_entryRuleOptQuest241);
            ruleOptQuest();

            state._fsp--;

             after(grammarAccess.getOptQuestRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOptQuest248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptQuest"


    // $ANTLR start "ruleOptQuest"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:153:1: ruleOptQuest : ( ( rule__OptQuest__Group__0 ) ) ;
    public final void ruleOptQuest() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:157:2: ( ( ( rule__OptQuest__Group__0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:158:1: ( ( rule__OptQuest__Group__0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:158:1: ( ( rule__OptQuest__Group__0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:159:1: ( rule__OptQuest__Group__0 )
            {
             before(grammarAccess.getOptQuestAccess().getGroup()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:160:1: ( rule__OptQuest__Group__0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:160:2: rule__OptQuest__Group__0
            {
            pushFollow(FOLLOW_rule__OptQuest__Group__0_in_ruleOptQuest274);
            rule__OptQuest__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptQuestAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptQuest"


    // $ANTLR start "entryRuleQuestion"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:172:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:173:1: ( ruleQuestion EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:174:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion301);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:181:1: ruleQuestion : ( ( rule__Question__NameAssignment ) ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:185:2: ( ( ( rule__Question__NameAssignment ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:186:1: ( ( rule__Question__NameAssignment ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:186:1: ( ( rule__Question__NameAssignment ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:187:1: ( rule__Question__NameAssignment )
            {
             before(grammarAccess.getQuestionAccess().getNameAssignment()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:188:1: ( rule__Question__NameAssignment )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:188:2: rule__Question__NameAssignment
            {
            pushFollow(FOLLOW_rule__Question__NameAssignment_in_ruleQuestion334);
            rule__Question__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:200:1: entryRuleOption : ruleOption EOF ;
    public final void entryRuleOption() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:201:1: ( ruleOption EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:202:1: ruleOption EOF
            {
             before(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption361);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getOptionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:209:1: ruleOption : ( ( rule__Option__NameAssignment ) ) ;
    public final void ruleOption() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:213:2: ( ( ( rule__Option__NameAssignment ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:214:1: ( ( rule__Option__NameAssignment ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:214:1: ( ( rule__Option__NameAssignment ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:215:1: ( rule__Option__NameAssignment )
            {
             before(grammarAccess.getOptionAccess().getNameAssignment()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:216:1: ( rule__Option__NameAssignment )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:216:2: rule__Option__NameAssignment
            {
            pushFollow(FOLLOW_rule__Option__NameAssignment_in_ruleOption394);
            rule__Option__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOption"


    // $ANTLR start "entryRuleWidgetQuestType"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:228:1: entryRuleWidgetQuestType : ruleWidgetQuestType EOF ;
    public final void entryRuleWidgetQuestType() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:229:1: ( ruleWidgetQuestType EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:230:1: ruleWidgetQuestType EOF
            {
             before(grammarAccess.getWidgetQuestTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType421);
            ruleWidgetQuestType();

            state._fsp--;

             after(grammarAccess.getWidgetQuestTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetQuestType428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetQuestType"


    // $ANTLR start "ruleWidgetQuestType"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:237:1: ruleWidgetQuestType : ( ( rule__WidgetQuestType__Alternatives ) ) ;
    public final void ruleWidgetQuestType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:241:2: ( ( ( rule__WidgetQuestType__Alternatives ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:242:1: ( ( rule__WidgetQuestType__Alternatives ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:242:1: ( ( rule__WidgetQuestType__Alternatives ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:243:1: ( rule__WidgetQuestType__Alternatives )
            {
             before(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:244:1: ( rule__WidgetQuestType__Alternatives )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:244:2: rule__WidgetQuestType__Alternatives
            {
            pushFollow(FOLLOW_rule__WidgetQuestType__Alternatives_in_ruleWidgetQuestType454);
            rule__WidgetQuestType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWidgetQuestTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetQuestType"


    // $ANTLR start "entryRuleWidgetOptType"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:256:1: entryRuleWidgetOptType : ruleWidgetOptType EOF ;
    public final void entryRuleWidgetOptType() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:257:1: ( ruleWidgetOptType EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:258:1: ruleWidgetOptType EOF
            {
             before(grammarAccess.getWidgetOptTypeRule()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType481);
            ruleWidgetOptType();

            state._fsp--;

             after(grammarAccess.getWidgetOptTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidgetOptType488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidgetOptType"


    // $ANTLR start "ruleWidgetOptType"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:265:1: ruleWidgetOptType : ( ( rule__WidgetOptType__Group__0 ) ) ;
    public final void ruleWidgetOptType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:269:2: ( ( ( rule__WidgetOptType__Group__0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:270:1: ( ( rule__WidgetOptType__Group__0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:270:1: ( ( rule__WidgetOptType__Group__0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:271:1: ( rule__WidgetOptType__Group__0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getGroup()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:272:1: ( rule__WidgetOptType__Group__0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:272:2: rule__WidgetOptType__Group__0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__0_in_ruleWidgetOptType514);
            rule__WidgetOptType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidgetOptType"


    // $ANTLR start "entryRuleImage"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:284:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:285:1: ( ruleImage EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:286:1: ruleImage EOF
            {
             before(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage541);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getImageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:293:1: ruleImage : ( 'Image' ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:297:2: ( ( 'Image' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:298:1: ( 'Image' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:298:1: ( 'Image' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:299:1: 'Image'
            {
             before(grammarAccess.getImageAccess().getImageKeyword()); 
            match(input,11,FOLLOW_11_in_ruleImage575); 
             after(grammarAccess.getImageAccess().getImageKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckbox"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:314:1: entryRuleCheckbox : ruleCheckbox EOF ;
    public final void entryRuleCheckbox() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:315:1: ( ruleCheckbox EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:316:1: ruleCheckbox EOF
            {
             before(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox603);
            ruleCheckbox();

            state._fsp--;

             after(grammarAccess.getCheckboxRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox610); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:323:1: ruleCheckbox : ( 'Checkbox' ) ;
    public final void ruleCheckbox() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:327:2: ( ( 'Checkbox' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:328:1: ( 'Checkbox' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:328:1: ( 'Checkbox' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:329:1: 'Checkbox'
            {
             before(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 
            match(input,12,FOLLOW_12_in_ruleCheckbox637); 
             after(grammarAccess.getCheckboxAccess().getCheckboxKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadio"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:344:1: entryRuleRadio : ruleRadio EOF ;
    public final void entryRuleRadio() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:345:1: ( ruleRadio EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:346:1: ruleRadio EOF
            {
             before(grammarAccess.getRadioRule()); 
            pushFollow(FOLLOW_ruleRadio_in_entryRuleRadio665);
            ruleRadio();

            state._fsp--;

             after(grammarAccess.getRadioRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadio672); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRadio"


    // $ANTLR start "ruleRadio"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:353:1: ruleRadio : ( 'Radio' ) ;
    public final void ruleRadio() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:357:2: ( ( 'Radio' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:358:1: ( 'Radio' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:358:1: ( 'Radio' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:359:1: 'Radio'
            {
             before(grammarAccess.getRadioAccess().getRadioKeyword()); 
            match(input,13,FOLLOW_13_in_ruleRadio699); 
             after(grammarAccess.getRadioAccess().getRadioKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRadio"


    // $ANTLR start "entryRuleSelect"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:374:1: entryRuleSelect : ruleSelect EOF ;
    public final void entryRuleSelect() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:375:1: ( ruleSelect EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:376:1: ruleSelect EOF
            {
             before(grammarAccess.getSelectRule()); 
            pushFollow(FOLLOW_ruleSelect_in_entryRuleSelect727);
            ruleSelect();

            state._fsp--;

             after(grammarAccess.getSelectRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleSelect734); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelect"


    // $ANTLR start "ruleSelect"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:383:1: ruleSelect : ( 'Select' ) ;
    public final void ruleSelect() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:387:2: ( ( 'Select' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:388:1: ( 'Select' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:388:1: ( 'Select' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:389:1: 'Select'
            {
             before(grammarAccess.getSelectAccess().getSelectKeyword()); 
            match(input,14,FOLLOW_14_in_ruleSelect761); 
             after(grammarAccess.getSelectAccess().getSelectKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelect"


    // $ANTLR start "entryRuleInput"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:404:1: entryRuleInput : ruleInput EOF ;
    public final void entryRuleInput() throws RecognitionException {
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:405:1: ( ruleInput EOF )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:406:1: ruleInput EOF
            {
             before(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput789);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getInputRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput796); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:413:1: ruleInput : ( 'Input' ) ;
    public final void ruleInput() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:417:2: ( ( 'Input' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:418:1: ( 'Input' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:418:1: ( 'Input' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:419:1: 'Input'
            {
             before(grammarAccess.getInputAccess().getInputKeyword()); 
            match(input,15,FOLLOW_15_in_ruleInput823); 
             after(grammarAccess.getInputAccess().getInputKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "rule__WidgetQuestType__Alternatives"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:434:1: rule__WidgetQuestType__Alternatives : ( ( ruleCheckbox ) | ( ruleRadio ) | ( ruleSelect ) );
    public final void rule__WidgetQuestType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:438:1: ( ( ruleCheckbox ) | ( ruleRadio ) | ( ruleSelect ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt1=1;
                }
                break;
            case 13:
                {
                alt1=2;
                }
                break;
            case 14:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:439:1: ( ruleCheckbox )
                    {
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:439:1: ( ruleCheckbox )
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:440:1: ruleCheckbox
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleCheckbox_in_rule__WidgetQuestType__Alternatives860);
                    ruleCheckbox();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getCheckboxParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:445:6: ( ruleRadio )
                    {
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:445:6: ( ruleRadio )
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:446:1: ruleRadio
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleRadio_in_rule__WidgetQuestType__Alternatives877);
                    ruleRadio();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getRadioParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:451:6: ( ruleSelect )
                    {
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:451:6: ( ruleSelect )
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:452:1: ruleSelect
                    {
                     before(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleSelect_in_rule__WidgetQuestType__Alternatives894);
                    ruleSelect();

                    state._fsp--;

                     after(grammarAccess.getWidgetQuestTypeAccess().getSelectParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuestType__Alternatives"


    // $ANTLR start "rule__WidgetOptType__TypeAlternatives_0_0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:462:1: rule__WidgetOptType__TypeAlternatives_0_0 : ( ( ruleImage ) | ( ruleInput ) );
    public final void rule__WidgetOptType__TypeAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:466:1: ( ( ruleImage ) | ( ruleInput ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==15) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:467:1: ( ruleImage )
                    {
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:467:1: ( ruleImage )
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:468:1: ruleImage
                    {
                     before(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 
                    pushFollow(FOLLOW_ruleImage_in_rule__WidgetOptType__TypeAlternatives_0_0926);
                    ruleImage();

                    state._fsp--;

                     after(grammarAccess.getWidgetOptTypeAccess().getTypeImageParserRuleCall_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:473:6: ( ruleInput )
                    {
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:473:6: ( ruleInput )
                    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:474:1: ruleInput
                    {
                     before(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 
                    pushFollow(FOLLOW_ruleInput_in_rule__WidgetOptType__TypeAlternatives_0_0943);
                    ruleInput();

                    state._fsp--;

                     after(grammarAccess.getWidgetOptTypeAccess().getTypeInputParserRuleCall_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__TypeAlternatives_0_0"


    // $ANTLR start "rule__UIQuestionnaire__Group__0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:486:1: rule__UIQuestionnaire__Group__0 : rule__UIQuestionnaire__Group__0__Impl rule__UIQuestionnaire__Group__1 ;
    public final void rule__UIQuestionnaire__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:490:1: ( rule__UIQuestionnaire__Group__0__Impl rule__UIQuestionnaire__Group__1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:491:2: rule__UIQuestionnaire__Group__0__Impl rule__UIQuestionnaire__Group__1
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__0__Impl_in_rule__UIQuestionnaire__Group__0973);
            rule__UIQuestionnaire__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__1_in_rule__UIQuestionnaire__Group__0976);
            rule__UIQuestionnaire__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__0"


    // $ANTLR start "rule__UIQuestionnaire__Group__0__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:498:1: rule__UIQuestionnaire__Group__0__Impl : ( 'Questions{' ) ;
    public final void rule__UIQuestionnaire__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:502:1: ( ( 'Questions{' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:503:1: ( 'Questions{' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:503:1: ( 'Questions{' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:504:1: 'Questions{'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getQuestionsKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__UIQuestionnaire__Group__0__Impl1004); 
             after(grammarAccess.getUIQuestionnaireAccess().getQuestionsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__0__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:517:1: rule__UIQuestionnaire__Group__1 : rule__UIQuestionnaire__Group__1__Impl rule__UIQuestionnaire__Group__2 ;
    public final void rule__UIQuestionnaire__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:521:1: ( rule__UIQuestionnaire__Group__1__Impl rule__UIQuestionnaire__Group__2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:522:2: rule__UIQuestionnaire__Group__1__Impl rule__UIQuestionnaire__Group__2
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__1__Impl_in_rule__UIQuestionnaire__Group__11035);
            rule__UIQuestionnaire__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__2_in_rule__UIQuestionnaire__Group__11038);
            rule__UIQuestionnaire__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__1"


    // $ANTLR start "rule__UIQuestionnaire__Group__1__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:529:1: rule__UIQuestionnaire__Group__1__Impl : ( ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )* ) ;
    public final void rule__UIQuestionnaire__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:533:1: ( ( ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )* ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:534:1: ( ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )* )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:534:1: ( ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )* )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:535:1: ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )*
            {
             before(grammarAccess.getUIQuestionnaireAccess().getQuestWidgetsAssignment_1()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:536:1: ( rule__UIQuestionnaire__QuestWidgetsAssignment_1 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:536:2: rule__UIQuestionnaire__QuestWidgetsAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__UIQuestionnaire__QuestWidgetsAssignment_1_in_rule__UIQuestionnaire__Group__1__Impl1065);
            	    rule__UIQuestionnaire__QuestWidgetsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getUIQuestionnaireAccess().getQuestWidgetsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__1__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:546:1: rule__UIQuestionnaire__Group__2 : rule__UIQuestionnaire__Group__2__Impl rule__UIQuestionnaire__Group__3 ;
    public final void rule__UIQuestionnaire__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:550:1: ( rule__UIQuestionnaire__Group__2__Impl rule__UIQuestionnaire__Group__3 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:551:2: rule__UIQuestionnaire__Group__2__Impl rule__UIQuestionnaire__Group__3
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__2__Impl_in_rule__UIQuestionnaire__Group__21096);
            rule__UIQuestionnaire__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__3_in_rule__UIQuestionnaire__Group__21099);
            rule__UIQuestionnaire__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__2"


    // $ANTLR start "rule__UIQuestionnaire__Group__2__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:558:1: rule__UIQuestionnaire__Group__2__Impl : ( '}' ) ;
    public final void rule__UIQuestionnaire__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:562:1: ( ( '}' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:563:1: ( '}' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:563:1: ( '}' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:564:1: '}'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__UIQuestionnaire__Group__2__Impl1127); 
             after(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__2__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__3"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:577:1: rule__UIQuestionnaire__Group__3 : rule__UIQuestionnaire__Group__3__Impl rule__UIQuestionnaire__Group__4 ;
    public final void rule__UIQuestionnaire__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:581:1: ( rule__UIQuestionnaire__Group__3__Impl rule__UIQuestionnaire__Group__4 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:582:2: rule__UIQuestionnaire__Group__3__Impl rule__UIQuestionnaire__Group__4
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__3__Impl_in_rule__UIQuestionnaire__Group__31158);
            rule__UIQuestionnaire__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__4_in_rule__UIQuestionnaire__Group__31161);
            rule__UIQuestionnaire__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__3"


    // $ANTLR start "rule__UIQuestionnaire__Group__3__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:589:1: rule__UIQuestionnaire__Group__3__Impl : ( 'Question:Options{' ) ;
    public final void rule__UIQuestionnaire__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:593:1: ( ( 'Question:Options{' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:594:1: ( 'Question:Options{' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:594:1: ( 'Question:Options{' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:595:1: 'Question:Options{'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getQuestionOptionsKeyword_3()); 
            match(input,18,FOLLOW_18_in_rule__UIQuestionnaire__Group__3__Impl1189); 
             after(grammarAccess.getUIQuestionnaireAccess().getQuestionOptionsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__3__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__4"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:608:1: rule__UIQuestionnaire__Group__4 : rule__UIQuestionnaire__Group__4__Impl rule__UIQuestionnaire__Group__5 ;
    public final void rule__UIQuestionnaire__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:612:1: ( rule__UIQuestionnaire__Group__4__Impl rule__UIQuestionnaire__Group__5 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:613:2: rule__UIQuestionnaire__Group__4__Impl rule__UIQuestionnaire__Group__5
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__4__Impl_in_rule__UIQuestionnaire__Group__41220);
            rule__UIQuestionnaire__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__5_in_rule__UIQuestionnaire__Group__41223);
            rule__UIQuestionnaire__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__4"


    // $ANTLR start "rule__UIQuestionnaire__Group__4__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:620:1: rule__UIQuestionnaire__Group__4__Impl : ( ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )* ) ;
    public final void rule__UIQuestionnaire__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:624:1: ( ( ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )* ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:625:1: ( ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )* )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:625:1: ( ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )* )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:626:1: ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )*
            {
             before(grammarAccess.getUIQuestionnaireAccess().getOptQuestWidgetAssignment_4()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:627:1: ( rule__UIQuestionnaire__OptQuestWidgetAssignment_4 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:627:2: rule__UIQuestionnaire__OptQuestWidgetAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__UIQuestionnaire__OptQuestWidgetAssignment_4_in_rule__UIQuestionnaire__Group__4__Impl1250);
            	    rule__UIQuestionnaire__OptQuestWidgetAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getUIQuestionnaireAccess().getOptQuestWidgetAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__4__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__5"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:637:1: rule__UIQuestionnaire__Group__5 : rule__UIQuestionnaire__Group__5__Impl rule__UIQuestionnaire__Group__6 ;
    public final void rule__UIQuestionnaire__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:641:1: ( rule__UIQuestionnaire__Group__5__Impl rule__UIQuestionnaire__Group__6 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:642:2: rule__UIQuestionnaire__Group__5__Impl rule__UIQuestionnaire__Group__6
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__5__Impl_in_rule__UIQuestionnaire__Group__51281);
            rule__UIQuestionnaire__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__6_in_rule__UIQuestionnaire__Group__51284);
            rule__UIQuestionnaire__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__5"


    // $ANTLR start "rule__UIQuestionnaire__Group__5__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:649:1: rule__UIQuestionnaire__Group__5__Impl : ( '}' ) ;
    public final void rule__UIQuestionnaire__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:653:1: ( ( '}' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:654:1: ( '}' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:654:1: ( '}' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:655:1: '}'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_5()); 
            match(input,17,FOLLOW_17_in_rule__UIQuestionnaire__Group__5__Impl1312); 
             after(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__5__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__6"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:668:1: rule__UIQuestionnaire__Group__6 : rule__UIQuestionnaire__Group__6__Impl rule__UIQuestionnaire__Group__7 ;
    public final void rule__UIQuestionnaire__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:672:1: ( rule__UIQuestionnaire__Group__6__Impl rule__UIQuestionnaire__Group__7 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:673:2: rule__UIQuestionnaire__Group__6__Impl rule__UIQuestionnaire__Group__7
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__6__Impl_in_rule__UIQuestionnaire__Group__61343);
            rule__UIQuestionnaire__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__7_in_rule__UIQuestionnaire__Group__61346);
            rule__UIQuestionnaire__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__6"


    // $ANTLR start "rule__UIQuestionnaire__Group__6__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:680:1: rule__UIQuestionnaire__Group__6__Impl : ( 'Options{' ) ;
    public final void rule__UIQuestionnaire__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:684:1: ( ( 'Options{' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:685:1: ( 'Options{' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:685:1: ( 'Options{' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:686:1: 'Options{'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getOptionsKeyword_6()); 
            match(input,19,FOLLOW_19_in_rule__UIQuestionnaire__Group__6__Impl1374); 
             after(grammarAccess.getUIQuestionnaireAccess().getOptionsKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__6__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__7"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:699:1: rule__UIQuestionnaire__Group__7 : rule__UIQuestionnaire__Group__7__Impl rule__UIQuestionnaire__Group__8 ;
    public final void rule__UIQuestionnaire__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:703:1: ( rule__UIQuestionnaire__Group__7__Impl rule__UIQuestionnaire__Group__8 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:704:2: rule__UIQuestionnaire__Group__7__Impl rule__UIQuestionnaire__Group__8
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__7__Impl_in_rule__UIQuestionnaire__Group__71405);
            rule__UIQuestionnaire__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__8_in_rule__UIQuestionnaire__Group__71408);
            rule__UIQuestionnaire__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__7"


    // $ANTLR start "rule__UIQuestionnaire__Group__7__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:711:1: rule__UIQuestionnaire__Group__7__Impl : ( ( rule__UIQuestionnaire__OptWidgetAssignment_7 )* ) ;
    public final void rule__UIQuestionnaire__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:715:1: ( ( ( rule__UIQuestionnaire__OptWidgetAssignment_7 )* ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:716:1: ( ( rule__UIQuestionnaire__OptWidgetAssignment_7 )* )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:716:1: ( ( rule__UIQuestionnaire__OptWidgetAssignment_7 )* )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:717:1: ( rule__UIQuestionnaire__OptWidgetAssignment_7 )*
            {
             before(grammarAccess.getUIQuestionnaireAccess().getOptWidgetAssignment_7()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:718:1: ( rule__UIQuestionnaire__OptWidgetAssignment_7 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:718:2: rule__UIQuestionnaire__OptWidgetAssignment_7
            	    {
            	    pushFollow(FOLLOW_rule__UIQuestionnaire__OptWidgetAssignment_7_in_rule__UIQuestionnaire__Group__7__Impl1435);
            	    rule__UIQuestionnaire__OptWidgetAssignment_7();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getUIQuestionnaireAccess().getOptWidgetAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__7__Impl"


    // $ANTLR start "rule__UIQuestionnaire__Group__8"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:728:1: rule__UIQuestionnaire__Group__8 : rule__UIQuestionnaire__Group__8__Impl ;
    public final void rule__UIQuestionnaire__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:732:1: ( rule__UIQuestionnaire__Group__8__Impl )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:733:2: rule__UIQuestionnaire__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__UIQuestionnaire__Group__8__Impl_in_rule__UIQuestionnaire__Group__81466);
            rule__UIQuestionnaire__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__8"


    // $ANTLR start "rule__UIQuestionnaire__Group__8__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:739:1: rule__UIQuestionnaire__Group__8__Impl : ( '}' ) ;
    public final void rule__UIQuestionnaire__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:743:1: ( ( '}' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:744:1: ( '}' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:744:1: ( '}' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:745:1: '}'
            {
             before(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_8()); 
            match(input,17,FOLLOW_17_in_rule__UIQuestionnaire__Group__8__Impl1494); 
             after(grammarAccess.getUIQuestionnaireAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__Group__8__Impl"


    // $ANTLR start "rule__WidgetQuest__Group__0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:776:1: rule__WidgetQuest__Group__0 : rule__WidgetQuest__Group__0__Impl rule__WidgetQuest__Group__1 ;
    public final void rule__WidgetQuest__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:780:1: ( rule__WidgetQuest__Group__0__Impl rule__WidgetQuest__Group__1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:781:2: rule__WidgetQuest__Group__0__Impl rule__WidgetQuest__Group__1
            {
            pushFollow(FOLLOW_rule__WidgetQuest__Group__0__Impl_in_rule__WidgetQuest__Group__01543);
            rule__WidgetQuest__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetQuest__Group__1_in_rule__WidgetQuest__Group__01546);
            rule__WidgetQuest__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__0"


    // $ANTLR start "rule__WidgetQuest__Group__0__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:788:1: rule__WidgetQuest__Group__0__Impl : ( ( rule__WidgetQuest__QuestionAssignment_0 ) ) ;
    public final void rule__WidgetQuest__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:792:1: ( ( ( rule__WidgetQuest__QuestionAssignment_0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:793:1: ( ( rule__WidgetQuest__QuestionAssignment_0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:793:1: ( ( rule__WidgetQuest__QuestionAssignment_0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:794:1: ( rule__WidgetQuest__QuestionAssignment_0 )
            {
             before(grammarAccess.getWidgetQuestAccess().getQuestionAssignment_0()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:795:1: ( rule__WidgetQuest__QuestionAssignment_0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:795:2: rule__WidgetQuest__QuestionAssignment_0
            {
            pushFollow(FOLLOW_rule__WidgetQuest__QuestionAssignment_0_in_rule__WidgetQuest__Group__0__Impl1573);
            rule__WidgetQuest__QuestionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetQuestAccess().getQuestionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__0__Impl"


    // $ANTLR start "rule__WidgetQuest__Group__1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:805:1: rule__WidgetQuest__Group__1 : rule__WidgetQuest__Group__1__Impl rule__WidgetQuest__Group__2 ;
    public final void rule__WidgetQuest__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:809:1: ( rule__WidgetQuest__Group__1__Impl rule__WidgetQuest__Group__2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:810:2: rule__WidgetQuest__Group__1__Impl rule__WidgetQuest__Group__2
            {
            pushFollow(FOLLOW_rule__WidgetQuest__Group__1__Impl_in_rule__WidgetQuest__Group__11603);
            rule__WidgetQuest__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetQuest__Group__2_in_rule__WidgetQuest__Group__11606);
            rule__WidgetQuest__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__1"


    // $ANTLR start "rule__WidgetQuest__Group__1__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:817:1: rule__WidgetQuest__Group__1__Impl : ( ':' ) ;
    public final void rule__WidgetQuest__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:821:1: ( ( ':' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:822:1: ( ':' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:822:1: ( ':' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:823:1: ':'
            {
             before(grammarAccess.getWidgetQuestAccess().getColonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__WidgetQuest__Group__1__Impl1634); 
             after(grammarAccess.getWidgetQuestAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__1__Impl"


    // $ANTLR start "rule__WidgetQuest__Group__2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:836:1: rule__WidgetQuest__Group__2 : rule__WidgetQuest__Group__2__Impl ;
    public final void rule__WidgetQuest__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:840:1: ( rule__WidgetQuest__Group__2__Impl )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:841:2: rule__WidgetQuest__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__WidgetQuest__Group__2__Impl_in_rule__WidgetQuest__Group__21665);
            rule__WidgetQuest__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__2"


    // $ANTLR start "rule__WidgetQuest__Group__2__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:847:1: rule__WidgetQuest__Group__2__Impl : ( ( rule__WidgetQuest__TypeAssignment_2 ) ) ;
    public final void rule__WidgetQuest__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:851:1: ( ( ( rule__WidgetQuest__TypeAssignment_2 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:852:1: ( ( rule__WidgetQuest__TypeAssignment_2 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:852:1: ( ( rule__WidgetQuest__TypeAssignment_2 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:853:1: ( rule__WidgetQuest__TypeAssignment_2 )
            {
             before(grammarAccess.getWidgetQuestAccess().getTypeAssignment_2()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:854:1: ( rule__WidgetQuest__TypeAssignment_2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:854:2: rule__WidgetQuest__TypeAssignment_2
            {
            pushFollow(FOLLOW_rule__WidgetQuest__TypeAssignment_2_in_rule__WidgetQuest__Group__2__Impl1692);
            rule__WidgetQuest__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWidgetQuestAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__Group__2__Impl"


    // $ANTLR start "rule__WidgetOptQuest__Group__0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:870:1: rule__WidgetOptQuest__Group__0 : rule__WidgetOptQuest__Group__0__Impl rule__WidgetOptQuest__Group__1 ;
    public final void rule__WidgetOptQuest__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:874:1: ( rule__WidgetOptQuest__Group__0__Impl rule__WidgetOptQuest__Group__1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:875:2: rule__WidgetOptQuest__Group__0__Impl rule__WidgetOptQuest__Group__1
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__0__Impl_in_rule__WidgetOptQuest__Group__01728);
            rule__WidgetOptQuest__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__1_in_rule__WidgetOptQuest__Group__01731);
            rule__WidgetOptQuest__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__0"


    // $ANTLR start "rule__WidgetOptQuest__Group__0__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:882:1: rule__WidgetOptQuest__Group__0__Impl : ( ( rule__WidgetOptQuest__QuestionAssignment_0 ) ) ;
    public final void rule__WidgetOptQuest__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:886:1: ( ( ( rule__WidgetOptQuest__QuestionAssignment_0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:887:1: ( ( rule__WidgetOptQuest__QuestionAssignment_0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:887:1: ( ( rule__WidgetOptQuest__QuestionAssignment_0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:888:1: ( rule__WidgetOptQuest__QuestionAssignment_0 )
            {
             before(grammarAccess.getWidgetOptQuestAccess().getQuestionAssignment_0()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:889:1: ( rule__WidgetOptQuest__QuestionAssignment_0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:889:2: rule__WidgetOptQuest__QuestionAssignment_0
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__QuestionAssignment_0_in_rule__WidgetOptQuest__Group__0__Impl1758);
            rule__WidgetOptQuest__QuestionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptQuestAccess().getQuestionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__0__Impl"


    // $ANTLR start "rule__WidgetOptQuest__Group__1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:899:1: rule__WidgetOptQuest__Group__1 : rule__WidgetOptQuest__Group__1__Impl rule__WidgetOptQuest__Group__2 ;
    public final void rule__WidgetOptQuest__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:903:1: ( rule__WidgetOptQuest__Group__1__Impl rule__WidgetOptQuest__Group__2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:904:2: rule__WidgetOptQuest__Group__1__Impl rule__WidgetOptQuest__Group__2
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__1__Impl_in_rule__WidgetOptQuest__Group__11788);
            rule__WidgetOptQuest__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__2_in_rule__WidgetOptQuest__Group__11791);
            rule__WidgetOptQuest__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__1"


    // $ANTLR start "rule__WidgetOptQuest__Group__1__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:911:1: rule__WidgetOptQuest__Group__1__Impl : ( '=>' ) ;
    public final void rule__WidgetOptQuest__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:915:1: ( ( '=>' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:916:1: ( '=>' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:916:1: ( '=>' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:917:1: '=>'
            {
             before(grammarAccess.getWidgetOptQuestAccess().getEqualsSignGreaterThanSignKeyword_1()); 
            match(input,21,FOLLOW_21_in_rule__WidgetOptQuest__Group__1__Impl1819); 
             after(grammarAccess.getWidgetOptQuestAccess().getEqualsSignGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__1__Impl"


    // $ANTLR start "rule__WidgetOptQuest__Group__2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:930:1: rule__WidgetOptQuest__Group__2 : rule__WidgetOptQuest__Group__2__Impl rule__WidgetOptQuest__Group__3 ;
    public final void rule__WidgetOptQuest__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:934:1: ( rule__WidgetOptQuest__Group__2__Impl rule__WidgetOptQuest__Group__3 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:935:2: rule__WidgetOptQuest__Group__2__Impl rule__WidgetOptQuest__Group__3
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__2__Impl_in_rule__WidgetOptQuest__Group__21850);
            rule__WidgetOptQuest__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__3_in_rule__WidgetOptQuest__Group__21853);
            rule__WidgetOptQuest__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__2"


    // $ANTLR start "rule__WidgetOptQuest__Group__2__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:942:1: rule__WidgetOptQuest__Group__2__Impl : ( ( rule__WidgetOptQuest__OptionAssignment_2 ) ) ;
    public final void rule__WidgetOptQuest__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:946:1: ( ( ( rule__WidgetOptQuest__OptionAssignment_2 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:947:1: ( ( rule__WidgetOptQuest__OptionAssignment_2 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:947:1: ( ( rule__WidgetOptQuest__OptionAssignment_2 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:948:1: ( rule__WidgetOptQuest__OptionAssignment_2 )
            {
             before(grammarAccess.getWidgetOptQuestAccess().getOptionAssignment_2()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:949:1: ( rule__WidgetOptQuest__OptionAssignment_2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:949:2: rule__WidgetOptQuest__OptionAssignment_2
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__OptionAssignment_2_in_rule__WidgetOptQuest__Group__2__Impl1880);
            rule__WidgetOptQuest__OptionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptQuestAccess().getOptionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__2__Impl"


    // $ANTLR start "rule__WidgetOptQuest__Group__3"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:959:1: rule__WidgetOptQuest__Group__3 : rule__WidgetOptQuest__Group__3__Impl rule__WidgetOptQuest__Group__4 ;
    public final void rule__WidgetOptQuest__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:963:1: ( rule__WidgetOptQuest__Group__3__Impl rule__WidgetOptQuest__Group__4 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:964:2: rule__WidgetOptQuest__Group__3__Impl rule__WidgetOptQuest__Group__4
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__3__Impl_in_rule__WidgetOptQuest__Group__31910);
            rule__WidgetOptQuest__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__4_in_rule__WidgetOptQuest__Group__31913);
            rule__WidgetOptQuest__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__3"


    // $ANTLR start "rule__WidgetOptQuest__Group__3__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:971:1: rule__WidgetOptQuest__Group__3__Impl : ( ':' ) ;
    public final void rule__WidgetOptQuest__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:975:1: ( ( ':' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:976:1: ( ':' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:976:1: ( ':' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:977:1: ':'
            {
             before(grammarAccess.getWidgetOptQuestAccess().getColonKeyword_3()); 
            match(input,20,FOLLOW_20_in_rule__WidgetOptQuest__Group__3__Impl1941); 
             after(grammarAccess.getWidgetOptQuestAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__3__Impl"


    // $ANTLR start "rule__WidgetOptQuest__Group__4"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:990:1: rule__WidgetOptQuest__Group__4 : rule__WidgetOptQuest__Group__4__Impl ;
    public final void rule__WidgetOptQuest__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:994:1: ( rule__WidgetOptQuest__Group__4__Impl )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:995:2: rule__WidgetOptQuest__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__Group__4__Impl_in_rule__WidgetOptQuest__Group__41972);
            rule__WidgetOptQuest__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__4"


    // $ANTLR start "rule__WidgetOptQuest__Group__4__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1001:1: rule__WidgetOptQuest__Group__4__Impl : ( ( rule__WidgetOptQuest__TypeAssignment_4 ) ) ;
    public final void rule__WidgetOptQuest__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1005:1: ( ( ( rule__WidgetOptQuest__TypeAssignment_4 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1006:1: ( ( rule__WidgetOptQuest__TypeAssignment_4 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1006:1: ( ( rule__WidgetOptQuest__TypeAssignment_4 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1007:1: ( rule__WidgetOptQuest__TypeAssignment_4 )
            {
             before(grammarAccess.getWidgetOptQuestAccess().getTypeAssignment_4()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1008:1: ( rule__WidgetOptQuest__TypeAssignment_4 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1008:2: rule__WidgetOptQuest__TypeAssignment_4
            {
            pushFollow(FOLLOW_rule__WidgetOptQuest__TypeAssignment_4_in_rule__WidgetOptQuest__Group__4__Impl1999);
            rule__WidgetOptQuest__TypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptQuestAccess().getTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__Group__4__Impl"


    // $ANTLR start "rule__OptQuest__Group__0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1028:1: rule__OptQuest__Group__0 : rule__OptQuest__Group__0__Impl rule__OptQuest__Group__1 ;
    public final void rule__OptQuest__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1032:1: ( rule__OptQuest__Group__0__Impl rule__OptQuest__Group__1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1033:2: rule__OptQuest__Group__0__Impl rule__OptQuest__Group__1
            {
            pushFollow(FOLLOW_rule__OptQuest__Group__0__Impl_in_rule__OptQuest__Group__02039);
            rule__OptQuest__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OptQuest__Group__1_in_rule__OptQuest__Group__02042);
            rule__OptQuest__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__0"


    // $ANTLR start "rule__OptQuest__Group__0__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1040:1: rule__OptQuest__Group__0__Impl : ( ( rule__OptQuest__OptionAssignment_0 ) ) ;
    public final void rule__OptQuest__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1044:1: ( ( ( rule__OptQuest__OptionAssignment_0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1045:1: ( ( rule__OptQuest__OptionAssignment_0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1045:1: ( ( rule__OptQuest__OptionAssignment_0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1046:1: ( rule__OptQuest__OptionAssignment_0 )
            {
             before(grammarAccess.getOptQuestAccess().getOptionAssignment_0()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1047:1: ( rule__OptQuest__OptionAssignment_0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1047:2: rule__OptQuest__OptionAssignment_0
            {
            pushFollow(FOLLOW_rule__OptQuest__OptionAssignment_0_in_rule__OptQuest__Group__0__Impl2069);
            rule__OptQuest__OptionAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOptQuestAccess().getOptionAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__0__Impl"


    // $ANTLR start "rule__OptQuest__Group__1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1057:1: rule__OptQuest__Group__1 : rule__OptQuest__Group__1__Impl rule__OptQuest__Group__2 ;
    public final void rule__OptQuest__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1061:1: ( rule__OptQuest__Group__1__Impl rule__OptQuest__Group__2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1062:2: rule__OptQuest__Group__1__Impl rule__OptQuest__Group__2
            {
            pushFollow(FOLLOW_rule__OptQuest__Group__1__Impl_in_rule__OptQuest__Group__12099);
            rule__OptQuest__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__OptQuest__Group__2_in_rule__OptQuest__Group__12102);
            rule__OptQuest__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__1"


    // $ANTLR start "rule__OptQuest__Group__1__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1069:1: rule__OptQuest__Group__1__Impl : ( ':' ) ;
    public final void rule__OptQuest__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1073:1: ( ( ':' ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1074:1: ( ':' )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1074:1: ( ':' )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1075:1: ':'
            {
             before(grammarAccess.getOptQuestAccess().getColonKeyword_1()); 
            match(input,20,FOLLOW_20_in_rule__OptQuest__Group__1__Impl2130); 
             after(grammarAccess.getOptQuestAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__1__Impl"


    // $ANTLR start "rule__OptQuest__Group__2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1088:1: rule__OptQuest__Group__2 : rule__OptQuest__Group__2__Impl ;
    public final void rule__OptQuest__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1092:1: ( rule__OptQuest__Group__2__Impl )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1093:2: rule__OptQuest__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__OptQuest__Group__2__Impl_in_rule__OptQuest__Group__22161);
            rule__OptQuest__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__2"


    // $ANTLR start "rule__OptQuest__Group__2__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1099:1: rule__OptQuest__Group__2__Impl : ( ( rule__OptQuest__TypeAssignment_2 ) ) ;
    public final void rule__OptQuest__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1103:1: ( ( ( rule__OptQuest__TypeAssignment_2 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1104:1: ( ( rule__OptQuest__TypeAssignment_2 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1104:1: ( ( rule__OptQuest__TypeAssignment_2 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1105:1: ( rule__OptQuest__TypeAssignment_2 )
            {
             before(grammarAccess.getOptQuestAccess().getTypeAssignment_2()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1106:1: ( rule__OptQuest__TypeAssignment_2 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1106:2: rule__OptQuest__TypeAssignment_2
            {
            pushFollow(FOLLOW_rule__OptQuest__TypeAssignment_2_in_rule__OptQuest__Group__2__Impl2188);
            rule__OptQuest__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOptQuestAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__Group__2__Impl"


    // $ANTLR start "rule__WidgetOptType__Group__0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1122:1: rule__WidgetOptType__Group__0 : rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1 ;
    public final void rule__WidgetOptType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1126:1: ( rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1127:2: rule__WidgetOptType__Group__0__Impl rule__WidgetOptType__Group__1
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__0__Impl_in_rule__WidgetOptType__Group__02224);
            rule__WidgetOptType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__WidgetOptType__Group__1_in_rule__WidgetOptType__Group__02227);
            rule__WidgetOptType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__0"


    // $ANTLR start "rule__WidgetOptType__Group__0__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1134:1: rule__WidgetOptType__Group__0__Impl : ( ( rule__WidgetOptType__TypeAssignment_0 ) ) ;
    public final void rule__WidgetOptType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1138:1: ( ( ( rule__WidgetOptType__TypeAssignment_0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1139:1: ( ( rule__WidgetOptType__TypeAssignment_0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1139:1: ( ( rule__WidgetOptType__TypeAssignment_0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1140:1: ( rule__WidgetOptType__TypeAssignment_0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1141:1: ( rule__WidgetOptType__TypeAssignment_0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1141:2: rule__WidgetOptType__TypeAssignment_0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__TypeAssignment_0_in_rule__WidgetOptType__Group__0__Impl2254);
            rule__WidgetOptType__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__0__Impl"


    // $ANTLR start "rule__WidgetOptType__Group__1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1151:1: rule__WidgetOptType__Group__1 : rule__WidgetOptType__Group__1__Impl ;
    public final void rule__WidgetOptType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1155:1: ( rule__WidgetOptType__Group__1__Impl )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1156:2: rule__WidgetOptType__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WidgetOptType__Group__1__Impl_in_rule__WidgetOptType__Group__12284);
            rule__WidgetOptType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__1"


    // $ANTLR start "rule__WidgetOptType__Group__1__Impl"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1162:1: rule__WidgetOptType__Group__1__Impl : ( ( rule__WidgetOptType__ParamAssignment_1 ) ) ;
    public final void rule__WidgetOptType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1166:1: ( ( ( rule__WidgetOptType__ParamAssignment_1 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1167:1: ( ( rule__WidgetOptType__ParamAssignment_1 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1167:1: ( ( rule__WidgetOptType__ParamAssignment_1 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1168:1: ( rule__WidgetOptType__ParamAssignment_1 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1169:1: ( rule__WidgetOptType__ParamAssignment_1 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1169:2: rule__WidgetOptType__ParamAssignment_1
            {
            pushFollow(FOLLOW_rule__WidgetOptType__ParamAssignment_1_in_rule__WidgetOptType__Group__1__Impl2311);
            rule__WidgetOptType__ParamAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getParamAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__Group__1__Impl"


    // $ANTLR start "rule__UIQuestionnaire__QuestWidgetsAssignment_1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1184:1: rule__UIQuestionnaire__QuestWidgetsAssignment_1 : ( ruleWidgetQuest ) ;
    public final void rule__UIQuestionnaire__QuestWidgetsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1188:1: ( ( ruleWidgetQuest ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1189:1: ( ruleWidgetQuest )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1189:1: ( ruleWidgetQuest )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1190:1: ruleWidgetQuest
            {
             before(grammarAccess.getUIQuestionnaireAccess().getQuestWidgetsWidgetQuestParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleWidgetQuest_in_rule__UIQuestionnaire__QuestWidgetsAssignment_12350);
            ruleWidgetQuest();

            state._fsp--;

             after(grammarAccess.getUIQuestionnaireAccess().getQuestWidgetsWidgetQuestParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__QuestWidgetsAssignment_1"


    // $ANTLR start "rule__UIQuestionnaire__OptQuestWidgetAssignment_4"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1199:1: rule__UIQuestionnaire__OptQuestWidgetAssignment_4 : ( ruleWidgetOptQuest ) ;
    public final void rule__UIQuestionnaire__OptQuestWidgetAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1203:1: ( ( ruleWidgetOptQuest ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1204:1: ( ruleWidgetOptQuest )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1204:1: ( ruleWidgetOptQuest )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1205:1: ruleWidgetOptQuest
            {
             before(grammarAccess.getUIQuestionnaireAccess().getOptQuestWidgetWidgetOptQuestParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleWidgetOptQuest_in_rule__UIQuestionnaire__OptQuestWidgetAssignment_42381);
            ruleWidgetOptQuest();

            state._fsp--;

             after(grammarAccess.getUIQuestionnaireAccess().getOptQuestWidgetWidgetOptQuestParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__OptQuestWidgetAssignment_4"


    // $ANTLR start "rule__UIQuestionnaire__OptWidgetAssignment_7"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1214:1: rule__UIQuestionnaire__OptWidgetAssignment_7 : ( ruleOptQuest ) ;
    public final void rule__UIQuestionnaire__OptWidgetAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1218:1: ( ( ruleOptQuest ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1219:1: ( ruleOptQuest )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1219:1: ( ruleOptQuest )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1220:1: ruleOptQuest
            {
             before(grammarAccess.getUIQuestionnaireAccess().getOptWidgetOptQuestParserRuleCall_7_0()); 
            pushFollow(FOLLOW_ruleOptQuest_in_rule__UIQuestionnaire__OptWidgetAssignment_72412);
            ruleOptQuest();

            state._fsp--;

             after(grammarAccess.getUIQuestionnaireAccess().getOptWidgetOptQuestParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UIQuestionnaire__OptWidgetAssignment_7"


    // $ANTLR start "rule__WidgetQuest__QuestionAssignment_0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1229:1: rule__WidgetQuest__QuestionAssignment_0 : ( ruleQuestion ) ;
    public final void rule__WidgetQuest__QuestionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1233:1: ( ( ruleQuestion ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1234:1: ( ruleQuestion )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1234:1: ( ruleQuestion )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1235:1: ruleQuestion
            {
             before(grammarAccess.getWidgetQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleQuestion_in_rule__WidgetQuest__QuestionAssignment_02443);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getWidgetQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__QuestionAssignment_0"


    // $ANTLR start "rule__WidgetQuest__TypeAssignment_2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1244:1: rule__WidgetQuest__TypeAssignment_2 : ( ruleWidgetQuestType ) ;
    public final void rule__WidgetQuest__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1248:1: ( ( ruleWidgetQuestType ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1249:1: ( ruleWidgetQuestType )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1249:1: ( ruleWidgetQuestType )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1250:1: ruleWidgetQuestType
            {
             before(grammarAccess.getWidgetQuestAccess().getTypeWidgetQuestTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleWidgetQuestType_in_rule__WidgetQuest__TypeAssignment_22474);
            ruleWidgetQuestType();

            state._fsp--;

             after(grammarAccess.getWidgetQuestAccess().getTypeWidgetQuestTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetQuest__TypeAssignment_2"


    // $ANTLR start "rule__WidgetOptQuest__QuestionAssignment_0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1259:1: rule__WidgetOptQuest__QuestionAssignment_0 : ( ruleQuestion ) ;
    public final void rule__WidgetOptQuest__QuestionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1263:1: ( ( ruleQuestion ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1264:1: ( ruleQuestion )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1264:1: ( ruleQuestion )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1265:1: ruleQuestion
            {
             before(grammarAccess.getWidgetOptQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleQuestion_in_rule__WidgetOptQuest__QuestionAssignment_02505);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getWidgetOptQuestAccess().getQuestionQuestionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__QuestionAssignment_0"


    // $ANTLR start "rule__WidgetOptQuest__OptionAssignment_2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1274:1: rule__WidgetOptQuest__OptionAssignment_2 : ( ruleOption ) ;
    public final void rule__WidgetOptQuest__OptionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1278:1: ( ( ruleOption ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1279:1: ( ruleOption )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1279:1: ( ruleOption )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1280:1: ruleOption
            {
             before(grammarAccess.getWidgetOptQuestAccess().getOptionOptionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleOption_in_rule__WidgetOptQuest__OptionAssignment_22536);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getWidgetOptQuestAccess().getOptionOptionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__OptionAssignment_2"


    // $ANTLR start "rule__WidgetOptQuest__TypeAssignment_4"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1289:1: rule__WidgetOptQuest__TypeAssignment_4 : ( ruleWidgetOptType ) ;
    public final void rule__WidgetOptQuest__TypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1293:1: ( ( ruleWidgetOptType ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1294:1: ( ruleWidgetOptType )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1294:1: ( ruleWidgetOptType )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1295:1: ruleWidgetOptType
            {
             before(grammarAccess.getWidgetOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_4_0()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_rule__WidgetOptQuest__TypeAssignment_42567);
            ruleWidgetOptType();

            state._fsp--;

             after(grammarAccess.getWidgetOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptQuest__TypeAssignment_4"


    // $ANTLR start "rule__OptQuest__OptionAssignment_0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1304:1: rule__OptQuest__OptionAssignment_0 : ( ruleOption ) ;
    public final void rule__OptQuest__OptionAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1308:1: ( ( ruleOption ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1309:1: ( ruleOption )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1309:1: ( ruleOption )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1310:1: ruleOption
            {
             before(grammarAccess.getOptQuestAccess().getOptionOptionParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleOption_in_rule__OptQuest__OptionAssignment_02598);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getOptQuestAccess().getOptionOptionParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__OptionAssignment_0"


    // $ANTLR start "rule__OptQuest__TypeAssignment_2"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1319:1: rule__OptQuest__TypeAssignment_2 : ( ruleWidgetOptType ) ;
    public final void rule__OptQuest__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1323:1: ( ( ruleWidgetOptType ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1324:1: ( ruleWidgetOptType )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1324:1: ( ruleWidgetOptType )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1325:1: ruleWidgetOptType
            {
             before(grammarAccess.getOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleWidgetOptType_in_rule__OptQuest__TypeAssignment_22629);
            ruleWidgetOptType();

            state._fsp--;

             after(grammarAccess.getOptQuestAccess().getTypeWidgetOptTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptQuest__TypeAssignment_2"


    // $ANTLR start "rule__Question__NameAssignment"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1334:1: rule__Question__NameAssignment : ( RULE_ID ) ;
    public final void rule__Question__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1338:1: ( ( RULE_ID ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1339:1: ( RULE_ID )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1339:1: ( RULE_ID )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1340:1: RULE_ID
            {
             before(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Question__NameAssignment2660); 
             after(grammarAccess.getQuestionAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__NameAssignment"


    // $ANTLR start "rule__Option__NameAssignment"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1349:1: rule__Option__NameAssignment : ( RULE_ID ) ;
    public final void rule__Option__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1353:1: ( ( RULE_ID ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1354:1: ( RULE_ID )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1354:1: ( RULE_ID )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1355:1: RULE_ID
            {
             before(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Option__NameAssignment2691); 
             after(grammarAccess.getOptionAccess().getNameIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__NameAssignment"


    // $ANTLR start "rule__WidgetOptType__TypeAssignment_0"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1364:1: rule__WidgetOptType__TypeAssignment_0 : ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) ) ;
    public final void rule__WidgetOptType__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1368:1: ( ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1369:1: ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1369:1: ( ( rule__WidgetOptType__TypeAlternatives_0_0 ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1370:1: ( rule__WidgetOptType__TypeAlternatives_0_0 )
            {
             before(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); 
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1371:1: ( rule__WidgetOptType__TypeAlternatives_0_0 )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1371:2: rule__WidgetOptType__TypeAlternatives_0_0
            {
            pushFollow(FOLLOW_rule__WidgetOptType__TypeAlternatives_0_0_in_rule__WidgetOptType__TypeAssignment_02722);
            rule__WidgetOptType__TypeAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getWidgetOptTypeAccess().getTypeAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__TypeAssignment_0"


    // $ANTLR start "rule__WidgetOptType__ParamAssignment_1"
    // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1380:1: rule__WidgetOptType__ParamAssignment_1 : ( RULE_STRING ) ;
    public final void rule__WidgetOptType__ParamAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1384:1: ( ( RULE_STRING ) )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1385:1: ( RULE_STRING )
            {
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1385:1: ( RULE_STRING )
            // ../m2.idm.tp3.UIQuestionnaire.ui/src-gen/m2/idm/tp3/ui/contentassist/antlr/internal/InternalUIQuestionnaire.g:1386:1: RULE_STRING
            {
             before(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__WidgetOptType__ParamAssignment_12755); 
             after(grammarAccess.getWidgetOptTypeAccess().getParamSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WidgetOptType__ParamAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleUIQuestionnaire_in_entryRuleUIQuestionnaire61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUIQuestionnaire68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__0_in_ruleUIQuestionnaire94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuest_in_entryRuleWidgetQuest121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuest128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__0_in_ruleWidgetQuest154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptQuest_in_entryRuleWidgetOptQuest181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptQuest188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__0_in_ruleWidgetOptQuest214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptQuest_in_entryRuleOptQuest241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOptQuest248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__0_in_ruleOptQuest274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__NameAssignment_in_ruleQuestion334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__NameAssignment_in_ruleOption394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_entryRuleWidgetQuestType421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetQuestType428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuestType__Alternatives_in_ruleWidgetQuestType454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_entryRuleWidgetOptType481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidgetOptType488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__0_in_ruleWidgetOptType514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleImage575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox603 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleCheckbox637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_entryRuleRadio665 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadio672 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleRadio699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_entryRuleSelect727 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSelect734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleSelect761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput789 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleInput823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_rule__WidgetQuestType__Alternatives860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadio_in_rule__WidgetQuestType__Alternatives877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSelect_in_rule__WidgetQuestType__Alternatives894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_rule__WidgetOptType__TypeAlternatives_0_0926 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_rule__WidgetOptType__TypeAlternatives_0_0943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__0__Impl_in_rule__UIQuestionnaire__Group__0973 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__1_in_rule__UIQuestionnaire__Group__0976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__UIQuestionnaire__Group__0__Impl1004 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__1__Impl_in_rule__UIQuestionnaire__Group__11035 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__2_in_rule__UIQuestionnaire__Group__11038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__QuestWidgetsAssignment_1_in_rule__UIQuestionnaire__Group__1__Impl1065 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__2__Impl_in_rule__UIQuestionnaire__Group__21096 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__3_in_rule__UIQuestionnaire__Group__21099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__UIQuestionnaire__Group__2__Impl1127 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__3__Impl_in_rule__UIQuestionnaire__Group__31158 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__4_in_rule__UIQuestionnaire__Group__31161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__UIQuestionnaire__Group__3__Impl1189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__4__Impl_in_rule__UIQuestionnaire__Group__41220 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__5_in_rule__UIQuestionnaire__Group__41223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__OptQuestWidgetAssignment_4_in_rule__UIQuestionnaire__Group__4__Impl1250 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__5__Impl_in_rule__UIQuestionnaire__Group__51281 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__6_in_rule__UIQuestionnaire__Group__51284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__UIQuestionnaire__Group__5__Impl1312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__6__Impl_in_rule__UIQuestionnaire__Group__61343 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__7_in_rule__UIQuestionnaire__Group__61346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__UIQuestionnaire__Group__6__Impl1374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__7__Impl_in_rule__UIQuestionnaire__Group__71405 = new BitSet(new long[]{0x0000000000020010L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__8_in_rule__UIQuestionnaire__Group__71408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__OptWidgetAssignment_7_in_rule__UIQuestionnaire__Group__7__Impl1435 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__UIQuestionnaire__Group__8__Impl_in_rule__UIQuestionnaire__Group__81466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__UIQuestionnaire__Group__8__Impl1494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__0__Impl_in_rule__WidgetQuest__Group__01543 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__1_in_rule__WidgetQuest__Group__01546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__QuestionAssignment_0_in_rule__WidgetQuest__Group__0__Impl1573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__1__Impl_in_rule__WidgetQuest__Group__11603 = new BitSet(new long[]{0x0000000000007000L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__2_in_rule__WidgetQuest__Group__11606 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__WidgetQuest__Group__1__Impl1634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__Group__2__Impl_in_rule__WidgetQuest__Group__21665 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetQuest__TypeAssignment_2_in_rule__WidgetQuest__Group__2__Impl1692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__0__Impl_in_rule__WidgetOptQuest__Group__01728 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__1_in_rule__WidgetOptQuest__Group__01731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__QuestionAssignment_0_in_rule__WidgetOptQuest__Group__0__Impl1758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__1__Impl_in_rule__WidgetOptQuest__Group__11788 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__2_in_rule__WidgetOptQuest__Group__11791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__WidgetOptQuest__Group__1__Impl1819 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__2__Impl_in_rule__WidgetOptQuest__Group__21850 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__3_in_rule__WidgetOptQuest__Group__21853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__OptionAssignment_2_in_rule__WidgetOptQuest__Group__2__Impl1880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__3__Impl_in_rule__WidgetOptQuest__Group__31910 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__4_in_rule__WidgetOptQuest__Group__31913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__WidgetOptQuest__Group__3__Impl1941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__Group__4__Impl_in_rule__WidgetOptQuest__Group__41972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptQuest__TypeAssignment_4_in_rule__WidgetOptQuest__Group__4__Impl1999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__0__Impl_in_rule__OptQuest__Group__02039 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__1_in_rule__OptQuest__Group__02042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__OptionAssignment_0_in_rule__OptQuest__Group__0__Impl2069 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__1__Impl_in_rule__OptQuest__Group__12099 = new BitSet(new long[]{0x0000000000008800L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__2_in_rule__OptQuest__Group__12102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__OptQuest__Group__1__Impl2130 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__Group__2__Impl_in_rule__OptQuest__Group__22161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OptQuest__TypeAssignment_2_in_rule__OptQuest__Group__2__Impl2188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__0__Impl_in_rule__WidgetOptType__Group__02224 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__1_in_rule__WidgetOptType__Group__02227 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__TypeAssignment_0_in_rule__WidgetOptType__Group__0__Impl2254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__Group__1__Impl_in_rule__WidgetOptType__Group__12284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__ParamAssignment_1_in_rule__WidgetOptType__Group__1__Impl2311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuest_in_rule__UIQuestionnaire__QuestWidgetsAssignment_12350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptQuest_in_rule__UIQuestionnaire__OptQuestWidgetAssignment_42381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOptQuest_in_rule__UIQuestionnaire__OptWidgetAssignment_72412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_rule__WidgetQuest__QuestionAssignment_02443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetQuestType_in_rule__WidgetQuest__TypeAssignment_22474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_rule__WidgetOptQuest__QuestionAssignment_02505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_rule__WidgetOptQuest__OptionAssignment_22536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_rule__WidgetOptQuest__TypeAssignment_42567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_rule__OptQuest__OptionAssignment_02598 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidgetOptType_in_rule__OptQuest__TypeAssignment_22629 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Question__NameAssignment2660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Option__NameAssignment2691 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WidgetOptType__TypeAlternatives_0_0_in_rule__WidgetOptType__TypeAssignment_02722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__WidgetOptType__ParamAssignment_12755 = new BitSet(new long[]{0x0000000000000002L});

}