package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.example.mydsl.myDsl.Checkbox;
import org.xtext.example.mydsl.myDsl.CheckboxGroup;
import org.xtext.example.mydsl.myDsl.Image;
import org.xtext.example.mydsl.myDsl.Input;
import org.xtext.example.mydsl.myDsl.MyDslPackage;
import org.xtext.example.mydsl.myDsl.RadioButton;
import org.xtext.example.mydsl.myDsl.RadioButtonGroup;
import org.xtext.example.mydsl.myDsl.Widget;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;

@SuppressWarnings("all")
public class MyDslSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private MyDslGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == MyDslPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case MyDslPackage.CHECKBOX:
				if(context == grammarAccess.getCheckboxRule()) {
					sequence_Checkbox(context, (Checkbox) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.CHECKBOX_GROUP:
				if(context == grammarAccess.getCheckboxGroupRule() ||
				   context == grammarAccess.getItemRule()) {
					sequence_CheckboxGroup(context, (CheckboxGroup) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.IMAGE:
				if(context == grammarAccess.getImageRule() ||
				   context == grammarAccess.getItemRule()) {
					sequence_Image(context, (Image) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.INPUT:
				if(context == grammarAccess.getInputRule() ||
				   context == grammarAccess.getItemRule()) {
					sequence_Input(context, (Input) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.RADIO_BUTTON:
				if(context == grammarAccess.getRadioButtonRule()) {
					sequence_RadioButton(context, (RadioButton) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.RADIO_BUTTON_GROUP:
				if(context == grammarAccess.getItemRule() ||
				   context == grammarAccess.getRadioButtonGroupRule()) {
					sequence_RadioButtonGroup(context, (RadioButtonGroup) semanticObject); 
					return; 
				}
				else break;
			case MyDslPackage.WIDGET:
				if(context == grammarAccess.getWidgetRule()) {
					sequence_Widget(context, (Widget) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (name=ID? label=STRING checkboxs+=Checkbox+)
	 */
	protected void sequence_CheckboxGroup(EObject context, CheckboxGroup semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? label=STRING value=INT)
	 */
	protected void sequence_Checkbox(EObject context, Checkbox semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (source=STRING alt=STRING)
	 */
	protected void sequence_Image(EObject context, Image semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslPackage.Literals.IMAGE__SOURCE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslPackage.Literals.IMAGE__SOURCE));
			if(transientValues.isValueTransient(semanticObject, MyDslPackage.Literals.IMAGE__ALT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslPackage.Literals.IMAGE__ALT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getImageAccess().getSourceSTRINGTerminalRuleCall_2_0(), semanticObject.getSource());
		feeder.accept(grammarAccess.getImageAccess().getAltSTRINGTerminalRuleCall_4_0(), semanticObject.getAlt());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? label=STRING value=STRING)
	 */
	protected void sequence_Input(EObject context, Input semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? label=STRING checkedRadioButton?=RadioButton uncheckedRadioButton+=RadioButton*)
	 */
	protected void sequence_RadioButtonGroup(EObject context, RadioButtonGroup semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID? label=STRING)
	 */
	protected void sequence_RadioButton(EObject context, RadioButton semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     items+=Item+
	 */
	protected void sequence_Widget(EObject context, Widget semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
