/**
 */
package org.xtext.example.mydsl.myDsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myDsl.MyDslFactory
 * @model kind="package"
 * @generated
 */
public interface MyDslPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "myDsl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/MyDsl";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "myDsl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyDslPackage eINSTANCE = org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.WidgetImpl <em>Widget</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.WidgetImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getWidget()
   * @generated
   */
  int WIDGET = 0;

  /**
   * The feature id for the '<em><b>Items</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET__ITEMS = 0;

  /**
   * The number of structural features of the '<em>Widget</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ItemImpl <em>Item</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ItemImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getItem()
   * @generated
   */
  int ITEM = 1;

  /**
   * The number of structural features of the '<em>Item</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ITEM_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.ImageImpl <em>Image</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.ImageImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getImage()
   * @generated
   */
  int IMAGE = 2;

  /**
   * The feature id for the '<em><b>Source</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE__SOURCE = ITEM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Alt</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE__ALT = ITEM_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Image</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE_FEATURE_COUNT = ITEM_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.CheckboxGroupImpl <em>Checkbox Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.CheckboxGroupImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCheckboxGroup()
   * @generated
   */
  int CHECKBOX_GROUP = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX_GROUP__NAME = ITEM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX_GROUP__LABEL = ITEM_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Checkboxs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX_GROUP__CHECKBOXS = ITEM_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Checkbox Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX_GROUP_FEATURE_COUNT = ITEM_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.CheckboxImpl <em>Checkbox</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.CheckboxImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCheckbox()
   * @generated
   */
  int CHECKBOX = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX__NAME = 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX__LABEL = 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX__VALUE = 2;

  /**
   * The number of structural features of the '<em>Checkbox</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECKBOX_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl <em>Radio Button Group</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getRadioButtonGroup()
   * @generated
   */
  int RADIO_BUTTON_GROUP = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_GROUP__NAME = ITEM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_GROUP__LABEL = ITEM_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Checked Radio Button</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON = ITEM_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Unchecked Radio Button</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON = ITEM_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Radio Button Group</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_GROUP_FEATURE_COUNT = ITEM_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonImpl <em>Radio Button</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.RadioButtonImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getRadioButton()
   * @generated
   */
  int RADIO_BUTTON = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON__NAME = 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON__LABEL = 1;

  /**
   * The number of structural features of the '<em>Radio Button</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDsl.impl.InputImpl <em>Input</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDsl.impl.InputImpl
   * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getInput()
   * @generated
   */
  int INPUT = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT__NAME = ITEM_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT__LABEL = ITEM_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT__VALUE = ITEM_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Input</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INPUT_FEATURE_COUNT = ITEM_FEATURE_COUNT + 3;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Widget <em>Widget</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Widget</em>'.
   * @see org.xtext.example.mydsl.myDsl.Widget
   * @generated
   */
  EClass getWidget();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.Widget#getItems <em>Items</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Items</em>'.
   * @see org.xtext.example.mydsl.myDsl.Widget#getItems()
   * @see #getWidget()
   * @generated
   */
  EReference getWidget_Items();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Item <em>Item</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Item</em>'.
   * @see org.xtext.example.mydsl.myDsl.Item
   * @generated
   */
  EClass getItem();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Image <em>Image</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Image</em>'.
   * @see org.xtext.example.mydsl.myDsl.Image
   * @generated
   */
  EClass getImage();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Image#getSource <em>Source</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Source</em>'.
   * @see org.xtext.example.mydsl.myDsl.Image#getSource()
   * @see #getImage()
   * @generated
   */
  EAttribute getImage_Source();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Image#getAlt <em>Alt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Alt</em>'.
   * @see org.xtext.example.mydsl.myDsl.Image#getAlt()
   * @see #getImage()
   * @generated
   */
  EAttribute getImage_Alt();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.CheckboxGroup <em>Checkbox Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Checkbox Group</em>'.
   * @see org.xtext.example.mydsl.myDsl.CheckboxGroup
   * @generated
   */
  EClass getCheckboxGroup();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.CheckboxGroup#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.CheckboxGroup#getName()
   * @see #getCheckboxGroup()
   * @generated
   */
  EAttribute getCheckboxGroup_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.CheckboxGroup#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.xtext.example.mydsl.myDsl.CheckboxGroup#getLabel()
   * @see #getCheckboxGroup()
   * @generated
   */
  EAttribute getCheckboxGroup_Label();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.CheckboxGroup#getCheckboxs <em>Checkboxs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Checkboxs</em>'.
   * @see org.xtext.example.mydsl.myDsl.CheckboxGroup#getCheckboxs()
   * @see #getCheckboxGroup()
   * @generated
   */
  EReference getCheckboxGroup_Checkboxs();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Checkbox <em>Checkbox</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Checkbox</em>'.
   * @see org.xtext.example.mydsl.myDsl.Checkbox
   * @generated
   */
  EClass getCheckbox();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Checkbox#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.Checkbox#getName()
   * @see #getCheckbox()
   * @generated
   */
  EAttribute getCheckbox_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Checkbox#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.xtext.example.mydsl.myDsl.Checkbox#getLabel()
   * @see #getCheckbox()
   * @generated
   */
  EAttribute getCheckbox_Label();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Checkbox#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.example.mydsl.myDsl.Checkbox#getValue()
   * @see #getCheckbox()
   * @generated
   */
  EAttribute getCheckbox_Value();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup <em>Radio Button Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Radio Button Group</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButtonGroup
   * @generated
   */
  EClass getRadioButtonGroup();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButtonGroup#getName()
   * @see #getRadioButtonGroup()
   * @generated
   */
  EAttribute getRadioButtonGroup_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButtonGroup#getLabel()
   * @see #getRadioButtonGroup()
   * @generated
   */
  EAttribute getRadioButtonGroup_Label();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#isCheckedRadioButton <em>Checked Radio Button</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Checked Radio Button</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButtonGroup#isCheckedRadioButton()
   * @see #getRadioButtonGroup()
   * @generated
   */
  EAttribute getRadioButtonGroup_CheckedRadioButton();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getUncheckedRadioButton <em>Unchecked Radio Button</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Unchecked Radio Button</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButtonGroup#getUncheckedRadioButton()
   * @see #getRadioButtonGroup()
   * @generated
   */
  EReference getRadioButtonGroup_UncheckedRadioButton();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.RadioButton <em>Radio Button</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Radio Button</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButton
   * @generated
   */
  EClass getRadioButton();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.RadioButton#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButton#getName()
   * @see #getRadioButton()
   * @generated
   */
  EAttribute getRadioButton_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.RadioButton#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.xtext.example.mydsl.myDsl.RadioButton#getLabel()
   * @see #getRadioButton()
   * @generated
   */
  EAttribute getRadioButton_Label();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDsl.Input <em>Input</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Input</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input
   * @generated
   */
  EClass getInput();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Input#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input#getName()
   * @see #getInput()
   * @generated
   */
  EAttribute getInput_Name();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Input#getLabel <em>Label</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Label</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input#getLabel()
   * @see #getInput()
   * @generated
   */
  EAttribute getInput_Label();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDsl.Input#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see org.xtext.example.mydsl.myDsl.Input#getValue()
   * @see #getInput()
   * @generated
   */
  EAttribute getInput_Value();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MyDslFactory getMyDslFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.WidgetImpl <em>Widget</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.WidgetImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getWidget()
     * @generated
     */
    EClass WIDGET = eINSTANCE.getWidget();

    /**
     * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WIDGET__ITEMS = eINSTANCE.getWidget_Items();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ItemImpl <em>Item</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ItemImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getItem()
     * @generated
     */
    EClass ITEM = eINSTANCE.getItem();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.ImageImpl <em>Image</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.ImageImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getImage()
     * @generated
     */
    EClass IMAGE = eINSTANCE.getImage();

    /**
     * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMAGE__SOURCE = eINSTANCE.getImage_Source();

    /**
     * The meta object literal for the '<em><b>Alt</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMAGE__ALT = eINSTANCE.getImage_Alt();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.CheckboxGroupImpl <em>Checkbox Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.CheckboxGroupImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCheckboxGroup()
     * @generated
     */
    EClass CHECKBOX_GROUP = eINSTANCE.getCheckboxGroup();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECKBOX_GROUP__NAME = eINSTANCE.getCheckboxGroup_Name();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECKBOX_GROUP__LABEL = eINSTANCE.getCheckboxGroup_Label();

    /**
     * The meta object literal for the '<em><b>Checkboxs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHECKBOX_GROUP__CHECKBOXS = eINSTANCE.getCheckboxGroup_Checkboxs();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.CheckboxImpl <em>Checkbox</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.CheckboxImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getCheckbox()
     * @generated
     */
    EClass CHECKBOX = eINSTANCE.getCheckbox();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECKBOX__NAME = eINSTANCE.getCheckbox_Name();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECKBOX__LABEL = eINSTANCE.getCheckbox_Label();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECKBOX__VALUE = eINSTANCE.getCheckbox_Value();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl <em>Radio Button Group</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getRadioButtonGroup()
     * @generated
     */
    EClass RADIO_BUTTON_GROUP = eINSTANCE.getRadioButtonGroup();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RADIO_BUTTON_GROUP__NAME = eINSTANCE.getRadioButtonGroup_Name();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RADIO_BUTTON_GROUP__LABEL = eINSTANCE.getRadioButtonGroup_Label();

    /**
     * The meta object literal for the '<em><b>Checked Radio Button</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON = eINSTANCE.getRadioButtonGroup_CheckedRadioButton();

    /**
     * The meta object literal for the '<em><b>Unchecked Radio Button</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON = eINSTANCE.getRadioButtonGroup_UncheckedRadioButton();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonImpl <em>Radio Button</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.RadioButtonImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getRadioButton()
     * @generated
     */
    EClass RADIO_BUTTON = eINSTANCE.getRadioButton();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RADIO_BUTTON__NAME = eINSTANCE.getRadioButton_Name();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RADIO_BUTTON__LABEL = eINSTANCE.getRadioButton_Label();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDsl.impl.InputImpl <em>Input</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDsl.impl.InputImpl
     * @see org.xtext.example.mydsl.myDsl.impl.MyDslPackageImpl#getInput()
     * @generated
     */
    EClass INPUT = eINSTANCE.getInput();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INPUT__NAME = eINSTANCE.getInput_Name();

    /**
     * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INPUT__LABEL = eINSTANCE.getInput_Label();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INPUT__VALUE = eINSTANCE.getInput_Value();

  }

} //MyDslPackage
