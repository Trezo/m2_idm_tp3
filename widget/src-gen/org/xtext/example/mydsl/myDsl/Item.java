/**
 */
package org.xtext.example.mydsl.myDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getItem()
 * @model
 * @generated
 */
public interface Item extends EObject
{
} // Item
