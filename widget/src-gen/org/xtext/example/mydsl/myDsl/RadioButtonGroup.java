/**
 */
package org.xtext.example.mydsl.myDsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Radio Button Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#isCheckedRadioButton <em>Checked Radio Button</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getUncheckedRadioButton <em>Unchecked Radio Button</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getRadioButtonGroup()
 * @model
 * @generated
 */
public interface RadioButtonGroup extends Item
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getRadioButtonGroup_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Label</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Label</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Label</em>' attribute.
   * @see #setLabel(String)
   * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getRadioButtonGroup_Label()
   * @model
   * @generated
   */
  String getLabel();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#getLabel <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Label</em>' attribute.
   * @see #getLabel()
   * @generated
   */
  void setLabel(String value);

  /**
   * Returns the value of the '<em><b>Checked Radio Button</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Checked Radio Button</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Checked Radio Button</em>' attribute.
   * @see #setCheckedRadioButton(boolean)
   * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getRadioButtonGroup_CheckedRadioButton()
   * @model
   * @generated
   */
  boolean isCheckedRadioButton();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDsl.RadioButtonGroup#isCheckedRadioButton <em>Checked Radio Button</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Checked Radio Button</em>' attribute.
   * @see #isCheckedRadioButton()
   * @generated
   */
  void setCheckedRadioButton(boolean value);

  /**
   * Returns the value of the '<em><b>Unchecked Radio Button</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myDsl.RadioButton}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unchecked Radio Button</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unchecked Radio Button</em>' containment reference list.
   * @see org.xtext.example.mydsl.myDsl.MyDslPackage#getRadioButtonGroup_UncheckedRadioButton()
   * @model containment="true"
   * @generated
   */
  EList<RadioButton> getUncheckedRadioButton();

} // RadioButtonGroup
