/**
 */
package org.xtext.example.mydsl.myDsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.example.mydsl.myDsl.MyDslPackage;
import org.xtext.example.mydsl.myDsl.RadioButton;
import org.xtext.example.mydsl.myDsl.RadioButtonGroup;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Radio Button Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl#isCheckedRadioButton <em>Checked Radio Button</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDsl.impl.RadioButtonGroupImpl#getUncheckedRadioButton <em>Unchecked Radio Button</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RadioButtonGroupImpl extends ItemImpl implements RadioButtonGroup
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabel()
   * @generated
   * @ordered
   */
  protected static final String LABEL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLabel()
   * @generated
   * @ordered
   */
  protected String label = LABEL_EDEFAULT;

  /**
   * The default value of the '{@link #isCheckedRadioButton() <em>Checked Radio Button</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCheckedRadioButton()
   * @generated
   * @ordered
   */
  protected static final boolean CHECKED_RADIO_BUTTON_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isCheckedRadioButton() <em>Checked Radio Button</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isCheckedRadioButton()
   * @generated
   * @ordered
   */
  protected boolean checkedRadioButton = CHECKED_RADIO_BUTTON_EDEFAULT;

  /**
   * The cached value of the '{@link #getUncheckedRadioButton() <em>Unchecked Radio Button</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUncheckedRadioButton()
   * @generated
   * @ordered
   */
  protected EList<RadioButton> uncheckedRadioButton;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RadioButtonGroupImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyDslPackage.Literals.RADIO_BUTTON_GROUP;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyDslPackage.RADIO_BUTTON_GROUP__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLabel()
  {
    return label;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLabel(String newLabel)
  {
    String oldLabel = label;
    label = newLabel;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyDslPackage.RADIO_BUTTON_GROUP__LABEL, oldLabel, label));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isCheckedRadioButton()
  {
    return checkedRadioButton;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheckedRadioButton(boolean newCheckedRadioButton)
  {
    boolean oldCheckedRadioButton = checkedRadioButton;
    checkedRadioButton = newCheckedRadioButton;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyDslPackage.RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON, oldCheckedRadioButton, checkedRadioButton));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<RadioButton> getUncheckedRadioButton()
  {
    if (uncheckedRadioButton == null)
    {
      uncheckedRadioButton = new EObjectContainmentEList<RadioButton>(RadioButton.class, this, MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON);
    }
    return uncheckedRadioButton;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON:
        return ((InternalEList<?>)getUncheckedRadioButton()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyDslPackage.RADIO_BUTTON_GROUP__NAME:
        return getName();
      case MyDslPackage.RADIO_BUTTON_GROUP__LABEL:
        return getLabel();
      case MyDslPackage.RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON:
        return isCheckedRadioButton();
      case MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON:
        return getUncheckedRadioButton();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyDslPackage.RADIO_BUTTON_GROUP__NAME:
        setName((String)newValue);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__LABEL:
        setLabel((String)newValue);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON:
        setCheckedRadioButton((Boolean)newValue);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON:
        getUncheckedRadioButton().clear();
        getUncheckedRadioButton().addAll((Collection<? extends RadioButton>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyDslPackage.RADIO_BUTTON_GROUP__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__LABEL:
        setLabel(LABEL_EDEFAULT);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON:
        setCheckedRadioButton(CHECKED_RADIO_BUTTON_EDEFAULT);
        return;
      case MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON:
        getUncheckedRadioButton().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyDslPackage.RADIO_BUTTON_GROUP__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MyDslPackage.RADIO_BUTTON_GROUP__LABEL:
        return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
      case MyDslPackage.RADIO_BUTTON_GROUP__CHECKED_RADIO_BUTTON:
        return checkedRadioButton != CHECKED_RADIO_BUTTON_EDEFAULT;
      case MyDslPackage.RADIO_BUTTON_GROUP__UNCHECKED_RADIO_BUTTON:
        return uncheckedRadioButton != null && !uncheckedRadioButton.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", label: ");
    result.append(label);
    result.append(", checkedRadioButton: ");
    result.append(checkedRadioButton);
    result.append(')');
    return result.toString();
  }

} //RadioButtonGroupImpl
