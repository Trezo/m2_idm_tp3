package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Widget'", "'{'", "'}'", "'Image'", "'('", "','", "')'", "'CheckboxGroup'", "'Checkbox'", "'RadioButtonGroup'", "'Checked'", "'Unchecked'", "'RadioButton'", "'Input'"
    };
    public static final int RULE_ID=5;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;
     	
        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Widget";	
       	}
       	
       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleWidget"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:67:1: entryRuleWidget returns [EObject current=null] : iv_ruleWidget= ruleWidget EOF ;
    public final EObject entryRuleWidget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidget = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:68:2: (iv_ruleWidget= ruleWidget EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:69:2: iv_ruleWidget= ruleWidget EOF
            {
             newCompositeNode(grammarAccess.getWidgetRule()); 
            pushFollow(FOLLOW_ruleWidget_in_entryRuleWidget75);
            iv_ruleWidget=ruleWidget();

            state._fsp--;

             current =iv_ruleWidget; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidget85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidget"


    // $ANTLR start "ruleWidget"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:76:1: ruleWidget returns [EObject current=null] : (otherlv_0= 'Widget' otherlv_1= '{' ( (lv_items_2_0= ruleItem ) )+ otherlv_3= '}' ) ;
    public final EObject ruleWidget() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_items_2_0 = null;


         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:79:28: ( (otherlv_0= 'Widget' otherlv_1= '{' ( (lv_items_2_0= ruleItem ) )+ otherlv_3= '}' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:80:1: (otherlv_0= 'Widget' otherlv_1= '{' ( (lv_items_2_0= ruleItem ) )+ otherlv_3= '}' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:80:1: (otherlv_0= 'Widget' otherlv_1= '{' ( (lv_items_2_0= ruleItem ) )+ otherlv_3= '}' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:80:3: otherlv_0= 'Widget' otherlv_1= '{' ( (lv_items_2_0= ruleItem ) )+ otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleWidget122); 

                	newLeafNode(otherlv_0, grammarAccess.getWidgetAccess().getWidgetKeyword_0());
                
            otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleWidget134); 

                	newLeafNode(otherlv_1, grammarAccess.getWidgetAccess().getLeftCurlyBracketKeyword_1());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:88:1: ( (lv_items_2_0= ruleItem ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14||LA1_0==18||LA1_0==20||LA1_0==24) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:89:1: (lv_items_2_0= ruleItem )
            	    {
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:89:1: (lv_items_2_0= ruleItem )
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:90:3: lv_items_2_0= ruleItem
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getWidgetAccess().getItemsItemParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleItem_in_ruleWidget155);
            	    lv_items_2_0=ruleItem();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getWidgetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"items",
            	            		lv_items_2_0, 
            	            		"Item");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleWidget168); 

                	newLeafNode(otherlv_3, grammarAccess.getWidgetAccess().getRightCurlyBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidget"


    // $ANTLR start "entryRuleItem"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:118:1: entryRuleItem returns [EObject current=null] : iv_ruleItem= ruleItem EOF ;
    public final EObject entryRuleItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleItem = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:119:2: (iv_ruleItem= ruleItem EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:120:2: iv_ruleItem= ruleItem EOF
            {
             newCompositeNode(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_ruleItem_in_entryRuleItem204);
            iv_ruleItem=ruleItem();

            state._fsp--;

             current =iv_ruleItem; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleItem214); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:127:1: ruleItem returns [EObject current=null] : (this_Image_0= ruleImage | this_CheckboxGroup_1= ruleCheckboxGroup | this_RadioButtonGroup_2= ruleRadioButtonGroup | this_Input_3= ruleInput ) ;
    public final EObject ruleItem() throws RecognitionException {
        EObject current = null;

        EObject this_Image_0 = null;

        EObject this_CheckboxGroup_1 = null;

        EObject this_RadioButtonGroup_2 = null;

        EObject this_Input_3 = null;


         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:130:28: ( (this_Image_0= ruleImage | this_CheckboxGroup_1= ruleCheckboxGroup | this_RadioButtonGroup_2= ruleRadioButtonGroup | this_Input_3= ruleInput ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:131:1: (this_Image_0= ruleImage | this_CheckboxGroup_1= ruleCheckboxGroup | this_RadioButtonGroup_2= ruleRadioButtonGroup | this_Input_3= ruleInput )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:131:1: (this_Image_0= ruleImage | this_CheckboxGroup_1= ruleCheckboxGroup | this_RadioButtonGroup_2= ruleRadioButtonGroup | this_Input_3= ruleInput )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 14:
                {
                alt2=1;
                }
                break;
            case 18:
                {
                alt2=2;
                }
                break;
            case 20:
                {
                alt2=3;
                }
                break;
            case 24:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:132:5: this_Image_0= ruleImage
                    {
                     
                            newCompositeNode(grammarAccess.getItemAccess().getImageParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleImage_in_ruleItem261);
                    this_Image_0=ruleImage();

                    state._fsp--;

                     
                            current = this_Image_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:142:5: this_CheckboxGroup_1= ruleCheckboxGroup
                    {
                     
                            newCompositeNode(grammarAccess.getItemAccess().getCheckboxGroupParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleCheckboxGroup_in_ruleItem288);
                    this_CheckboxGroup_1=ruleCheckboxGroup();

                    state._fsp--;

                     
                            current = this_CheckboxGroup_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:152:5: this_RadioButtonGroup_2= ruleRadioButtonGroup
                    {
                     
                            newCompositeNode(grammarAccess.getItemAccess().getRadioButtonGroupParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleRadioButtonGroup_in_ruleItem315);
                    this_RadioButtonGroup_2=ruleRadioButtonGroup();

                    state._fsp--;

                     
                            current = this_RadioButtonGroup_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:162:5: this_Input_3= ruleInput
                    {
                     
                            newCompositeNode(grammarAccess.getItemAccess().getInputParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleInput_in_ruleItem342);
                    this_Input_3=ruleInput();

                    state._fsp--;

                     
                            current = this_Input_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleImage"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:178:1: entryRuleImage returns [EObject current=null] : iv_ruleImage= ruleImage EOF ;
    public final EObject entryRuleImage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImage = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:179:2: (iv_ruleImage= ruleImage EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:180:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage377);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage387); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:187:1: ruleImage returns [EObject current=null] : (otherlv_0= 'Image' otherlv_1= '(' ( (lv_source_2_0= RULE_STRING ) ) otherlv_3= ',' ( (lv_alt_4_0= RULE_STRING ) ) otherlv_5= ')' ) ;
    public final EObject ruleImage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_source_2_0=null;
        Token otherlv_3=null;
        Token lv_alt_4_0=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:190:28: ( (otherlv_0= 'Image' otherlv_1= '(' ( (lv_source_2_0= RULE_STRING ) ) otherlv_3= ',' ( (lv_alt_4_0= RULE_STRING ) ) otherlv_5= ')' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:191:1: (otherlv_0= 'Image' otherlv_1= '(' ( (lv_source_2_0= RULE_STRING ) ) otherlv_3= ',' ( (lv_alt_4_0= RULE_STRING ) ) otherlv_5= ')' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:191:1: (otherlv_0= 'Image' otherlv_1= '(' ( (lv_source_2_0= RULE_STRING ) ) otherlv_3= ',' ( (lv_alt_4_0= RULE_STRING ) ) otherlv_5= ')' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:191:3: otherlv_0= 'Image' otherlv_1= '(' ( (lv_source_2_0= RULE_STRING ) ) otherlv_3= ',' ( (lv_alt_4_0= RULE_STRING ) ) otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_ruleImage424); 

                	newLeafNode(otherlv_0, grammarAccess.getImageAccess().getImageKeyword_0());
                
            otherlv_1=(Token)match(input,15,FOLLOW_15_in_ruleImage436); 

                	newLeafNode(otherlv_1, grammarAccess.getImageAccess().getLeftParenthesisKeyword_1());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:199:1: ( (lv_source_2_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:200:1: (lv_source_2_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:200:1: (lv_source_2_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:201:3: lv_source_2_0= RULE_STRING
            {
            lv_source_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleImage453); 

            			newLeafNode(lv_source_2_0, grammarAccess.getImageAccess().getSourceSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getImageRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"source",
                    		lv_source_2_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_16_in_ruleImage470); 

                	newLeafNode(otherlv_3, grammarAccess.getImageAccess().getCommaKeyword_3());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:221:1: ( (lv_alt_4_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:222:1: (lv_alt_4_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:222:1: (lv_alt_4_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:223:3: lv_alt_4_0= RULE_STRING
            {
            lv_alt_4_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleImage487); 

            			newLeafNode(lv_alt_4_0, grammarAccess.getImageAccess().getAltSTRINGTerminalRuleCall_4_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getImageRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"alt",
                    		lv_alt_4_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_5=(Token)match(input,17,FOLLOW_17_in_ruleImage504); 

                	newLeafNode(otherlv_5, grammarAccess.getImageAccess().getRightParenthesisKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleCheckboxGroup"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:251:1: entryRuleCheckboxGroup returns [EObject current=null] : iv_ruleCheckboxGroup= ruleCheckboxGroup EOF ;
    public final EObject entryRuleCheckboxGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheckboxGroup = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:252:2: (iv_ruleCheckboxGroup= ruleCheckboxGroup EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:253:2: iv_ruleCheckboxGroup= ruleCheckboxGroup EOF
            {
             newCompositeNode(grammarAccess.getCheckboxGroupRule()); 
            pushFollow(FOLLOW_ruleCheckboxGroup_in_entryRuleCheckboxGroup540);
            iv_ruleCheckboxGroup=ruleCheckboxGroup();

            state._fsp--;

             current =iv_ruleCheckboxGroup; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckboxGroup550); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckboxGroup"


    // $ANTLR start "ruleCheckboxGroup"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:260:1: ruleCheckboxGroup returns [EObject current=null] : (otherlv_0= 'CheckboxGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) ( (lv_checkboxs_4_0= ruleCheckbox ) )+ otherlv_5= '}' ) ;
    public final EObject ruleCheckboxGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_label_3_0=null;
        Token otherlv_5=null;
        EObject lv_checkboxs_4_0 = null;


         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:263:28: ( (otherlv_0= 'CheckboxGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) ( (lv_checkboxs_4_0= ruleCheckbox ) )+ otherlv_5= '}' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:264:1: (otherlv_0= 'CheckboxGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) ( (lv_checkboxs_4_0= ruleCheckbox ) )+ otherlv_5= '}' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:264:1: (otherlv_0= 'CheckboxGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) ( (lv_checkboxs_4_0= ruleCheckbox ) )+ otherlv_5= '}' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:264:3: otherlv_0= 'CheckboxGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) ( (lv_checkboxs_4_0= ruleCheckbox ) )+ otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleCheckboxGroup587); 

                	newLeafNode(otherlv_0, grammarAccess.getCheckboxGroupAccess().getCheckboxGroupKeyword_0());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:268:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:269:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:269:1: (lv_name_1_0= RULE_ID )
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:270:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheckboxGroup604); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getCheckboxGroupAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCheckboxGroupRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleCheckboxGroup622); 

                	newLeafNode(otherlv_2, grammarAccess.getCheckboxGroupAccess().getLeftCurlyBracketKeyword_2());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:290:1: ( (lv_label_3_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:291:1: (lv_label_3_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:291:1: (lv_label_3_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:292:3: lv_label_3_0= RULE_STRING
            {
            lv_label_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleCheckboxGroup639); 

            			newLeafNode(lv_label_3_0, grammarAccess.getCheckboxGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCheckboxGroupRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"label",
                    		lv_label_3_0, 
                    		"STRING");
            	    

            }


            }

            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:308:2: ( (lv_checkboxs_4_0= ruleCheckbox ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==19) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:309:1: (lv_checkboxs_4_0= ruleCheckbox )
            	    {
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:309:1: (lv_checkboxs_4_0= ruleCheckbox )
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:310:3: lv_checkboxs_4_0= ruleCheckbox
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCheckboxGroupAccess().getCheckboxsCheckboxParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleCheckbox_in_ruleCheckboxGroup665);
            	    lv_checkboxs_4_0=ruleCheckbox();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCheckboxGroupRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"checkboxs",
            	            		lv_checkboxs_4_0, 
            	            		"Checkbox");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_13_in_ruleCheckboxGroup678); 

                	newLeafNode(otherlv_5, grammarAccess.getCheckboxGroupAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckboxGroup"


    // $ANTLR start "entryRuleCheckbox"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:338:1: entryRuleCheckbox returns [EObject current=null] : iv_ruleCheckbox= ruleCheckbox EOF ;
    public final EObject entryRuleCheckbox() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheckbox = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:339:2: (iv_ruleCheckbox= ruleCheckbox EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:340:2: iv_ruleCheckbox= ruleCheckbox EOF
            {
             newCompositeNode(grammarAccess.getCheckboxRule()); 
            pushFollow(FOLLOW_ruleCheckbox_in_entryRuleCheckbox714);
            iv_ruleCheckbox=ruleCheckbox();

            state._fsp--;

             current =iv_ruleCheckbox; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckbox724); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckbox"


    // $ANTLR start "ruleCheckbox"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:347:1: ruleCheckbox returns [EObject current=null] : (otherlv_0= 'Checkbox' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= ')' ) ;
    public final EObject ruleCheckbox() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_label_3_0=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:350:28: ( (otherlv_0= 'Checkbox' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= ')' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:351:1: (otherlv_0= 'Checkbox' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= ')' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:351:1: (otherlv_0= 'Checkbox' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= ')' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:351:3: otherlv_0= 'Checkbox' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_INT ) ) otherlv_6= ')'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleCheckbox761); 

                	newLeafNode(otherlv_0, grammarAccess.getCheckboxAccess().getCheckboxKeyword_0());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:355:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:356:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:356:1: (lv_name_1_0= RULE_ID )
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:357:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheckbox778); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getCheckboxAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getCheckboxRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleCheckbox796); 

                	newLeafNode(otherlv_2, grammarAccess.getCheckboxAccess().getLeftParenthesisKeyword_2());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:377:1: ( (lv_label_3_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:378:1: (lv_label_3_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:378:1: (lv_label_3_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:379:3: lv_label_3_0= RULE_STRING
            {
            lv_label_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleCheckbox813); 

            			newLeafNode(lv_label_3_0, grammarAccess.getCheckboxAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCheckboxRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"label",
                    		lv_label_3_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleCheckbox830); 

                	newLeafNode(otherlv_4, grammarAccess.getCheckboxAccess().getCommaKeyword_4());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:399:1: ( (lv_value_5_0= RULE_INT ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:400:1: (lv_value_5_0= RULE_INT )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:400:1: (lv_value_5_0= RULE_INT )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:401:3: lv_value_5_0= RULE_INT
            {
            lv_value_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleCheckbox847); 

            			newLeafNode(lv_value_5_0, grammarAccess.getCheckboxAccess().getValueINTTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCheckboxRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"INT");
            	    

            }


            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleCheckbox864); 

                	newLeafNode(otherlv_6, grammarAccess.getCheckboxAccess().getRightParenthesisKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckbox"


    // $ANTLR start "entryRuleRadioButtonGroup"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:429:1: entryRuleRadioButtonGroup returns [EObject current=null] : iv_ruleRadioButtonGroup= ruleRadioButtonGroup EOF ;
    public final EObject entryRuleRadioButtonGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRadioButtonGroup = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:430:2: (iv_ruleRadioButtonGroup= ruleRadioButtonGroup EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:431:2: iv_ruleRadioButtonGroup= ruleRadioButtonGroup EOF
            {
             newCompositeNode(grammarAccess.getRadioButtonGroupRule()); 
            pushFollow(FOLLOW_ruleRadioButtonGroup_in_entryRuleRadioButtonGroup900);
            iv_ruleRadioButtonGroup=ruleRadioButtonGroup();

            state._fsp--;

             current =iv_ruleRadioButtonGroup; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButtonGroup910); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRadioButtonGroup"


    // $ANTLR start "ruleRadioButtonGroup"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:438:1: ruleRadioButtonGroup returns [EObject current=null] : (otherlv_0= 'RadioButtonGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= 'Checked' ( (lv_checkedRadioButton_5_0= ruleRadioButton ) ) otherlv_6= 'Unchecked' otherlv_7= '{' ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )* otherlv_9= '}' otherlv_10= '}' ) ;
    public final EObject ruleRadioButtonGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_label_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_checkedRadioButton_5_0 = null;

        EObject lv_uncheckedRadioButton_8_0 = null;


         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:441:28: ( (otherlv_0= 'RadioButtonGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= 'Checked' ( (lv_checkedRadioButton_5_0= ruleRadioButton ) ) otherlv_6= 'Unchecked' otherlv_7= '{' ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )* otherlv_9= '}' otherlv_10= '}' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:442:1: (otherlv_0= 'RadioButtonGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= 'Checked' ( (lv_checkedRadioButton_5_0= ruleRadioButton ) ) otherlv_6= 'Unchecked' otherlv_7= '{' ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )* otherlv_9= '}' otherlv_10= '}' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:442:1: (otherlv_0= 'RadioButtonGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= 'Checked' ( (lv_checkedRadioButton_5_0= ruleRadioButton ) ) otherlv_6= 'Unchecked' otherlv_7= '{' ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )* otherlv_9= '}' otherlv_10= '}' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:442:3: otherlv_0= 'RadioButtonGroup' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= 'Checked' ( (lv_checkedRadioButton_5_0= ruleRadioButton ) ) otherlv_6= 'Unchecked' otherlv_7= '{' ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )* otherlv_9= '}' otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleRadioButtonGroup947); 

                	newLeafNode(otherlv_0, grammarAccess.getRadioButtonGroupAccess().getRadioButtonGroupKeyword_0());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:446:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_ID) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:447:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:447:1: (lv_name_1_0= RULE_ID )
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:448:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRadioButtonGroup964); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getRadioButtonGroupAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRadioButtonGroupRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleRadioButtonGroup982); 

                	newLeafNode(otherlv_2, grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_2());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:468:1: ( (lv_label_3_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:469:1: (lv_label_3_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:469:1: (lv_label_3_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:470:3: lv_label_3_0= RULE_STRING
            {
            lv_label_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleRadioButtonGroup999); 

            			newLeafNode(lv_label_3_0, grammarAccess.getRadioButtonGroupAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRadioButtonGroupRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"label",
                    		lv_label_3_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleRadioButtonGroup1016); 

                	newLeafNode(otherlv_4, grammarAccess.getRadioButtonGroupAccess().getCheckedKeyword_4());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:490:1: ( (lv_checkedRadioButton_5_0= ruleRadioButton ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:491:1: (lv_checkedRadioButton_5_0= ruleRadioButton )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:491:1: (lv_checkedRadioButton_5_0= ruleRadioButton )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:492:3: lv_checkedRadioButton_5_0= ruleRadioButton
            {
             
            	        newCompositeNode(grammarAccess.getRadioButtonGroupAccess().getCheckedRadioButtonRadioButtonParserRuleCall_5_0()); 
            	    
            pushFollow(FOLLOW_ruleRadioButton_in_ruleRadioButtonGroup1037);
            lv_checkedRadioButton_5_0=ruleRadioButton();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRadioButtonGroupRule());
            	        }
                   		set(
                   			current, 
                   			"checkedRadioButton",
                    		true, 
                    		"RadioButton");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleRadioButtonGroup1049); 

                	newLeafNode(otherlv_6, grammarAccess.getRadioButtonGroupAccess().getUncheckedKeyword_6());
                
            otherlv_7=(Token)match(input,12,FOLLOW_12_in_ruleRadioButtonGroup1061); 

                	newLeafNode(otherlv_7, grammarAccess.getRadioButtonGroupAccess().getLeftCurlyBracketKeyword_7());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:516:1: ( (lv_uncheckedRadioButton_8_0= ruleRadioButton ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==23) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:517:1: (lv_uncheckedRadioButton_8_0= ruleRadioButton )
            	    {
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:517:1: (lv_uncheckedRadioButton_8_0= ruleRadioButton )
            	    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:518:3: lv_uncheckedRadioButton_8_0= ruleRadioButton
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRadioButtonGroupAccess().getUncheckedRadioButtonRadioButtonParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleRadioButton_in_ruleRadioButtonGroup1082);
            	    lv_uncheckedRadioButton_8_0=ruleRadioButton();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRadioButtonGroupRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"uncheckedRadioButton",
            	            		lv_uncheckedRadioButton_8_0, 
            	            		"RadioButton");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_9=(Token)match(input,13,FOLLOW_13_in_ruleRadioButtonGroup1095); 

                	newLeafNode(otherlv_9, grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_9());
                
            otherlv_10=(Token)match(input,13,FOLLOW_13_in_ruleRadioButtonGroup1107); 

                	newLeafNode(otherlv_10, grammarAccess.getRadioButtonGroupAccess().getRightCurlyBracketKeyword_10());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRadioButtonGroup"


    // $ANTLR start "entryRuleRadioButton"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:550:1: entryRuleRadioButton returns [EObject current=null] : iv_ruleRadioButton= ruleRadioButton EOF ;
    public final EObject entryRuleRadioButton() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRadioButton = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:551:2: (iv_ruleRadioButton= ruleRadioButton EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:552:2: iv_ruleRadioButton= ruleRadioButton EOF
            {
             newCompositeNode(grammarAccess.getRadioButtonRule()); 
            pushFollow(FOLLOW_ruleRadioButton_in_entryRuleRadioButton1143);
            iv_ruleRadioButton=ruleRadioButton();

            state._fsp--;

             current =iv_ruleRadioButton; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButton1153); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRadioButton"


    // $ANTLR start "ruleRadioButton"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:559:1: ruleRadioButton returns [EObject current=null] : (otherlv_0= 'RadioButton' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ')' ) ;
    public final EObject ruleRadioButton() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_label_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:562:28: ( (otherlv_0= 'RadioButton' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ')' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:563:1: (otherlv_0= 'RadioButton' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ')' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:563:1: (otherlv_0= 'RadioButton' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ')' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:563:3: otherlv_0= 'RadioButton' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleRadioButton1190); 

                	newLeafNode(otherlv_0, grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:567:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:568:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:568:1: (lv_name_1_0= RULE_ID )
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:569:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRadioButton1207); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getRadioButtonAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getRadioButtonRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleRadioButton1225); 

                	newLeafNode(otherlv_2, grammarAccess.getRadioButtonAccess().getLeftParenthesisKeyword_2());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:589:1: ( (lv_label_3_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:590:1: (lv_label_3_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:590:1: (lv_label_3_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:591:3: lv_label_3_0= RULE_STRING
            {
            lv_label_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleRadioButton1242); 

            			newLeafNode(lv_label_3_0, grammarAccess.getRadioButtonAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRadioButtonRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"label",
                    		lv_label_3_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleRadioButton1259); 

                	newLeafNode(otherlv_4, grammarAccess.getRadioButtonAccess().getRightParenthesisKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRadioButton"


    // $ANTLR start "entryRuleInput"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:619:1: entryRuleInput returns [EObject current=null] : iv_ruleInput= ruleInput EOF ;
    public final EObject entryRuleInput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInput = null;


        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:620:2: (iv_ruleInput= ruleInput EOF )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:621:2: iv_ruleInput= ruleInput EOF
            {
             newCompositeNode(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_ruleInput_in_entryRuleInput1295);
            iv_ruleInput=ruleInput();

            state._fsp--;

             current =iv_ruleInput; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInput1305); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:628:1: ruleInput returns [EObject current=null] : (otherlv_0= 'Input' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= ')' ) ;
    public final EObject ruleInput() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_label_3_0=null;
        Token otherlv_4=null;
        Token lv_value_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:631:28: ( (otherlv_0= 'Input' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= ')' ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:632:1: (otherlv_0= 'Input' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= ')' )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:632:1: (otherlv_0= 'Input' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= ')' )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:632:3: otherlv_0= 'Input' ( (lv_name_1_0= RULE_ID ) )? otherlv_2= '(' ( (lv_label_3_0= RULE_STRING ) ) otherlv_4= ',' ( (lv_value_5_0= RULE_STRING ) ) otherlv_6= ')'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleInput1342); 

                	newLeafNode(otherlv_0, grammarAccess.getInputAccess().getInputKeyword_0());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:636:1: ( (lv_name_1_0= RULE_ID ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:637:1: (lv_name_1_0= RULE_ID )
                    {
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:637:1: (lv_name_1_0= RULE_ID )
                    // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:638:3: lv_name_1_0= RULE_ID
                    {
                    lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInput1359); 

                    			newLeafNode(lv_name_1_0, grammarAccess.getInputAccess().getNameIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getInputRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,15,FOLLOW_15_in_ruleInput1377); 

                	newLeafNode(otherlv_2, grammarAccess.getInputAccess().getLeftParenthesisKeyword_2());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:658:1: ( (lv_label_3_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:659:1: (lv_label_3_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:659:1: (lv_label_3_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:660:3: lv_label_3_0= RULE_STRING
            {
            lv_label_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleInput1394); 

            			newLeafNode(lv_label_3_0, grammarAccess.getInputAccess().getLabelSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInputRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"label",
                    		lv_label_3_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleInput1411); 

                	newLeafNode(otherlv_4, grammarAccess.getInputAccess().getCommaKeyword_4());
                
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:680:1: ( (lv_value_5_0= RULE_STRING ) )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:681:1: (lv_value_5_0= RULE_STRING )
            {
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:681:1: (lv_value_5_0= RULE_STRING )
            // ../widget/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDsl.g:682:3: lv_value_5_0= RULE_STRING
            {
            lv_value_5_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleInput1428); 

            			newLeafNode(lv_value_5_0, grammarAccess.getInputAccess().getValueSTRINGTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInputRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_5_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleInput1445); 

                	newLeafNode(otherlv_6, grammarAccess.getInputAccess().getRightParenthesisKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleWidget_in_entryRuleWidget75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidget85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleWidget122 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleWidget134 = new BitSet(new long[]{0x0000000001144000L});
    public static final BitSet FOLLOW_ruleItem_in_ruleWidget155 = new BitSet(new long[]{0x0000000001146000L});
    public static final BitSet FOLLOW_13_in_ruleWidget168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleItem_in_entryRuleItem204 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleItem214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_ruleItem261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckboxGroup_in_ruleItem288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButtonGroup_in_ruleItem315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_ruleItem342 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage377 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage387 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_ruleImage424 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleImage436 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleImage453 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleImage470 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleImage487 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleImage504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckboxGroup_in_entryRuleCheckboxGroup540 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckboxGroup550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleCheckboxGroup587 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheckboxGroup604 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleCheckboxGroup622 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleCheckboxGroup639 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_ruleCheckbox_in_ruleCheckboxGroup665 = new BitSet(new long[]{0x0000000000082000L});
    public static final BitSet FOLLOW_13_in_ruleCheckboxGroup678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckbox_in_entryRuleCheckbox714 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckbox724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleCheckbox761 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheckbox778 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleCheckbox796 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleCheckbox813 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleCheckbox830 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleCheckbox847 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleCheckbox864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButtonGroup_in_entryRuleRadioButtonGroup900 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButtonGroup910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRadioButtonGroup947 = new BitSet(new long[]{0x0000000000001020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRadioButtonGroup964 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleRadioButtonGroup982 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleRadioButtonGroup999 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleRadioButtonGroup1016 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_ruleRadioButton_in_ruleRadioButtonGroup1037 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleRadioButtonGroup1049 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleRadioButtonGroup1061 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_ruleRadioButton_in_ruleRadioButtonGroup1082 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_13_in_ruleRadioButtonGroup1095 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleRadioButtonGroup1107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_entryRuleRadioButton1143 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButton1153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleRadioButton1190 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRadioButton1207 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleRadioButton1225 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleRadioButton1242 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleRadioButton1259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInput_in_entryRuleInput1295 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInput1305 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleInput1342 = new BitSet(new long[]{0x0000000000008020L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInput1359 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleInput1377 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleInput1394 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleInput1411 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleInput1428 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleInput1445 = new BitSet(new long[]{0x0000000000000002L});

}